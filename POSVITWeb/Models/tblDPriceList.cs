//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSVITWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblDPriceList
    {
        public string kodepricelist { get; set; }
        public string kodebarang { get; set; }
        public decimal hargajual { get; set; }
        public decimal discjual { get; set; }
        public decimal minstok { get; set; }
        public System.DateTime modidate { get; set; }
    
        public virtual tblHBarang tblHBarang { get; set; }
    }
}
