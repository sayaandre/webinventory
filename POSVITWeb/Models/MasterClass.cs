﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace POSVITWeb.Models
{
    public class WelcomeClass
    {
        public List<tblLokasi> store { get; set; }
        public int csstoresize { get; set; }
        public List<tblLokasi> csstore { get; set; }
        public int storesize { get; set; }
        public string choosestore { get; set; }
        public string currkodelokasi { get; set; }
        public string currnamalokasi { get; set; }
        public int trpending { get; set; }
        public int tsopending { get; set; }
        public int stocktemppending { get; set; }
        public int popending { get; set; }
    }

    public class ChooseItemClass
    {
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public string kodeprodusen { get; set; }
        public string namaprodusen { get; set; }
        public int stock { get; set; }
        public double harga { get; set; }
        public string strharga { get; set; }
        public string satuan { get; set; }
        public string itemcode { get; set; }
        public bool aktif { get; set; }
    }

    public class TransferOutClass
    {
        public string nofaktur { get; set; }
        public string nofakturtr { get; set; }
        public string tanggaltr { get; set; }
        public string tanggal { get; set; }
        public string keterangan { get; set; }
        public string kodeawal { get; set; }
        public string namaawal { get; set; }
        public string kodeakhir { get; set; }
        public string namaakhir { get; set; }
        public int totalitem { get; set; }
        public float totalitemdetail { get; set; }
        public string tipe { get; set; }
        public long nourut { get; set; }
        public string status { get; set; }
        public string modidate { get; set; }
        public string userid { get; set; }
        public string usernama { get; set; }
    }

    public class TransferOutDetailClass
    {
        public string tanggal { get; set; }
        public string reqtanggal { get; set; }
        public string comment { get; set; }
        public string fromstore { get; set; }
        public string kodefrom { get; set; }
        public List<tblLokasi> tostore { get; set; }
        public string tostorename { get; set; }
        public string kodeto { get; set; }
        public List<ChooseTypeClass> type { get; set; }
        public string tipe { get; set; }
        public List<ChooseItemClass> items { get; set; }
        public List<ViewCartClass> carts { get; set; }
        public string cartstring { get; set; }
        public List<ChooseTransferRequestClass> trlist { get; set; }
        public string nofakturtr { get; set; }
        public string nofaktur { get; set; }
        public string status { get; set; }
        public string tsomorethantr { get; set; }
        public string tsogreaterthantr { get; set; }
        public string tsocopytr { get; set; }
        public string lockdate { get; set; }
        public string createtime { get; set; }
        public string userid { get; set; }
        public string alamatfrom { get; set; }
        public string alamatto { get; set; }
        public string alamatfrom2 { get; set; }
        public string alamatto2 { get; set; }
        public string fromstorename { get; set; }
        public float totalqty { get; set; }
        public float totalqtykirim { get; set; }
        public decimal total { get; set; }
    }

    public class GoodReceiptPOClass
    {
        public string nofaktur { get; set; }
        public string tanggal { get; set; }
        public string keterangan { get; set; }
        public string vendor { get; set; }
        public int totalpo { get; set; }
        public decimal totalitem { get; set; }
        public long nourut { get; set; }
        public string usernama { get; set; }
    }

    public class GoodReturnClass
    {
        public string nofaktur { get; set; }
        public string tgl { get; set; }
        public string keterangan { get; set; }
        public string username { get; set; }
        public string tipe { get; set; }
        public string suppliername { get; set; }
        public long nourut { get; set; }
        public int qtydetail { get; set; }
    }

    public class PurchaseRequestClass
    {
        public string nofaktur { get; set; }
        public string tanggal { get; set; }
        public string modidate { get; set; }
        public string keterangan { get; set; }
        public string vendor { get; set; }
        public int totalpo { get; set; }
        public float totalitem { get; set; }
        public long nourut { get; set; }
        public string status { get; set; }
        public string usernama { get; set; }
    }

    public class AddGoodsReturnClass
    {
        public List<GetSupplierClass> suppliers { get; set; }
        public string supplier { get; set; }
        public string namalokasi { get; set; }
        public string kodelokasi { get; set; }
        public string tanggal { get; set; }
        public string keterangan { get; set; }
        public string nofaktur { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string username { get; set; }
        public string lockdate { get; set; }
        public List<ViewCartClass> carts { get; set; }
    }

    public class GetSupplierClass
    {
        public string suppliercode { get; set; }
        public string suppliername { get; set; }
        public string supplierPIC { get; set; }
    }

    public class GoodReceiptPODetailClass
    {
        public string nofaktur { get; set; }
        public string tanggal { get; set; }
        public string keterangan { get; set; }
        public string vendorcode { get; set; }
        public string vendorname { get; set; }
        public List<ChooseVendorClass> suppliers { get; set; }
        public int totalpo { get; set; }
        public List<ChoosePOClass> poes { get; set; }
        public List<GoodReceiptPOItemDetailClass> items { get; set; }
        public string reffpo { get; set; }
        public int totalqty { get; set; }
        public string lockdate { get; set; }
    }

    public class PurchaseRequestDetailClass
    {
        public string nofaktur { get; set; }
        public string tanggal { get; set; }
        public string reqdate { get; set; }
        public string keterangan { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string vendorcode { get; set; }
        public string vendorname { get; set; }
        public List<ChooseVendorClass> suppliers { get; set; }
        public List<PurchaseRequestItemDetailClass> items { get; set; }
        public float totalqty { get; set; }
        public double totalamount { get; set; }
        public string strtotalamount { get; set; }
        public string modidate { get; set; }
        public string lockdate { get; set; }
    }

    public class GoodReceiptPOItemDetailClass
    {
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int order { get; set; }
        public int open { get; set; }
        public int qtyterima { get; set; }
        public string satuan { get; set; }
        public long? poreff { get; set; }
        public int lineNum { get; set; }
    }

    public class PurchaseRequestItemDetailClass
    {
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public float order { get; set; }
        public int qtyterima { get; set; }
        public string satuan { get; set; }
        public string poreff { get; set; }
        public double harga { get; set; }
        public string strharga { get; set; }
    }

    public class ChooseVendorClass
    {
        public string vendorname { get; set; }
        public string vendorcode { get; set; }
        public int totalpo { get; set; }
    }

    public class ChoosePOClass
    {
        public int docentry { get; set; }
        public string nopo { get; set; }
        public int itempo { get; set; }
        public List<GoodReceiptPOItemDetailClass> details { get; set; }
    }

    public class ChoosePOWithoutClass
    {
        public int docentry { get; set; }
        public string pocode { get; set; }
        public string itemcode { get; set; }
        public string itemname { get; set; }
        public int qty { get; set; }
        public int openqty { get; set; }
        public string vendorcode { get; set; }
        public string vendorname { get; set; }
        public string docdate { get; set; }
    }

    public class TransferInDetailClass
    {
        public string tanggal { get; set; }
        public List<ChooseTransferOutClass> transferouts { get; set; }
        public string transferoutchoose { get; set; }
        public string comment { get; set; }
        public string fromstore { get; set; }
        public string kodefrom { get; set; }
        public List<tblLokasi> tostore { get; set; }
        public string tostorename { get; set; }
        public string kodeto { get; set; }
        public List<ChooseTypeClass> type { get; set; }
        public string tipe { get; set; }
        public List<ChooseItemClass> items { get; set; }
        public List<ViewCartClass> carts { get; set; }
        public string cartstring { get; set; }
        public string nofakturtrreff { get; set; }
        public string nofakturtr { get; set; }
        public string nofaktur { get; set; }
        public string createtime { get; set; }
        public string userid { get; set; }
        public int totalqty { get; set; }
        public int totalitem { get; set; }
        public string alamatfrom { get; set; }
        public string alamatfrom2 { get; set; }
        public string alamatto { get; set; }
        public string alamatto2 { get; set; }
        public string lockdate { get; set; }
    }

    public class ChooseTransferOutClass
    {
        public string kodefaktur { get; set; }
        public string fromstore { get; set; }
        public float qty { get; set; }
        public string tgl { get; set; }
        public string notes { get; set; }
    }

    public class ChooseStoreClass
    {
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
    }

    public class ChooseTypeClass
    {
        public string typename { get; set; }
    }

    public class ChooseTransferRequestClass
    {
        public string nofaktur { get; set; }
        public string fromstore { get; set; }
        public string tostore { get; set; }
        public float qty { get; set; }
        public string tgl { get; set; }
        public string notes { get; set; }
    }

    public class ViewCartClass
    {
        public int itemno { get; set; }
        public string itemcode { get; set; }
        public string itemname { get; set; }
        public float itemqty { get; set; }
        public double itemprice { get; set; }
        public float itemstok { get; set; }
        public float itemkirim { get; set; }
        public float itemrealqty { get; set; }
        public double itemtotalprice { get; set; }
        public double totalallprice { get; set; }
        public string barcode { get; set; }
    }

    public class GetTransferOutClass
    {
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string kodelokasito { get; set; }
        public string namalokasito { get; set; }
        public string refftr { get; set; }
        public string keterangan { get; set; }
        public string requireddate { get; set; }
        public List<ViewCartClass> carts { get; set; }
    }

    public class GRPO
    {
        public string nofaktur { get; set; }
        public string tanggal { get; set; }
        public string keterangan { get; set; }
        public int totalpo { get; set; }
        public int totalitem { get; set; }
    }

    public class ReportMutasiStokLokasi
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public string fromlokasi { get; set; }
        public string tolokasi { get; set; }
        public string kodelokasi { get; set; }
        public List<tblLokasi> tostore { get; set; }
        public List<tblLokasi> fromstore { get; set; }
        public List<ChooseReportMutasiStokLokasi> tipes { get; set; }
        public string tipe { get; set; }
        public List<ChooseReportMutasiStokLokasi> nofakturs { get; set; }
        public List<GetTypeReportTransferRequest> typereports { get; set; }
        public string tipereport { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportMutasiStokLokasi
    {
        public string nofaktur { get; set; }
        public string nobukti { get; set; }
        public string fromcode { get; set; }
        public string fromname { get; set; }
        public string tocode { get; set; }
        public string toname { get; set; }
        public string tgl { get; set; }
        public string refftr { get; set; }
        public string keterangan { get; set; }
        public string satuan { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int qty { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string modidate { get; set; }
        public string status { get; set; }
    }

    public class ChooseReportMutasiStokLokasi
    {
        public string name { get; set; }
        public string code { get; set; }
    }

    public class ReportStok
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<ChooseReportMutasiStokLokasi> tipes { get; set; }
        public string tipe { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportStok
    {
        public string nofaktur { get; set; }
        public string nobukti { get; set; }
        public string fromcode { get; set; }
        public string fromname { get; set; }
        public string tgl { get; set; }
        public string keterangan { get; set; }
        public string satuan { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int qty { get; set; }
        public int stok { get; set; }
        public int selisih { get; set; }
        public double price { get; set; }
        public double value { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string modidate { get; set; }
    }

    public class ReportStokAkhir
    {
        public string tanggal { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportStokAkhir
    {
        public int no { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string satuan { get; set; }
        public int stokakhir { get; set; }
        public string modidate { get; set; }
    }

    public class ReportKartuStok
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public string itemcode { get; set; }
        public List<GetKategoriClass> kategori { get; set; }
        public string kodelokasi { get; set; }
        public string kodekategori { get; set; }
    }

    public class GetReportKartuStok
    {
        public int no { get; set; }
        public string kategori { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public string nofaktur { get; set; }
        public string tgl { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string realkodebarang { get; set; }
        public string realnamabarang { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int masuk { get; set; }
        public int keluar { get; set; }
        public int stokakhir { get; set; }
        public int stokawal { get; set; }
        public int totalmasuk { get; set; }
        public int totalkeluar { get; set; }
        public int headerawal { get; set; }
        public int headerakhir { get; set; }
        public int saldo { get; set; }
        public string modidate { get; set; }
    }

    public class ItemMasterClass
    {
        public string kodebarang { get; set; }
        public string nama { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public string satuan { get; set; }
        public string strharga { get; set; }
        public double harga { get; set; }
        public int stok { get; set; }
        public string strStock { get; set; }
        public string barcode { get; set; }
        public List<ItemMasterStockDetailsClass> stocks { get; set; }
    }

    public class ItemMasterDetailsClass
    {
        public string kodebarang { get; set; }
        public string realkodebarang { get; set; }
        public string nama { get; set; }
        public string realnama { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public string satuan { get; set; }
        public string strharga { get; set; }
        public double harga { get; set; }
        public int stok { get; set; }
        public string kodevendor { get; set; }
        public string namavendor { get; set; }
        public string itemcode { get; set; }
        public List<ItemMasterStockDetailsClass> stocks { get; set; }
        public byte[] foto { get; set; } 
        public List<ItemMasterPriceListClass> prices { get; set; }
    }

    public class ItemMasterPriceListClass
    {
        public string kodecustomergroup { get; set; }
        public string namacustomergroup { get; set; }
        public double harga { get; set; }
        public string strharga { get; set; }
        public double disc { get; set; }
        public double hargaNett { get; set; }
        public string strHargaNett { get; set; }
    }

    public class ItemMasterStockDetailsClass
    {
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public int stok { get; set; }
    }

    public class EndofShiftClass
    {
        public string fromdate { get; set; }
        public string todate { get; set; }
        public string fromdatesecond { get; set; }
        public string todatesecond { get; set; }
        public double saldoawal { get; set; }
        public string strsaldoawal { get; set; }
        public long shiftid { get; set; }
        public double nominal { get; set; }
        public string beritaacara { get; set; }
        public List<PembayaranClass> payments { get; set; }
        public double headerdiff { get; set; }
        public string strheaderdiff { get; set; }
        public int isclosed { get; set; }
        public string startshiftdate { get; set; }
        public string strTamp { get; set; }
    }

    public class EndofDayClass
    {
        public string indate { get; set; }
        public int checksettlement { get; set; }
        public string beritaacara { get; set; }
        public List<PembayaranClass> payments { get; set; }
        public double headerdiff { get; set; }
        public string strheaderdiff { get; set; }
        public string strTamp { get; set; }
    }

    public class PembayaranClass
    {
        public int nomor { get; set; }
        public string kodepembayaran { get; set; }
        public string namapembayaran { get; set; }
        public double nominal { get; set; }
        public string strnominal { get; set; }
        public double fisik { get; set; }
        public string strfisik { get; set; }
        public double diff { get; set; }
        public string strdiff { get; set; }
        public long id { get; set; }
        public int isHeader { get; set; }
        public string headerName { get; set; }
    }

    public class ReportPenjualanItemClass
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<ChooseReportMutasiStokLokasi> nofakturs { get; set; }
        public List<muser> musers { get; set; }
        public List<GetKategoriClass> kategori { get; set; }
        public List<GetCustomerClass> customers { get; set; }
        public string itemcode { get; set; }
        public string user { get; set; }
        public string customer { get; set; }
        public string kodelokasi { get; set; }
        public string kodekategori { get; set; }
    }

    public class GetReportPenjualanItem
    {
        public string nofaktur { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string tgl { get; set; }
        public string tgljatuhtempo { get; set; }
        public string kodecustomer { get; set; }
        public string namacustomer { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string userid { get; set; }
        public string kategori { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public int qty { get; set; }
        public double total { get; set; }
        public string strtotal { get; set; }
        public string modidate { get; set; }
    }

    public class ReportPenjualanInvoiceClass
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<ChooseReportMutasiStokLokasi> nofakturs { get; set; }
        public List<muser> musers { get; set; }
        public List<GetCustomerClass> customers { get; set; }
        public List<GetKategoriClass> kategori { get; set; }
        public string itemcode { get; set; }
        public string kodelokasi { get; set; }
        public string user { get; set; }
        public string customer { get; set; }
        public string kodekategori { get; set; }
        public List<GetCashierClass> cahiers { get; set; }
    }

    public class GetKategoriClass
    {
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
    }

    public class GetCustomerClass
    {
        public string kodecustomer { get; set; }
        public string namacustomer { get; set; }
    }

    public class GetCashierClass
    {
        public string userid { get; set; }
        public string username { get; set; }
    }

    public class ReportPenjualanByHourClass
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<ChooseReportMutasiStokLokasi> nofakturs { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportPenjualanInvoice
    {
        public int no { get; set; }
        public string strno { get; set; }
        public string nofaktur { get; set; }
        public string kodepembayaran { get; set; }
        public string namapembayaran { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string tgl { get; set; }
        public string tgljatuhtempo { get; set; }
        public string kodecustomer { get; set; }
        public string namacustomer { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string userid { get; set; }
        public string usernama { get; set; }
        public string customerid { get; set; }
        public string kategori { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public string strprice { get; set; }
        public double disc { get; set; }
        public double total { get; set; }
        public string strtotal { get; set; }
        public string modidate { get; set; }
    }

    public class GetReportPenjualanByHour
    {
        public int no { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string kategori { get; set; }
        public int totalqty { get; set; }
        public double totalamount { get; set; }
        public string strtotalqty { get; set; }
        public string strtotalamount { get; set; }
        public int salesqty1 { get; set; }
        public double salesnominal1 { get; set; }
        public int salesqty2 { get; set; }
        public double salesnominal2 { get; set; }
        public int salesqty3 { get; set; }
        public double salesnominal3 { get; set; }
        public int salesqty4 { get; set; }
        public double salesnominal4 { get; set; }
        public int salesqty5 { get; set; }
        public double salesnominal5 { get; set; }
        public int salesqty6 { get; set; }
        public double salesnominal6 { get; set; }
        public int salesqty7 { get; set; }
        public double salesnominal7 { get; set; }
        public int salesqty8 { get; set; }
        public double salesnominal8 { get; set; }
        public int salesqty9 { get; set; }
        public double salesnominal9 { get; set; }
        public int salesqty10 { get; set; }
        public double salesnominal10 { get; set; }
        public int salesqty11 { get; set; }
        public double salesnominal11 { get; set; }
        public int salesqty12 { get; set; }
        public double salesnominal12 { get; set; }
        public int salesqty13 { get; set; }
        public double salesnominal13 { get; set; }
        public int salesqty14 { get; set; }
        public double salesnominal14 { get; set; }
        public int salesqty15 { get; set; }
        public double salesnominal15 { get; set; }
        public int salesqty16 { get; set; }
        public double salesnominal16 { get; set; }
        public int salesqty17 { get; set; }
        public double salesnominal17 { get; set; }
        public int salesqty18 { get; set; }
        public double salesnominal18 { get; set; }
        public int salesqty19 { get; set; }
        public double salesnominal19 { get; set; }
        public int salesqty20 { get; set; }
        public double salesnominal20 { get; set; }
        public int salesqty21 { get; set; }
        public double salesnominal21 { get; set; }
        public int salesqty22 { get; set; }
        public double salesnominal22 { get; set; }
        public int salesqty23 { get; set; }
        public double salesnominal23 { get; set; }
        public int salesqty24 { get; set; }
        public double salesnominal24 { get; set; }
    }

    public class ReportPaymentSummary
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<muser> musers { get; set; }
        public string user { get; set; }
    }

    public class GetReportPaymentSummary
    {
        public string nofaktur { get; set; }
        public string kodelokasi { get; set; }
        public string namalokasi { get; set; }
        public string tgl { get; set; }
        public string tgljatuhtempo { get; set; }
        public string kodecustomer { get; set; }
        public string namacustomer { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string userid { get; set; }
        public string kategori { get; set; }
        public string kodekategori { get; set; }
        public string namakategori { get; set; }
        public int qty { get; set; }
        public double total { get; set; }
        public string strtotal { get; set; }
        public string modidate { get; set; }
        public string kodepembayaran { get; set; }
        public string namapembayaran { get; set; }
    }

    public class ReportEndofShift
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<muser> musers { get; set; }
        public string user { get; set; }
    }

    public class GetReportEndofShift
    {
        public string userid { get; set; }
        public string username { get; set; }
        public double saldoawal { get; set; }
        public string strsaldoawal { get; set; }
        public string kodepembayaran { get; set; }
        public string namapembayaran { get; set; }
        public double nominalsystem { get; set; }
        public string strnominalsystem { get; set; }
        public double nominalfisik { get; set; }
        public string strnominalfisik { get; set; }
        public double nominaldiff { get; set; }
        public string strnominaldiff { get; set; }
        public string beritaacara { get; set; }
        public string tgl { get; set; }
        public string startshift { get; set; }
        public long idshift { get; set; }
    }

    public class ReportEndofDay
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<muser> musers { get; set; }
        public string user { get; set; }
    }

    public class GetReportEndofDay
    {
        public string userid { get; set; }
        public string username { get; set; }
        public double saldoawal { get; set; }
        public string strsaldoawal { get; set; }
        public string kodepembayaran { get; set; }
        public string namapembayaran { get; set; }
        public double nominalsystem { get; set; }
        public string strnominalsystem { get; set; }
        public double nominalfisik { get; set; }
        public string strnominalfisik { get; set; }
        public double nominaldiff { get; set; }
        public string strnominaldiff { get; set; }
        public string beritaacara { get; set; }
        public string tgl { get; set; }
        public string startshift { get; set; }
        public long idshift { get; set; }
    }

    public class ReportTransferRequest
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public List<GetStatusReportTransferRequest> statuses { get; set; }
        public string status { get; set; }
        public List<tblLokasi> lokasis { get; set; }
        public string kodelokasi { get; set; }
        public List<GetTypeReportTransferRequest> tipes { get; set; }
        public string tipe { get; set; }
    }

    public class GetTypeReportTransferRequest
    {
        public string kodetipe { get; set; }
        public string namatipe { get; set; }
    }

    public class GetStatusReportTransferRequest
    {
        public string kodestatus { get; set; }
        public string namastatus { get; set; }
    }

    public class GetReportTransferRequest
    {
        public string nofaktur { get; set; }
        public string tgl { get; set; }
        public string tgljatuhtempo { get; set; }
        public string fromnama { get; set; }
        public string fromcode { get; set; }
        public string tonama { get; set; }
        public string tocode { get; set; }
        public string tipe { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int qty { get; set; }
        public int kirim { get; set; }
        public int open { get; set; }
        public int totalqty { get; set; }
        public int totalkirim { get; set; }
        public int totalopen { get; set; }
        public string comment { get; set; }
        public string status { get; set; }
        public string userid { get; set; }
        public string usernama { get; set; }
        public string requireddate { get; set; }
    }

    public class ChangePasswordClass
    {
        public string userid { get; set; }
        public string username { get; set; }
        public string oldpassword { get; set; }
        public string newpassword { get; set; }
        public string confnewpasswprd { get; set; }
    }

    public class SendNotifOutput
    {
        public string result { get; set; }
        public string transferoutdetail { get; set; }
        public string message { get; set; }
    }

    public class ReportGRPO
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportGRPO
    {
        public string nofaktur { get; set; }
        public string nobukti { get; set; }
        public string tgl { get; set; }
        public string keterangan { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public int qty { get; set; }
        public long docentry { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string modidate { get; set; }
        public string kodesupplier { get; set; }
        public string namasupplier { get; set; }
    }

    public class ReportGoodsReturn
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportGoodsReturn
    {
        public string nofaktur { get; set; }
        public string nobukti { get; set; }
        public string tgl { get; set; }
        public string keterangan { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string suppliercode { get; set; }
        public string suppliername { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public double value { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string modidate { get; set; }
    }

    public class ReportPurchaseRequest
    {
        public string fromtanggal { get; set; }
        public string totanggal { get; set; }
        public string itemcode { get; set; }
    }

    public class GetReportPurchaseRequest
    {
        public string nofaktur { get; set; }
        public string nobukti { get; set; }
        public string tgl { get; set; }
        public string tglrequired { get; set; }
        public string keterangan { get; set; }
        public string kodebarang { get; set; }
        public string namabarang { get; set; }
        public string suppliercode { get; set; }
        public string suppliername { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public double value { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
        public string modidate { get; set; }
    }

    public class MasterClass
    {

    }
}