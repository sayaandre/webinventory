//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSVITWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCustomer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCustomer()
        {
            this.tblthPenjualans = new HashSet<tblthPenjualan>();
        }
    
        public string kodecustomer { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string alamat2 { get; set; }
        public string alamat3 { get; set; }
        public string telp { get; set; }
        public string hp { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string kodewilayah { get; set; }
        public decimal disctotaljual { get; set; }
        public string alamatdelivery { get; set; }
        public string alamatdelivery2 { get; set; }
        public string alamatdelivery3 { get; set; }
        public string negara { get; set; }
        public string kodepos { get; set; }
        public string website { get; set; }
        public string disctotaljual_string { get; set; }
        public int term { get; set; }
        public string npwp { get; set; }
        public string catatan { get; set; }
        public string namacontact1 { get; set; }
        public string jabatancontact1 { get; set; }
        public string alamatcontact1 { get; set; }
        public string telponcontact1 { get; set; }
        public string hpcontact1 { get; set; }
        public string faxcontact1 { get; set; }
        public string emailcontact1 { get; set; }
        public string namacontact2 { get; set; }
        public string jabatancontact2 { get; set; }
        public string alamatcontact2 { get; set; }
        public string telponcontact2 { get; set; }
        public string hpcontact2 { get; set; }
        public string faxcontact2 { get; set; }
        public string emailcontact2 { get; set; }
        public bool aktif { get; set; }
        public System.DateTime tglAkhir { get; set; }
        public System.DateTime tglMasuk { get; set; }
        public decimal bataspiutang { get; set; }
        public decimal saldoawal { get; set; }
        public string userid { get; set; }
        public System.DateTime modidate { get; set; }
        public string ip { get; set; }
        public decimal deposit { get; set; }
        public string telp2 { get; set; }
        public string tempatlahir { get; set; }
        public System.DateTime tgllahir { get; set; }
        public bool defhargagrosir { get; set; }
        public string persediaan { get; set; }
        public string namawp { get; set; }
        public string alamatwp { get; set; }
        public decimal discwholesale { get; set; }
        public decimal disckonsinyasi { get; set; }
        public decimal WSBrandMin { get; set; }
        public decimal WSBrandMax { get; set; }
        public decimal WSDiscMin { get; set; }
        public decimal WSDiscMax { get; set; }
        public int tipecustomerwholesale { get; set; }
        public string initial { get; set; }
        public string WhsCode { get; set; }
        public string kodelokasi { get; set; }
        public long point { get; set; }
        public string namamember { get; set; }
        public string ktp { get; set; }
        public string alamatmember { get; set; }
        public string alamatmember2 { get; set; }
        public string countercard { get; set; }
        public string membercard { get; set; }
        public System.DateTime membersince { get; set; }
        public System.DateTime tglmember { get; set; }
        public System.DateTime expireddate { get; set; }
        public string lokasiregistrasi { get; set; }
        public string sex { get; set; }
        public string statuskawin { get; set; }
        public string jabatan { get; set; }
        public string agama { get; set; }
        public string kebangsaan { get; set; }
        public string InfoKaliMain { get; set; }
        public string InfoTempatMain { get; set; }
        public string InfoKejuaraan { get; set; }
        public string InfoRekomendasi { get; set; }
        public string InfoAlasanRekomendasi { get; set; }
        public bool defppn { get; set; }
        public bool pointtanpakartu { get; set; }
        public System.DateTime tglaktivasicountercard { get; set; }
        public System.DateTime tglaktivasimembercard { get; set; }
        public int AvgDurasiVisit { get; set; }
        public string BuyingFlag { get; set; }
        public string SpendingFlag { get; set; }
        public string HandicapFlag { get; set; }
        public string VisitorFlag { get; set; }
        public string Segment { get; set; }
        public string CustomerRank { get; set; }
        public string AgeLevel { get; set; }
        public string accountrep { get; set; }
        public string kategori1Flag { get; set; }
        public string kategori1Name { get; set; }
        public string kategori2Flag { get; set; }
        public string kategori2Name { get; set; }
        public string kategori3Flag { get; set; }
        public string kategori3Name { get; set; }
        public string kategori4Flag { get; set; }
        public string kategori4Name { get; set; }
        public string Segment2 { get; set; }
        public string prefkodelokasi { get; set; }
        public string id_account_sfdc { get; set; }
        public string id_contact_sfdc { get; set; }
        public string tipewholesale { get; set; }
        public int pointmember { get; set; }
        public string perusahaan { get; set; }
        public string vouchersms { get; set; }
        public string tipemember { get; set; }
        public bool mediapromosimember { get; set; }
        public string kodecustomergroup { get; set; }
        public decimal batasplafon { get; set; }
        public int tipeplafon { get; set; }
        public string kota { get; set; }
        public string tipecustomer { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblthPenjualan> tblthPenjualans { get; set; }
        public virtual tblCustomerGroup tblCustomerGroup { get; set; }
    }
}
