﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.Ajax.Utilities;
using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class ReportController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult ReportMutasiStokLokasi()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportMutasiStokLokasi reportMutasiStokLokasi = new ReportMutasiStokLokasi();
            string kodelokasi = Session["kode_lokasi"].ToString();
            reportMutasiStokLokasi.kodelokasi = kodelokasi;
            reportMutasiStokLokasi.fromstore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && p.initial.Length > 0 && p.aktif == true).ToList();
            reportMutasiStokLokasi.tipes = new List<ChooseReportMutasiStokLokasi>();
            ChooseReportMutasiStokLokasi choose = new ChooseReportMutasiStokLokasi();
            choose.code = "TSI";
            choose.name = "Transfer In";
            reportMutasiStokLokasi.tipes.Add(choose);
            choose = new ChooseReportMutasiStokLokasi();
            choose.code = "TSO";
            choose.name = "Transfer Out";
            reportMutasiStokLokasi.tipes.Add(choose);
            return View(reportMutasiStokLokasi);
        }

        public ActionResult ReportMutasiStokOutLokasi()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportMutasiStokLokasi reportMutasiStokLokasi = new ReportMutasiStokLokasi();
            string kodelokasi = Session["kode_lokasi"].ToString();
            reportMutasiStokLokasi.kodelokasi = kodelokasi;
            reportMutasiStokLokasi.tostore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && p.initial.Length > 0 && p.kodelokasi != kodelokasi && p.aktif == true).ToList();
            reportMutasiStokLokasi.tipes = new List<ChooseReportMutasiStokLokasi>();
            ChooseReportMutasiStokLokasi choose = new ChooseReportMutasiStokLokasi();
            choose.code = "TSI";
            choose.name = "Transfer In";
            reportMutasiStokLokasi.tipes.Add(choose);
            choose = new ChooseReportMutasiStokLokasi();
            choose.code = "TSO";
            choose.name = "Transfer Out";
            reportMutasiStokLokasi.tipes.Add(choose);
            reportMutasiStokLokasi.typereports = new List<GetTypeReportTransferRequest>();
            GetTypeReportTransferRequest gettipe = new GetTypeReportTransferRequest();
            gettipe.kodetipe = "1";
            gettipe.namatipe = "TSO From This Location";
            reportMutasiStokLokasi.typereports.Add(gettipe);
            gettipe = new GetTypeReportTransferRequest();
            gettipe.kodetipe = "2";
            gettipe.namatipe = "TSO For This Location";
            reportMutasiStokLokasi.typereports.Add(gettipe);
            return View(reportMutasiStokLokasi);
        }

        public JsonResult GetReportMutasiStokLokasi(string fromdate, string todate, string fromlocation, string tolocation, string type, string itemcode)
        {
            List<GetReportMutasiStokLokasi> journallist = new List<GetReportMutasiStokLokasi>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();

                    var report = (from th in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains(type) && p.tujuan == tolocation && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur &&
                                  p.kodebarang == itemcode)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                                  group new { th, td, b, u } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l1.kodelokasi,
                                      fromname = l1.nama,
                                      tokode = l2.kodelokasi,
                                      toname = l2.nama,
                                      th.tgl,
                                      th.refftr,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tokode,
                                      g.Key.toname,
                                      g.Key.tgl,
                                      g.Key.refftr,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.musernama,
                                      g.Key.muserid
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    if (itemcode.Equals(""))
                    {
                        report = (from th in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains(type) && p.tujuan == tolocation && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                                  group new { th, td, b, u } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l1.kodelokasi,
                                      fromname = l1.nama,
                                      tokode = l2.kodelokasi,
                                      toname = l2.nama,
                                      th.tgl,
                                      th.refftr,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tokode,
                                      g.Key.toname,
                                      g.Key.tgl,
                                      g.Key.refftr,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.musernama,
                                      g.Key.muserid
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    }
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        string refftr = reports.refftr;
                        string fromcode = reports.fromkode;
                        string fromname = reports.fromname;
                        var checktso = db.tblthMutasiStoks.Where(p => p.NoFaktur == refftr).FirstOrDefault();
                        if (checktso != null)
                        {
                            string froma = checktso.kodelokasi;
                            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == froma).FirstOrDefault();
                            if (checklokasi != null)
                            {
                                fromcode = checklokasi.kodelokasi;
                                fromname = checklokasi.nama;
                            }
                        }
                        var resultreport = new GetReportMutasiStokLokasi
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : fromcode,
                            fromname = reports.NoFaktur.Equals(nofaktur) ? "" : fromname,
                            tocode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.tokode,
                            toname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.toname,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            refftr = reports.NoFaktur.Equals(nofaktur) ? "" : reports.refftr,
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            satuan = reports.satuan,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            qty = (int)reports.qty,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        //if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                        //{
                            if (fromlocation != "0")
                            {
                                if (reports.refftr.Contains(fromlocation))
                                {
                                    journallist.Add(resultreport);
                                    nofaktur = reports.NoFaktur;
                                    i++;
                                }
                            }
                            else
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                                i++;
                            }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportMutasiStokOutLokasi(string fromdate, string todate, string location, string tolocation, string type, string itemcode)
        {
            List<GetReportMutasiStokLokasi> journallist = new List<GetReportMutasiStokLokasi>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    string transitkodelokasi = "";
                    string kodefrom = Session["kode_lokasi"].ToString();
                    string kodetos = Session["kode_lokasi"].ToString();
                    var checklokasis = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                    if (checklokasis != null)
                    {
                        transitkodelokasi = checklokasis.InTransit;
                        kodetos = checklokasis.InTransit;
                    }
                    var report = (from th in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains(type) && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur &&
                                  p.kodebarang == itemcode)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                                  group new { th, td, b, u } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l1.kodelokasi,
                                      fromname = l1.nama,
                                      tokode = l2.kodelokasi,
                                      toname = l2.nama,
                                      th.tujuan,
                                      th.tgl,
                                      th.refftr,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tokode,
                                      g.Key.toname,
                                      g.Key.tujuan,
                                      g.Key.tgl,
                                      g.Key.refftr,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate,
                                      checktolokasi = db.tblLokasis.Where(p => p.InTransit == g.Key.tokode).FirstOrDefault(),
                                      checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    if (itemcode.Equals(""))
                    {
                        report = (from th in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains(type) && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                                  group new { th, td, b, u } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l1.kodelokasi,
                                      fromname = l1.nama,
                                      tokode = l2.kodelokasi,
                                      toname = l2.nama,
                                      th.tujuan,
                                      th.tgl,
                                      th.refftr,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tokode,
                                      g.Key.toname,
                                      g.Key.tujuan,
                                      g.Key.tgl,
                                      g.Key.refftr,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate,
                                      checktolokasi = db.tblLokasis.Where(p => p.InTransit == g.Key.tokode).FirstOrDefault(),
                                      checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    }
                    if (location == "1")
                    {
                        report = report.Where(p => p.fromkode == kodefrom).ToList();
                    }
                    else if (location == "2")
                    {
                        report = report.Where(p => p.tokode == kodetos).ToList();
                    }
                    if (tolocation != "0")
                    {
                        string transitlocation = "";
                        var checktransit = db.tblLokasis.Where(p => p.kodelokasi == tolocation).FirstOrDefault();
                        if (checklokasis != null)
                        {
                            transitlocation = checktransit.InTransit;
                        }
                        report = location == "1" ? report.Where(p => p.tokode == transitlocation).ToList() : report.Where(p => p.fromkode == tolocation).ToList();
                    }
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        string tocode = checklokasis != null ? checklokasis.kodelokasi : "";
                        string toname = checklokasis != null ? checklokasis.nama : "";
                        if (reports.checktolokasi != null)
                        {
                            tocode = reports.checktolokasi.kodelokasi;
                            toname = reports.checktolokasi.nama;
                        }
                        int countdetailmutasi = 0;
                        for (int j = 0; j < reports.checkdetailmutasi.Count; j++)
                        {
                            if (reports.checkdetailmutasi[j].qty == reports.checkdetailmutasi[j].kirim)
                            {
                                countdetailmutasi++;
                            }
                        }
                        var resultreport = new GetReportMutasiStokLokasi
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                            fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                            tocode = reports.NoFaktur.Equals(nofaktur) ? "" : tocode,
                            toname = reports.NoFaktur.Equals(nofaktur) ? "" : toname,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            refftr = reports.NoFaktur.Equals(nofaktur) ? "" : (reports.refftr == "0" ? "-" : reports.refftr),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            satuan = reports.satuan,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.qty,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama,
                            status = reports.checkdetailmutasi.Count == countdetailmutasi ? "Close" : "Open"
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        journallist.Add(resultreport);
                        nofaktur = reports.NoFaktur;
                        i++;
                        //if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                        //{
                        //    //if (tolocation != "0")
                        //    //{
                        //    //    if (reports.refftr.Contains(tolocation))
                        //    //    {
                        //    //        journallist.Add(resultreport);
                        //    //        nofaktur = reports.NoFaktur;
                        //    //        i++;
                        //    //    }
                        //    //}
                        //    //else
                        //    //{
                        //    //    journallist.Add(resultreport);
                        //    //    nofaktur = reports.NoFaktur;
                        //    //    i++;
                        //    //}
                        //    journallist.Add(resultreport);
                        //    nofaktur = reports.NoFaktur;
                        //    i++;
                        //}
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public FileResult ExportReportMutasiStokLokasi(string fromdate, string todate, string fromlocation, string tolocation, string type, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? totalmutasistok = 0;
            decimal? totalitem = 0;
            decimal? grandtotalmutasistok = 0;
            List<GetReportMutasiStokLokasi> journallist = new List<GetReportMutasiStokLokasi>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblthMutasiStoks.Where(p => p.kodelokasi == kodelokasi || p.tujuan == kodelokasi)
                              from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur &&
                              p.kodebarang == itemcode)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                              group new { th, td, b, u } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l1.kodelokasi,
                                  fromname = l1.nama,
                                  tokode = l2.kodelokasi,
                                  toname = l2.nama,
                                  th.tgl,
                                  th.refftr,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tokode,
                                  g.Key.toname,
                                  g.Key.tgl,
                                  g.Key.refftr,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                if (itemcode.Equals(""))
                {
                    report = (from th in db.tblthMutasiStoks.Where(p => p.kodelokasi == kodelokasi || p.tujuan == kodelokasi)
                              from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                              group new { th, td, b, u } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l1.kodelokasi,
                                  fromname = l1.nama,
                                  tokode = l2.kodelokasi,
                                  toname = l2.nama,
                                  th.tgl,
                                  th.refftr,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tokode,
                                  g.Key.toname,
                                  g.Key.tgl,
                                  g.Key.refftr,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                }
                string nofaktur = "";
                foreach (var reports in report)
                {
                    string refftr = reports.refftr;
                    string fromcode = reports.fromkode;
                    string fromname = reports.fromname;
                    var checktso = db.tblthMutasiStoks.Where(p => p.NoFaktur == refftr).FirstOrDefault();
                    if (checktso != null)
                    {
                        string froma = checktso.kodelokasi;
                        var checklokasitsi = db.tblLokasis.Where(p => p.kodelokasi == froma).FirstOrDefault();
                        if (checklokasitsi != null)
                        {
                            fromcode = checklokasitsi.kodelokasi;
                            fromname = checklokasitsi.nama;
                        }
                    }
                    var resultreport = new GetReportMutasiStokLokasi
                    {
                        nofaktur = reports.NoFaktur,
                        nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                        fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : fromcode,
                        fromname = reports.NoFaktur.Equals(nofaktur) ? "" : fromname,
                        tocode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.tokode,
                        toname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.toname,
                        tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                        refftr = reports.NoFaktur.Equals(nofaktur) ? "" : reports.refftr,
                        keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                        satuan = reports.satuan,
                        kodebarang = reports.kodebarang,
                        namabarang = reports.nama,
                        qty = (int)reports.qty,
                        userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                        username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                    };
                    string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                    {
                        if (fromlocation != "0")
                        {
                            if (reports.refftr.Contains(fromlocation))
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            nofaktur = reports.NoFaktur;
                        }
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table-bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial; border: 1px solid black;'> <thead> <tr>"
                           + "<th style='text-align:center; border: 1px solid black;'>No Bukti</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Tanggal</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Transfer Dari</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Transfer Tujuan</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>No. Reff</th>"
                           + "</tr> </thead> <tbody>";
                int no = 0;
                for (int i = 0; i < journallist.Count; i++)
                {
                    no = no + 1;
                    totalmutasistok = totalmutasistok + journallist[i].qty;
                    totalitem = no;
                    grandtotalmutasistok = grandtotalmutasistok + journallist[i].qty;
                    if (i == 0)
                    {
                        list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].fromname + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].toname + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + (journallist[i].refftr.Equals("") ? "" : (journallist[i].refftr)) + "</td>"
                           + "</tr>"
                            + "<tr>"
                            + "<th style='text-align:center; border: 1px solid black;'></th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Kode Barang</th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Nama Barang</th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Qty</th>"
                            + "<th style='text-align:center; border: 1px solid black;'></th>"
                            + "</tr>";
                    }
                    list = list
                        + "<tr colspan='5'>"
                        + "<td style='background - color: #B8DBFD' colspan='5'>&nbsp;</td>"
                        + "</tr>";
                    list = list
                            + "<tr>"
                            + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namabarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].qty + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "<td style='background - color: #B8DBFD' colspan='3'><hr style='color: black;'></hr></td>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL QUANTITY</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmutasistok + "</td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL ITEM</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalitem + "</td>"
                               + "</tr>";
                        list = list + "<tr colspan='5'>"
                             + "<td style='background - color: #B8DBFD' colspan='5'><hr style='color:black;'></hr></td>"
                             + "</tr>";
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left; font-weight: bold;'>GRAND TOTAL MUTASI STOK</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalmutasistok + "</td>"
                          + "</tr>";
                        grandtotalmutasistok = 0;
                        no = 0;
                    }
                    else
                    {
                        if (!journallist[i].nofaktur.Equals(journallist[i + 1].nofaktur))
                        {
                            list = list + "<tr colspan='5'>"
                                    + "<td style='background - color: #B8DBFD' colspan='5'>&nbsp;</td>"
                                    + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "<td style='background - color: #B8DBFD' colspan='3'><hr style='color:black'></hr></td>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "</tr>";
                            list = list + "<tr>"
                              + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL MUTASI STOK</td>"
                              + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmutasistok + "</td>"
                              + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL ITEM</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalitem + "</td>"
                               + "</tr>";
                            list = list + "<tr colspan='5'>"
                                 + "<td style='background - color: #B8DBFD' colspan='5'>=============================================================================================</td>"
                                 + "</tr>";
                            list = list + "<tr>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].nobukti + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].tgl + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].fromname + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].toname + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + (journallist[i + 1].refftr.Equals("") ? "" : (journallist[i + 1].refftr)) + "</td>"
                              + "</tr>"
                               + "<tr>"
                               + "<th style='text-align:center; border: 1px solid black;'></th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Kode Barang</th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Nama Barang</th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Qty</th>"
                               + "<th style='text-align:center; border: 1px solid black;'></th>"
                               + "</tr>";
                            totalmutasistok = 0;
                            no = 0;
                        }
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Mutasi Stok In", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(fromdate + " s.d. " + todate + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Mutasi_Stok_In.pdf");
            }
        }

        public FileResult ExportReportMutasiStokOutLokasi(string fromdate, string todate, string fromlocation, string tolocation, string type, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? totalmutasistok = 0;
            decimal? totalitem = 0;
            decimal? grandtotalmutasistok = 0;
            List<GetReportMutasiStokLokasi> journallist = new List<GetReportMutasiStokLokasi>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblthMutasiStoks.Where(p => p.kodelokasi == kodelokasi || p.tujuan == kodelokasi)
                              from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur &&
                              p.kodebarang == itemcode)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                              group new { th, td, b, u } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l1.kodelokasi,
                                  fromname = l1.nama,
                                  tokode = l2.kodelokasi,
                                  toname = l2.nama,
                                  th.tgl,
                                  th.refftr,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tokode,
                                  g.Key.toname,
                                  g.Key.tgl,
                                  g.Key.refftr,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                if (itemcode.Equals(""))
                {
                    report = (from th in db.tblthMutasiStoks.Where(p => p.kodelokasi == kodelokasi || p.tujuan == kodelokasi)
                              from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == th.NoFaktur)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from l2 in db.tblLokasis.Where(p => p.kodelokasi == th.tujuan)
                              group new { th, td, b, u } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l1.kodelokasi,
                                  fromname = l1.nama,
                                  tokode = l2.kodelokasi,
                                  toname = l2.nama,
                                  th.tgl,
                                  th.refftr,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tokode,
                                  g.Key.toname,
                                  g.Key.tgl,
                                  g.Key.refftr,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                }
                string nofaktur = "";
                foreach (var reports in report)
                {
                    string tocode = reports.tokode;
                    string toname = reports.toname;
                    var checktolokasi = db.tblLokasis.Where(p => p.InTransit == tocode).FirstOrDefault();
                    if (checktolokasi != null)
                    {
                        tocode = checktolokasi.kodelokasi;
                        toname = checktolokasi.nama;
                    }
                    var resultreport = new GetReportMutasiStokLokasi
                    {
                        nofaktur = reports.NoFaktur,
                        nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                        fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                        fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                        tocode = reports.NoFaktur.Equals(nofaktur) ? "" : tocode,
                        toname = reports.NoFaktur.Equals(nofaktur) ? "" : toname,
                        tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                        refftr = reports.NoFaktur.Equals(nofaktur) ? "" : reports.refftr,
                        keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                        satuan = reports.satuan,
                        kodebarang = reports.kodebarang,
                        namabarang = reports.nama,
                        qty = (int)reports.qty,
                        userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                        username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                    };
                    string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                    {
                        if (tolocation != "0")
                        {
                            if (reports.refftr.Contains(tolocation))
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            nofaktur = reports.NoFaktur;
                        }
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table-bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial; border: 1px solid black;'> <thead> <tr>"
                           + "<th style='text-align:center; border: 1px solid black;'>No Bukti</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Tanggal</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Transfer Dari</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>Transfer Tujuan</th>"
                           + "<th style='text-align:center; border: 1px solid black;'>No. Reff</th>"
                           + "</tr> </thead> <tbody>";
                int no = 0;
                for (int i = 0; i < journallist.Count; i++)
                {
                    no = no + 1;
                    totalmutasistok = totalmutasistok + journallist[i].qty;
                    totalitem = no;
                    grandtotalmutasistok = grandtotalmutasistok + journallist[i].qty;
                    if (i == 0)
                    {
                        list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].fromname + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].toname + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + (journallist[i].refftr.Equals("") ? "" : (journallist[i].refftr)) + "</td>"
                           + "</tr>"
                            + "<tr>"
                            + "<th style='text-align:center; border: 1px solid black;'></th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Kode Barang</th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Nama Barang</th>"
                            + "<th style='text-align:center; border: 1px solid black;'>Qty</th>"
                            + "<th style='text-align:center; border: 1px solid black;'></th>"
                            + "</tr>";
                    }
                    list = list
                        + "<tr colspan='5'>"
                        + "<td style='background - color: #B8DBFD' colspan='5'>&nbsp;</td>"
                        + "</tr>";
                    list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namabarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].qty + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "<td style='background - color: #B8DBFD' colspan='3'><hr style='color:black'></hr></td>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL MUTASI STOK</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmutasistok + "</td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL ITEM</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalitem + "</td>"
                               + "</tr>";
                        list = list + "<tr colspan='5'>"
                             + "<td style='background - color: #B8DBFD' colspan='5'><hr style='color:black;'></hr></td>"
                             + "</tr>";
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left; font-weight: bold;'>GRAND TOTAL MUTASI STOK</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalmutasistok + "</td>"
                          + "</tr>";
                        grandtotalmutasistok = 0;
                        no = 0;
                    }
                    else
                    {
                        if (!journallist[i].nofaktur.Equals(journallist[i + 1].nofaktur))
                        {
                            list = list + "<tr colspan='5'>"
                                    + "<td style='background - color: #B8DBFD' colspan='5'>&nbsp;</td>"
                                    + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "<td style='background - color: #B8DBFD' colspan='3'><hr style='color:black'></hr></td>"
                               + "<td style='background - color: #B8DBFD'></td>"
                               + "</tr>";
                            list = list + "<tr>"
                              + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL MUTASI STOK</td>"
                              + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmutasistok + "</td>"
                              + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL ITEM</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalitem + "</td>"
                               + "</tr>";
                            list = list + "<tr colspan='5'>"
                                 + "<td style='background - color: #B8DBFD' colspan='5'>=============================================================================================</td>"
                                 + "</tr>";
                            list = list + "<tr>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].nobukti + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].tgl + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].fromname + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].toname + "</td>"
                              + "<td style='background - color: #B8DBFD;text-align:center'>" + (journallist[i + 1].refftr.Equals("") ? "" : (journallist[i + 1].refftr)) + "</td>"
                              + "</tr>"
                               + "<tr>"
                               + "<th style='text-align:center; border: 1px solid black;'></th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Kode Barang</th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Nama Barang</th>"
                               + "<th style='text-align:center; border: 1px solid black;'>Qty</th>"
                               + "<th style='text-align:center; border: 1px solid black;'></th>"
                               + "</tr>";
                            totalmutasistok = 0;
                            no = 0;
                        }
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Mutasi Stok Out", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(fromdate + " s.d. " + todate + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Mutasi_Stok_Out.pdf");
            }
        }

        public ActionResult ReportStok()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportStok report = new ReportStok();
            report.tipes = new List<ChooseReportMutasiStokLokasi>();
            ChooseReportMutasiStokLokasi choose = new ChooseReportMutasiStokLokasi();
            choose.code = "STM";
            choose.name = "Stok Masuk";
            report.tipes.Add(choose);
            choose = new ChooseReportMutasiStokLokasi();
            choose.code = "STO";
            choose.name = "Stok Opname";
            report.tipes.Add(choose);
            return View(report);
        }

        public JsonResult GetReportStok(string fromdate, string todate, string type, string itemcode)
        {
            List<GetReportStok> journallist = new List<GetReportStok>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthOpnames.Where(p => p.kodelokasi == kodelokasi && p.tipe != 3)
                                  from td in db.tbltdOpnames.Where(p => p.NoFaktur == th.NoFaktur &&
                                  p.kodebarang == itemcode)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  group new { th, td, b, u, l } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l.kodelokasi,
                                      fromname = l.nama,
                                      th.tgl,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      td.stok,
                                      td.harga,
                                      td.hpp,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.stok,
                                      g.Key.harga,
                                      g.Key.hpp,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    if (itemcode.Equals(""))
                    {
                        report = (from th in db.tblthOpnames.Where(p => p.kodelokasi == kodelokasi && p.tipe != 3)
                                  from td in db.tbltdOpnames.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  group new { th, td, b, u, l } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l.kodelokasi,
                                      fromname = l.nama,
                                      th.tgl,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      td.stok,
                                      td.harga,
                                      td.hpp,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.stok,
                                      g.Key.harga,
                                      g.Key.hpp,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    }
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportStok
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                            fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            satuan = reports.satuan,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.qty,
                            stok = (int)reports.stok,
                            selisih = (int)reports.qty - (int)reports.stok,
                            price = (double)reports.harga,
                            value = (double)reports.hpp,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                        {
                            if (!type.Equals("0"))
                            {
                                if (reports.NoFaktur.StartsWith(type))
                                {
                                    journallist.Add(resultreport);
                                    nofaktur = reports.NoFaktur;
                                    i++;
                                }
                            }
                            else
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                                i++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportReportStok(string fromdate, string todate, string type, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? totalstok = 0;
            decimal? grandtotalstok = 0;
            decimal? totalqty = 0;
            decimal? grandtotalqty = 0;
            decimal? totalselisih = 0;
            decimal? grandtotalselisih = 0;
            decimal? totalvalue = 0;
            decimal? grandtotalvalue = 0;
            List<GetReportStok> journallist = new List<GetReportStok>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblthOpnames.Where(p => p.kodelokasi == kodelokasi)
                              from td in db.tbltdOpnames.Where(p => p.NoFaktur == th.NoFaktur &&
                              p.kodebarang == itemcode)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              group new { th, td, b, u, l } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l.kodelokasi,
                                  fromname = l.nama,
                                  th.tgl,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  td.stok,
                                  td.harga,
                                  td.hpp,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tgl,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.stok,
                                  g.Key.harga,
                                  g.Key.hpp,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                if (itemcode.Equals(""))
                {
                    report = (from th in db.tblthOpnames.Where(p => p.kodelokasi == kodelokasi)
                              from td in db.tbltdOpnames.Where(p => p.NoFaktur == th.NoFaktur)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              group new { th, td, b, u, l } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l.kodelokasi,
                                  fromname = l.nama,
                                  th.tgl,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  td.stok,
                                  td.harga,
                                  td.hpp,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tgl,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.stok,
                                  g.Key.harga,
                                  g.Key.hpp,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                }
                string nofaktur = "";
                foreach (var reports in report)
                {
                    var resultreport = new GetReportStok
                    {
                        nofaktur = reports.NoFaktur,
                        nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                        fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                        fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                        tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                        keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                        satuan = reports.satuan,
                        kodebarang = reports.kodebarang,
                        namabarang = reports.nama,
                        qty = (int)reports.qty,
                        stok = (int)reports.stok,
                        selisih = (int)reports.qty - (int)reports.stok,
                        price = (double)reports.harga,
                        value = (double)reports.hpp,
                        userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                        username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                    };
                    string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                    {
                        if (!type.Equals("0"))
                        {
                            if (reports.NoFaktur.StartsWith(type))
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            nofaktur = reports.NoFaktur;
                        }
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table - bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial'> <thead> <tr>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Tanggal</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>No. Dokumen</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'></th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'></th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='8%'></th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'></th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'></th>"
                           + "</tr> </thead> <tbody>";
                for (int i = 0; i < journallist.Count; i++)
                {
                    int no = i + 1;
                    totalstok = totalstok + journallist[i].stok;
                    totalqty = totalqty + journallist[i].qty;
                    totalselisih = totalselisih + journallist[i].selisih;
                    totalvalue = totalvalue + (journallist[i].selisih * (int)journallist[i].price);
                    grandtotalstok = grandtotalstok + journallist[i].stok;
                    grandtotalqty = grandtotalqty + journallist[i].qty;
                    grandtotalselisih = grandtotalselisih + journallist[i].selisih;
                    grandtotalvalue = grandtotalvalue + (journallist[i].selisih * (int)journallist[i].price);
                    if (i == 0)
                    {
                        list = list + "<tr colspan='7'>"
                               + "<td style='background - color: #B8DBFD' colspan='7'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                            + "<th style='background - color: #B8DBFD;text-align:center'>Kode Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Nama Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'>Stok</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'>Qty</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='8%'>Selisih</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Harga</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Value</th>"
                           + "</tr>";
                        list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left' colspan='5'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                           + "</tr>";
                        list = list + "<tr colspan='7'>"
                               + "<td style='background - color: #B8DBFD' colspan='7'>&nbsp;</td>"
                               + "</tr>";
                    }
                    list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namabarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].stok + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].qty + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].selisih + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + Convert.ToDecimal(journallist[i].price).ToString("#,###") + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + Convert.ToDecimal((journallist[i].selisih * (int)journallist[i].price)).ToString("#,###") + "</td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr colspan='7'>"
                               + "<td style='background - color: #B8DBFD' colspan='7'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                            + "<th style='background - color: #B8DBFD;text-align:center'>Kode Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Nama Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'>Stok</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='5%'>Qty</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center' width='8%'>Selisih</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Harga</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Value</th>"
                           + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL STOK</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalstok + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqty + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalselisih + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (totalvalue == 0 ? "0" : Convert.ToDecimal(totalvalue).ToString("#,###")) + "</td>"
                               + "</tr>";
                        list = list + "<tr colspan='7'>"
                             + "<td style='background - color: #B8DBFD' colspan='7'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                             + "</tr>";
                        totalstok = 0;
                        totalqty = 0;
                        totalselisih = 0;
                        totalvalue = 0;
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left'>GRAND TOTAL STOK</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalstok + "</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalqty + "</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalselisih + "</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + Convert.ToDecimal(grandtotalvalue).ToString("#,###") + "</td>"
                          + "</tr>";
                        grandtotalstok = 0;
                        grandtotalqty = 0;
                        grandtotalselisih = 0;
                        grandtotalvalue = 0;
                    }
                    else
                    {
                        if (!journallist[i].nofaktur.Equals(journallist[i + 1].nofaktur))
                        {
                            list = list + "<tr colspan='7'>"
                                    + "<td style='background - color: #B8DBFD' colspan='7'>&nbsp;</td>"
                                    + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL STOK</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalstok + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqty + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalselisih + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (totalvalue == 0 ? "0" : Convert.ToDecimal(totalvalue).ToString("#,###")) + "</td>"
                               + "</tr>";
                            list = list + "<tr colspan='7'>"
                                 + "<td style='background - color: #B8DBFD' colspan='7'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                                 + "</tr>";
                            list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left' colspan='5'>" + (journallist[i + 1].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i + 1].keterangan) + "</td>"
                           + "</tr>";
                            list = list + "<tr colspan='7'>"
                                   + "<td style='background - color: #B8DBFD' colspan='7'>&nbsp;</td>"
                                   + "</tr>";
                            totalstok = 0;
                            totalqty = 0;
                            totalselisih = 0;
                            totalvalue = 0;
                        }
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Stok", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(fromdate + " s.d. " + todate + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Stok.pdf");
            }
        }

        public ActionResult ReportStokTemporary()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportStok report = new ReportStok();
            return View(report);
        }

        public JsonResult GetReportStokTemporary(string fromdate, string todate, string itemcode)
        {
            List<GetReportStok> journallist = new List<GetReportStok>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthOpnameTemps.Where(p => p.kodelokasi == kodelokasi)
                                  from td in db.tbltdOpnameTemps.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  group new { th, td, b, u, l } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      fromkode = l.kodelokasi,
                                      fromname = l.nama,
                                      th.tgl,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      td.stok,
                                      td.harga,
                                      td.hpp,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.fromkode,
                                      g.Key.fromname,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.stok,
                                      g.Key.harga,
                                      g.Key.hpp,
                                      g.Key.musernama,
                                      g.Key.muserid
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportStok
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                            fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            satuan = reports.satuan,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.qty,
                            stok = (int)reports.stok,
                            selisih = (int)reports.qty - (int)reports.stok,
                            price = (double)reports.harga,
                            value = (double)reports.hpp,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                        {
                            if (!itemcode.Equals(""))
                            {
                                if (itemcode.Equals(reports.kodebarang))
                                {
                                    journallist.Add(resultreport);
                                    nofaktur = reports.NoFaktur;
                                    i++;
                                }
                            }
                            else
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                                i++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public FileResult ExportReportStokTemporary(string fromdate, string todate, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? totalstok = 0;
            decimal? grandtotalstok = 0;
            decimal? totalqty = 0;
            decimal? grandtotalqty = 0;
            decimal? totalselisih = 0;
            decimal? grandtotalselisih = 0;
            decimal? totalvalue = 0;
            decimal? grandtotalvalue = 0;
            List<GetReportStok> journallist = new List<GetReportStok>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblthOpnameTemps.Where(p => p.kodelokasi == kodelokasi)
                              from td in db.tbltdOpnameTemps.Where(p => p.NoFaktur == th.NoFaktur)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                              from u in db.musers.Where(p => p.muserid == th.userid)
                              from l in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              group new { th, td, b, u, l } by new
                              {
                                  th.NoFaktur,
                                  fromkode = l.kodelokasi,
                                  fromname = l.nama,
                                  th.tgl,
                                  th.keterangan,
                                  b.satuan,
                                  b.kodebarang,
                                  b.nama,
                                  td.qty,
                                  td.stok,
                                  td.harga,
                                  td.hpp,
                                  u.musernama,
                                  u.muserid
                              }
                                   into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.fromkode,
                                  g.Key.fromname,
                                  g.Key.tgl,
                                  g.Key.keterangan,
                                  g.Key.satuan,
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.qty,
                                  g.Key.stok,
                                  g.Key.harga,
                                  g.Key.hpp,
                                  g.Key.musernama,
                                  g.Key.muserid
                              }).OrderBy(p => p.NoFaktur).ToList();
                string nofaktur = "";
                foreach (var reports in report)
                {
                    var resultreport = new GetReportStok
                    {
                        nofaktur = reports.NoFaktur,
                        nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                        fromcode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromkode,
                        fromname = reports.NoFaktur.Equals(nofaktur) ? "" : reports.fromname,
                        tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                        keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                        satuan = reports.satuan,
                        kodebarang = reports.kodebarang,
                        namabarang = reports.nama,
                        qty = (int)reports.qty,
                        stok = (int)reports.stok,
                        selisih = (int)reports.qty - (int)reports.stok,
                        price = (double)reports.harga,
                        value = (double)reports.hpp,
                        userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                        username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                    };
                    string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                    {
                        if (!itemcode.Equals(""))
                        {
                            if (itemcode.Equals(reports.kodebarang))
                            {
                                journallist.Add(resultreport);
                                nofaktur = reports.NoFaktur;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            nofaktur = reports.NoFaktur;
                        }
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table - bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial'> <thead> <tr>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Kode Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Nama Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Qty</th>"
                           + "</tr> </thead> <tbody>";
                for (int i = 0; i < journallist.Count; i++)
                {
                    int no = i + 1;
                    totalstok = totalstok + journallist[i].stok;
                    totalqty = totalqty + journallist[i].qty;
                    totalselisih = totalselisih + journallist[i].selisih;
                    totalvalue = totalvalue + (journallist[i].selisih * (int)journallist[i].price);
                    grandtotalstok = grandtotalstok + journallist[i].stok;
                    grandtotalqty = grandtotalqty + journallist[i].qty;
                    grandtotalselisih = grandtotalselisih + journallist[i].selisih;
                    grandtotalvalue = grandtotalvalue + (journallist[i].selisih * (int)journallist[i].price);
                    if (i == 0)
                    {
                        list = list + "<tr colspan='3'>"
                               + "<td style='background - color: #B8DBFD' colspan='3'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + (journallist[i].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i].keterangan) + "</td>"
                           + "</tr>";
                        list = list + "<tr colspan='3'>"
                               + "<td style='background - color: #B8DBFD' colspan='3'>&nbsp;</td>"
                               + "</tr>";
                    }
                    list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namabarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].qty + "</td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr colspan='3'>"
                               + "<td style='background - color: #B8DBFD' colspan='3'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL QTY</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqty + "</td>"
                               + "</tr>";
                        list = list + "<tr colspan='3'>"
                             + "<td style='background - color: #B8DBFD' colspan='3'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                             + "</tr>";
                        totalstok = 0;
                        totalqty = 0;
                        totalselisih = 0;
                        totalvalue = 0;
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left'>GRAND TOTAL QTY</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalqty + "</td>"
                          + "</tr>";
                        grandtotalstok = 0;
                        grandtotalqty = 0;
                        grandtotalselisih = 0;
                        grandtotalvalue = 0;
                    }
                    else
                    {
                        if (!journallist[i].nofaktur.Equals(journallist[i + 1].nofaktur))
                        {
                            list = list + "<tr colspan='3'>"
                                    + "<td style='background - color: #B8DBFD' colspan='3'>&nbsp;</td>"
                                    + "</tr>";
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left'>TOTAL QTY</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqty + "</td>"
                               + "</tr>";
                            list = list + "<tr colspan='3'>"
                                 + "<td style='background - color: #B8DBFD' colspan='3'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                                 + "</tr>";
                            list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].nobukti + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + (journallist[i + 1].keterangan.Length == 0 ? "" : "Keterangan : " + journallist[i + 1].keterangan) + "</td>"
                           + "</tr>";
                            list = list + "<tr colspan='3'>"
                                   + "<td style='background - color: #B8DBFD' colspan='3'>&nbsp;</td>"
                                   + "</tr>";
                            totalstok = 0;
                            totalqty = 0;
                            totalselisih = 0;
                            totalvalue = 0;
                        }
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Stok Temporary", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(fromdate + " s.d. " + todate + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Stok_Temporary.pdf");
            }
        }

        public ActionResult ReportStokAkhir()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportStokAkhir report = new ReportStokAkhir();
            return View(report);
        }

        public JsonResult GetReportStokAkhir(string date, string itemcode)
        {
            List<GetReportStokAkhir> journallist = new List<GetReportStokAkhir>();
            if (!date.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(date + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblKartuStoks.Where(p => p.kodelokasi == kodelokasi)
                                  from td in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == th.kodebarang)
                                  group new { th, td, b } by new
                                  {
                                      th.tgl,
                                      kodelokasi = td.kodelokasi,
                                      namalokasi = td.nama,
                                      kodebarang = b.kodebarang,
                                      namabarang = b.nama,
                                      b.satuan,
                                      th.qty
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.tgl,
                                      g.Key.kodelokasi,
                                      g.Key.namalokasi,
                                      g.Key.kodebarang,
                                      g.Key.namabarang,
                                      g.Key.satuan,
                                      g.Key.qty,
                                      stokakhir = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                                                p.tgl <= dtfrom && p.kodelokasi == g.Key.kodelokasi).Sum(p => p.qty)
                                  }).DistinctBy(p => p.kodebarang).OrderBy(p => p.kodebarang).ToList();
                    int i = 0;
                    int no = 1;
                    int stok = 0;
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportStokAkhir
                        {
                            no = no,
                            kodelokasi = reports.kodelokasi,
                            namalokasi = reports.namalokasi,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.namabarang,
                            satuan = reports.satuan,
                            stokakhir = (int)reports.stokakhir
                        };
                        if (!itemcode.Equals(""))
                        {
                            if (itemcode.Equals(reports.kodebarang))
                            {
                                journallist.Add(resultreport);
                                no++;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            no++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public FileResult ExportReportStokAkhir(string date, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? grandtotalstokakhir = 0;
            List<GetReportStokAkhir> journallist = new List<GetReportStokAkhir>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblKartuStoks.Where(p => p.kodelokasi == kodelokasi)
                              from td in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == th.kodebarang)
                              group new { th, td, b } by new
                              {
                                  th.tgl,
                                  kodelokasi = td.kodelokasi,
                                  namalokasi = td.nama,
                                  kodebarang = b.kodebarang,
                                  namabarang = b.nama,
                                  b.satuan,
                                  th.qty
                              }
                              into g
                              select new
                              {
                                  g.Key.tgl,
                                  g.Key.kodelokasi,
                                  g.Key.namalokasi,
                                  g.Key.kodebarang,
                                  g.Key.namabarang,
                                  g.Key.satuan,
                                  g.Key.qty,
                                  stokakhir = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                                                p.tgl <= dtfrom).Sum(p => p.qty)
                              }).DistinctBy(p => p.kodebarang).OrderBy(p => p.kodebarang).ToList();
                int no = 1;
                foreach (var reports in report)
                {
                    Regex reg = new Regex("[*'\",_&#^@]");
                    var resultreport = new GetReportStokAkhir
                    {
                        no = no,
                        kodelokasi = reports.kodelokasi,
                        namalokasi = reports.namalokasi,
                        kodebarang = reports.kodebarang,
                        namabarang = reg.Replace(reports.namabarang, string.Empty),
                        satuan = reports.satuan,
                        stokakhir = (int)reports.stokakhir
                    };
                    if (!itemcode.Equals(""))
                    {
                        if (itemcode.Equals(reports.kodebarang))
                        {
                            journallist.Add(resultreport);
                            no++;
                        }
                    }
                    else
                    {
                        journallist.Add(resultreport);
                        no++;
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table - bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial;border:1px solid black;'> <thead> <tr>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>No</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>Lokasi</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>Kode Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>Nama</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>Satuan</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center;border:1px solid black;'>Stok Akhir</th>"
                           + "</tr> </thead> <tbody>";
                for (int i = 0; i < journallist.Count; i++)
                {
                    grandtotalstokakhir = grandtotalstokakhir + journallist[i].stokakhir;
                    list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].no + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namalokasi + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left'>" + journallist[i].namabarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].satuan + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + journallist[i].stokakhir + "</td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr colspan='6'>"
                               + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr colspan='6'>"
                             + "<td style='background - color: #B8DBFD' colspan='6'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                             + "</tr>";
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'>GRAND TOTAL STOK AKHIR</td>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalstokakhir + "</td>"
                          + "</tr>";
                        grandtotalstokakhir = 0;
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Stok Akhir", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(date + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Stok_Akhir.pdf");
            }
        }

        public ActionResult ReportKartuStok()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportKartuStok report = new ReportKartuStok();
            report.kodelokasi = Session["kode_lokasi"].ToString();
            report.kategori = (from s in db.tblLokasiKategoris.Where(p => p.kodelokasi == report.kodelokasi)
                               from l in db.tblKategoris.Where(p => p.kodekategori == s.kodekategori)
                               select new GetKategoriClass
                               {
                                   kodekategori = l.kodekategori,
                                   namakategori = l.nama
                               }).ToList();
            return View(report);
        }

        public JsonResult GetReportKartuStok(string fromdate, string todate, string itemcode, string kodekategori)
        {
            List<GetReportKartuStok> journallist = new List<GetReportKartuStok>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var strdtfrom = dtfrom.ToString("MM/dd/yyyy");
                    var strdtto = dtto.ToString("MM/dd/yyyy");
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    string namalokasi = "";
                    var checkLokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                    if (checkLokasi != null)
                    {
                        namalokasi = checkLokasi.nama;
                    }
                    int no = 0;
                    Regex reg = new Regex("[*'\",_&#^@]");
                    string kodebarang = "";
                    int totalm = 0;
                    int totalk = 0;
                    var itemdb = (from e in db.tblHBarangs.Where(p => p.aktif == true)
                                  from k in db.tblKategoris.Where(p => p.kodekategori == e.kodekategori)
                                  from pr in db.tblProdusens.Where(p => p.kodeprodusen == e.kodeprodusen)
                                  from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier).DefaultIfEmpty()
                                  from ls in db.tblLokasiKategoris.Where(p => (p.kodelokasi == kodelokasi && p.kodekategori == e.kodekategori))
                                  from lp in db.tblLokasiProdusens.Where(p => (p.kodelokasi == kodelokasi && p.kodeprodusen == e.kodeprodusen))
                                  group new { e, k } by new
                                  {
                                      e.kodebarang,
                                      e.nama,
                                      k.kodekategori,
                                      namakategori = k.nama,
                                      pr.kodeprodusen,
                                      namaprodusen = pr.nama,
                                      e.itemcode
                                  }
                       into g
                                  select new
                                  {
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.kodekategori,
                                      g.Key.namakategori,
                                      g.Key.kodeprodusen,
                                      g.Key.namaprodusen,
                                      g.Key.itemcode
                                  }).ToList();
                    var checkCat = db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi).ToList();
                    var checkProd = db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi).ToList();
                    if (checkCat.Count == 0 && checkProd.Count == 0)
                    {
                        itemdb = (from e in db.tblHBarangs.Where(p => p.aktif == true)
                                  from k in db.tblKategoris.Where(p => p.kodekategori == e.kodekategori)
                                  from pr in db.tblProdusens.Where(p => p.kodeprodusen == e.kodeprodusen)
                                  from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier).DefaultIfEmpty()
                                  from ls in db.tblLokasiKategoris.Where(p => (p.kodelokasi == kodelokasi && p.kodekategori == e.kodekategori)).DefaultIfEmpty()
                                  from lp in db.tblLokasiProdusens.Where(p => (p.kodelokasi == kodelokasi && p.kodeprodusen == e.kodeprodusen)).DefaultIfEmpty()
                                  group new { e, k } by new
                                  {
                                      e.kodebarang,
                                      e.nama,
                                      k.kodekategori,
                                      namakategori = k.nama,
                                      pr.kodeprodusen,
                                      namaprodusen = pr.nama,
                                      e.itemcode
                                  }
                       into g
                                  select new
                                  {
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.kodekategori,
                                      g.Key.namakategori,
                                      g.Key.kodeprodusen,
                                      g.Key.namaprodusen,
                                      g.Key.itemcode
                                  }).ToList();
                    }
                    for (int i = 0; i < itemdb.Count; i++)
                    {
                        string kodebarangatas = itemdb[i].kodebarang;
                        //decimal? awal = db.tblKartuStoks.Where(p => p.kodebarang == kodebarangatas &&
                        //          p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty);
                        SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                        try
                        {
                            int saldo = 0;
                            SqlDataReader rdr = null;
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("sp_infokartustok", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@kodelokasi", kodelokasi));
                            cmd.Parameters.Add(new SqlParameter("@kodebarang", itemdb[i].kodebarang));
                            cmd.Parameters.Add(new SqlParameter("@tglawal", dtfrom));
                            cmd.Parameters.Add(new SqlParameter("@tglakhir", dtto));
                            rdr = cmd.ExecuteReader();
                            while (rdr.Read())
                            {
                                string kodebarangbawah = rdr["kodebarang"].ToString();
                                string cardcode = rdr["tgl"].ToString();
                                var resultreport = new GetReportKartuStok
                                {
                                    no = no,
                                    nofaktur = rdr["nofaktur"].ToString(),
                                    tgl = Convert.ToDateTime(rdr["tgl"].ToString()).ToString("dd-MMM-yyyy HH.mm"),
                                    modidate = Convert.ToDateTime(rdr["tgl"].ToString()).ToString("dd-MMM-yyyy HH.mm"),
                                    kodelokasi = kodelokasi,
                                    namalokasi = namalokasi,
                                    realkodebarang = rdr["kodebarang"].ToString(),
                                    realnamabarang = rdr["nama"].ToString(),
                                    kodebarang = rdr["kodebarang"].ToString(),
                                    namabarang = reg.Replace(rdr["nama"].ToString(), string.Empty),
                                    masuk = Convert.ToInt32(rdr["masuk"]),
                                    keluar = Convert.ToInt32(rdr["keluar"]),
                                    saldo = Convert.ToInt32(rdr["saldo"])
                                };
                                //string startshift = Convert.ToDateTime(rdr["tgl"].ToString()).ToString("yyyyMMdd");
                                //string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                                //string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                                if (!kodebarang.Equals(rdr["kodebarang"].ToString()))
                                {
                                    resultreport.stokawal = resultreport.saldo;
                                    resultreport.tgl = dtfrom.AddDays(-1).ToString("dd-MMM-yyyy HH.mm");
                                    resultreport.modidate = dtfrom.ToString("dd-MMM-yyyy HH.mm");
                                    resultreport.stokakhir = resultreport.stokawal;
                                    resultreport.headerawal = resultreport.stokakhir;
                                    resultreport.masuk = 0;
                                    //var sumakhir = db.tblKartuStoks.Where(p => p.kodebarang == kodebarangbawah &&
                                    //                                      p.kodelokasi == kodelokasi).ToList();
                                    //int dblakhir = 0;
                                    //for (int j = 0; j < sumakhir.Count; j++)
                                    //{
                                    //    string tglakhir = Convert.ToDateTime(sumakhir[j].tgl).ToString("yyyyMMdd");
                                    //    if (Int64.Parse(tglakhir) >= Int64.Parse(getdatefromfilter) && Int64.Parse(tglakhir) <= Int64.Parse(getdatetofilter))
                                    //    {
                                    //        dblakhir = dblakhir + (int)sumakhir[j].qty;
                                    //    }
                                    //}
                                    resultreport.headerakhir = 0;
                                    //totalm = 0;
                                    //totalm = (int)awal;
                                }
                                else
                                {
                                    resultreport.kodebarang = "";
                                    resultreport.namabarang = "";
                                    //resultreport.stokawal = (int)reports.awal + totalm;
                                    resultreport.stokawal = saldo;
                                    resultreport.stokakhir = resultreport.stokawal + Convert.ToInt32(rdr["masuk"]) - Convert.ToInt32(rdr["keluar"]);

                                }
                                kodebarang = rdr["kodebarang"].ToString();
                                saldo = Convert.ToInt32(rdr["saldo"]);
                                totalm = totalm + Convert.ToInt32(rdr["masuk"]) - Convert.ToInt32(rdr["keluar"]);
                                if (!itemcode.Equals(""))
                                {
                                    if (itemcode.Equals(rdr["kodebarang"].ToString()))
                                    {
                                        journallist.Add(resultreport);
                                    }
                                }
                                else
                                {
                                    journallist.Add(resultreport);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            string errormassage = e.Message;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                    //    var report = (from th in db.tblKartuStoks.Where(p => p.kodelokasi == kodelokasi && p.kodebarang == itemcode)
                    //                      //&& p.tgl <= dtto)
                    //                  from td in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                    //                  from b in db.tblHBarangs.Where(p => p.kodebarang == th.kodebarang && p.aktif == true)
                    //                  group new { th, td, b } by new
                    //                  {
                    //                      th.tgl,
                    //                      th.modidate,
                    //                      th.nofaktur,
                    //                      kodelokasi = td.kodelokasi,
                    //                      namalokasi = td.nama,
                    //                      kodebarang = b.kodebarang,
                    //                      namabarang = b.nama,
                    //                      th.qty,
                    //                      th.nourut,
                    //                      b.kodekategori,
                    //                      th.tipe
                    //                  }
                    //              into g
                    //                  select new
                    //                  {
                    //                      g.Key.tgl,
                    //                      g.Key.modidate,
                    //                      g.Key.nofaktur,
                    //                      g.Key.kodelokasi,
                    //                      g.Key.namalokasi,
                    //                      g.Key.kodebarang,
                    //                      g.Key.namabarang,
                    //                      g.Key.qty,
                    //                      g.Key.nourut,
                    //                      g.Key.kodekategori,
                    //                      g.Key.tipe,
                    //                      awal = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      p.kodelokasi == kodelokasi && p.tgl < dtfrom).ToList().Count() == 0 ? 0 :
                    //                      db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty)
                    //                      //awal = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      //p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty)
                    //                  }).OrderBy(p => p.kodebarang).ThenBy(p => p.tgl).ThenByDescending(p => p.tipe).ThenBy(p => p.nourut).ToList();
                    //    if (itemcode.Equals(""))
                    //    {
                    //        report = (from th in db.tblKartuStoks.Where(p => p.kodelokasi == kodelokasi)
                    //                          //&& p.tgl <= dtto)
                    //                  from td in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                    //                  from b in db.tblHBarangs.Where(p => p.kodebarang == th.kodebarang && p.aktif == true)
                    //                  group new { th, td, b } by new
                    //                  {
                    //                      th.tgl,
                    //                      th.modidate,
                    //                      th.nofaktur,
                    //                      kodelokasi = td.kodelokasi,
                    //                      namalokasi = td.nama,
                    //                      kodebarang = b.kodebarang,
                    //                      namabarang = b.nama,
                    //                      th.qty,
                    //                      th.nourut,
                    //                      b.kodekategori,
                    //                      th.tipe
                    //                  }
                    //              into g
                    //                  select new
                    //                  {
                    //                      g.Key.tgl,
                    //                      g.Key.modidate,
                    //                      g.Key.nofaktur,
                    //                      g.Key.kodelokasi,
                    //                      g.Key.namalokasi,
                    //                      g.Key.kodebarang,
                    //                      g.Key.namabarang,
                    //                      g.Key.qty,
                    //                      g.Key.nourut,
                    //                      g.Key.kodekategori,
                    //                      g.Key.tipe,
                    //                      awal = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      p.kodelokasi == kodelokasi && p.tgl < dtfrom).ToList().Count() == 0 ? 0 :
                    //                      db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty)
                    //                      //awal = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                    //                      //p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty)
                    //                  }).OrderBy(p => p.kodebarang).ThenBy(p => p.tgl).ThenByDescending(p => p.tipe).ThenBy(p => p.nourut).ToList();
                    //    }
                    //    if (!kodekategori.Equals("0"))
                    //    {
                    //        report = report.Where(p => p.kodekategori == kodekategori).ToList();
                    //    }
                    //    foreach (var reports in report)
                    //    {
                    //        no++;
                    //        var resultreport = new GetReportKartuStok
                    //        {
                    //            no = no,
                    //            nofaktur = reports.nofaktur,
                    //            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy HH.mm"),
                    //            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH.mm"),
                    //            kodelokasi = reports.kodelokasi,
                    //            namalokasi = reports.namalokasi,
                    //            realkodebarang = reports.kodebarang,
                    //            realnamabarang = reports.namabarang,
                    //            kodebarang = reports.kodebarang,
                    //            namabarang = reg.Replace(reports.namabarang, string.Empty),
                    //            masuk = (int)reports.qty > 0 ? (int)reports.qty : 0,
                    //            keluar = (int)reports.qty < 0 ? (-1 * (int)reports.qty) : 0
                    //        };
                    //        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    //        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    //        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    //        if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter) && Int64.Parse(startshift) <= Int64.Parse(getdatetofilter))
                    //        {
                    //            if (!kodebarang.Equals(reports.kodebarang))
                    //            {
                    //                resultreport.stokawal = (int)reports.awal;
                    //                resultreport.tgl = dtfrom.AddDays(-1).ToString("dd-MMM-yyyy HH.mm");
                    //                resultreport.modidate = dtfrom.ToString("dd-MMM-yyyy HH.mm");
                    //                resultreport.stokakhir = resultreport.stokawal +
                    //                    ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);
                    //                resultreport.headerawal = resultreport.stokakhir;
                    //                resultreport.masuk = 0;
                    //                var sumakhir = db.tblKartuStoks.Where(p => p.kodebarang == reports.kodebarang && p.kodelokasi == reports.kodelokasi).ToList();
                    //                int dblakhir = 0;
                    //                for (int i = 0; i < sumakhir.Count; i++)
                    //                {
                    //                    string tglakhir = Convert.ToDateTime(sumakhir[i].tgl).ToString("yyyyMMdd");
                    //                    if (Int64.Parse(tglakhir) >= Int64.Parse(getdatefromfilter) && Int64.Parse(tglakhir) <= Int64.Parse(getdatetofilter))
                    //                    {
                    //                        dblakhir = dblakhir + (int)sumakhir[i].qty;
                    //                    }
                    //                }
                    //                resultreport.headerakhir = dblakhir;
                    //                //totalm = 0;
                    //                totalm = (int)reports.awal;
                    //            }
                    //            else
                    //            {
                    //                resultreport.kodebarang = "";
                    //                resultreport.namabarang = "";
                    //                //resultreport.stokawal = (int)reports.awal + totalm;
                    //                resultreport.stokawal = totalm;
                    //                resultreport.stokakhir = resultreport.stokawal +
                    //                    ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);

                    //            }
                    //            kodebarang = reports.kodebarang;
                    //            totalm = totalm + ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);
                    //            if (!itemcode.Equals(""))
                    //            {
                    //                if (itemcode.Equals(reports.kodebarang))
                    //                {
                    //                    journallist.Add(resultreport);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                journallist.Add(resultreport);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (!kodebarang.Equals(reports.kodebarang))
                    //            {
                    //                resultreport.tgl = dtfrom.AddDays(-1).ToString("dd-MMM-yyyy");
                    //                resultreport.modidate = dtfrom.AddDays(-1).ToString("dd-MMM-yyyy HH:mm");
                    //                resultreport.stokawal = 0;
                    //                resultreport.masuk = 0;
                    //                resultreport.keluar = 0;
                    //                resultreport.stokakhir = (int)reports.awal;
                    //                resultreport.headerawal = (int)reports.awal;
                    //                var sumakhir = db.tblKartuStoks.Where(p => p.kodebarang == reports.kodebarang && p.kodelokasi == reports.kodelokasi).ToList();
                    //                int dblakhir = 0;
                    //                for (int i = 0; i < sumakhir.Count; i++)
                    //                {
                    //                    string tglakhir = Convert.ToDateTime(sumakhir[i].tgl).ToString("yyyyMMdd");
                    //                    if (Int64.Parse(tglakhir) >= Int64.Parse(getdatefromfilter) && Int64.Parse(tglakhir) <= Int64.Parse(getdatetofilter))
                    //                    {
                    //                        dblakhir = dblakhir + (int)sumakhir[i].qty;
                    //                    }
                    //                }
                    //                resultreport.headerakhir = resultreport.headerawal + dblakhir;
                    //                //totalm = 0;
                    //                totalm = (int)reports.awal;
                    //                if (!itemcode.Equals(""))
                    //                {
                    //                    if (itemcode.Equals(reports.kodebarang))
                    //                    {
                    //                        journallist.Add(resultreport);
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    journallist.Add(resultreport);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                resultreport.kodebarang = "";
                    //                resultreport.namabarang = "";
                    //                //resultreport.stokawal = (int)reports.awal + totalm;
                    //                resultreport.stokawal = totalm;
                    //                resultreport.stokakhir = resultreport.stokawal + ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);
                    //            }
                    //            kodebarang = reports.kodebarang;
                    //            //totalm = ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);
                    //            //totalm = totalm + ((int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty);
                    //        }
                    //    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public FileResult ExportReportKartuStok(string fromdate, string todate, string itemcode)
        {
            string GridHtml;
            string list = "";
            string storename = "";
            decimal? totalmasuk = 0;
            decimal? totalkeluar = 0;
            decimal? totalqtyakhir = 0;
            decimal? grandtotalmasuk = 0;
            decimal? grandtotalkeluar = 0;
            decimal? grandtotalqtyakhir = 0;
            List<GetReportKartuStok> journallist = new List<GetReportKartuStok>();
            using (MemoryStream stream = new System.IO.MemoryStream())
            {
                var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                string kodelokasi = Session["kode_lokasi"].ToString();
                var report = (from th in db.tblKartuStoks.Where(p => p.kodelokasi == kodelokasi)
                                  //&& p.tgl <= dtto)
                              from td in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                              from b in db.tblHBarangs.Where(p => p.kodebarang == th.kodebarang)
                              group new { th, td, b } by new
                              {
                                  th.tgl,
                                  th.nofaktur,
                                  kodelokasi = td.kodelokasi,
                                  namalokasi = td.nama,
                                  kodebarang = b.kodebarang,
                                  namabarang = b.nama,
                                  th.qty
                              }
                              into g
                              select new
                              {
                                  g.Key.tgl,
                                  g.Key.nofaktur,
                                  g.Key.kodelokasi,
                                  g.Key.namalokasi,
                                  g.Key.kodebarang,
                                  g.Key.namabarang,
                                  g.Key.qty,
                                  awal = db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                                  p.kodelokasi == kodelokasi && p.tgl < dtfrom).ToList().Count() == 0 ? 0 :
                                  db.tblKartuStoks.Where(p => p.kodebarang == g.Key.kodebarang &&
                                  p.kodelokasi == kodelokasi && p.tgl < dtfrom).Sum(p => p.qty)
                              }).OrderBy(p => p.kodebarang).ThenBy(p => p.tgl).ToList();
                string kodebarang = "";
                Regex reg = new Regex("[*'\",_&#^@]");
                foreach (var reports in report)
                {
                    var resultreport = new GetReportKartuStok
                    {
                        no = 1,
                        nofaktur = reports.nofaktur,
                        tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                        //tgl = reports.tgl.AddDays(-1).ToString("dd-MMM-yyyy"),
                        kodelokasi = reports.kodelokasi,
                        namalokasi = reports.namalokasi,
                        kodebarang = reports.kodebarang,
                        namabarang = reg.Replace(reports.namabarang, string.Empty),
                        masuk = (int)reports.qty > 0 ? (int)reports.qty : 0,
                        keluar = (int)reports.qty < 0 ? ((int)reports.qty) : 0,
                        stokakhir = (int)reports.qty < 0 ? ((int)reports.qty) : (int)reports.qty,
                        stokawal = (int)reports.awal
                    };
                    string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                    string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                    string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                    if (Int64.Parse(startshift) >= Int64.Parse(getdatefromfilter))
                    {
                        if (!itemcode.Equals(""))
                        {
                            if (itemcode.Equals(reports.kodebarang))
                            {
                                journallist.Add(resultreport);
                                kodebarang = reports.kodebarang;
                            }
                        }
                        else
                        {
                            journallist.Add(resultreport);
                            kodebarang = reports.kodebarang;
                        }
                    }
                }
                GridHtml = "<table id='dttest' class='table table-striped table - bordered' cellspacing='0' width='100%' style='font-size:13px;font-family:arial'> <thead> <tr>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Date</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Kode Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Nama Barang</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Masuk</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Keluar</th>"
                           + "<th style='background - color: #B8DBFD;text-align:center'>Stok Akhir</th>"
                           + "</tr> </thead> <tbody>";
                for (int i = 0; i < journallist.Count; i++)
                {
                    int no = i + 1;
                    totalmasuk = totalmasuk + journallist[i].masuk;
                    totalkeluar = totalkeluar + journallist[i].keluar;
                    totalqtyakhir = totalqtyakhir + journallist[i].stokakhir;
                    grandtotalmasuk = grandtotalmasuk + journallist[i].masuk;
                    grandtotalkeluar = grandtotalkeluar + journallist[i].keluar;
                    grandtotalqtyakhir = grandtotalqtyakhir + journallist[i].stokakhir;
                    if (i == 0)
                    {
                        list = list + "<tr colspan='6'>"
                               + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + journallist[i].kodebarang + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:left' colspan='4'>" + (journallist[i].namabarang.Length == 0 ? "" : journallist[i].namabarang) + "</td>"
                           + "</tr>";
                        list = list + "<tr colspan='6'>"
                               + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                               + "</tr>";
                        if (journallist[i].stokawal != 0)
                        {
                            list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>STOK AWAL</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i].stokawal < 0 ? "0" : Convert.ToDecimal(journallist[i].stokawal).ToString("#,###")) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i].stokawal > 0 ? "0" : Convert.ToDecimal(journallist[i].stokawal).ToString("#,###")) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'></td>"
                               + "</tr>";
                        }
                    }
                    list = list + "<tr>"
                           + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i].tgl + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + journallist[i].nofaktur + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i].masuk == 0 ? "0" : Convert.ToDecimal(journallist[i].masuk).ToString("#,###")) + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i].keluar == 0 ? "0" : Convert.ToDecimal(journallist[i].keluar).ToString("#,###")) + "</td>"
                           + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i].stokakhir == 0 ? "0" : Convert.ToDecimal(journallist[i].stokakhir).ToString("#,###")) + "</td>"
                           + "</tr>";
                    if (i == journallist.Count - 1)
                    {
                        list = list + "<tr colspan='6'>"
                               + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                               + "</tr>";
                        list = list + "<tr>"
                               + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                               + "<td style='background - color: #B8DBFD;text-align:left' colspan='2'>TOTAL STOK</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmasuk + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalkeluar + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqtyakhir + "</td>"
                               + "</tr>";
                        list = list + "<tr colspan='6'>"
                             + "<td style='background - color: #B8DBFD' colspan='6'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                             + "</tr>";
                        totalmasuk = 0;
                        totalkeluar = 0;
                        totalqtyakhir = 0;
                        list = list + "<tr>"
                          + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                          + "<td style='background - color: #B8DBFD;text-align:left' colspan='2'>GRAND TOTAL STOK</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalmasuk + "</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalkeluar + "</td>"
                          + "<td style='background - color: #B8DBFD;text-align:right'>" + grandtotalqtyakhir + "</td>"
                          + "</tr>";
                        grandtotalmasuk = 0;
                        grandtotalkeluar = 0;
                        grandtotalqtyakhir = 0;
                    }
                    else
                    {
                        if (!journallist[i].kodebarang.Equals(journallist[i + 1].kodebarang))
                        {
                            list = list + "<tr colspan='6'>"
                                   + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                                   + "</tr>";
                            list = list + "<tr>"
                                   + "<td style='background - color: #B8DBFD;text-align:center'></td>"
                                   + "<td style='background - color: #B8DBFD;text-align:left' colspan='2'>TOTAL STOK</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:right'>" + totalmasuk + "</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:right'>" + totalkeluar + "</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:right'>" + totalqtyakhir + "</td>"
                                   + "</tr>";
                            list = list + "<tr colspan='6'>"
                            + "<td style='background - color: #B8DBFD' colspan='6'>--------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>"
                            + "</tr>";
                            list = list + "<tr>"
                                   + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>" + journallist[i + 1].kodebarang + "</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:left' colspan='4'>" + (journallist[i + 1].namabarang.Length == 0 ? "" : journallist[i + 1].namabarang) + "</td>"
                                   + "</tr>";
                            list = list + "<tr colspan='6'>"
                                   + "<td style='background - color: #B8DBFD' colspan='6'>&nbsp;</td>"
                                   + "</tr>";
                            list = list + "<tr>"
                                   + "<td style='background - color: #B8DBFD;text-align:center'>" + journallist[i + 1].tgl + "</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:center' colspan='2'>STOK AWAL</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i + 1].stokawal < 0 ? "0" : Convert.ToDecimal(journallist[i + 1].stokawal).ToString("#,###")) + "</td>"
                               + "<td style='background - color: #B8DBFD;text-align:right'>" + (journallist[i + 1].stokawal > 0 ? "0" : Convert.ToDecimal(journallist[i + 1].stokawal).ToString("#,###")) + "</td>"
                                   + "<td style='background - color: #B8DBFD;text-align:right'></td>"
                                   + "</tr>";
                            totalmasuk = 0;
                            totalkeluar = 0;
                            totalqtyakhir = 0;
                        }
                    }
                }
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    storename = checklokasi.nama;
                }
                GridHtml = GridHtml + list + "</tbody> </table>";
                StringReader sr = new StringReader(GridHtml);
                Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.BLACK);
                Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 20f, 30f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                Paragraph title = new Paragraph("Laporan Kartu Stok", fdefault);
                title.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(title);
                Paragraph site = new Paragraph(storename, fdefault);
                site.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(site);
                Paragraph tgl = new Paragraph(fromdate + " s.d. " + todate + "\n\n", fdefault);
                tgl.Alignment = Element.ALIGN_CENTER;
                pdfDoc.Add(tgl);
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                //pdfDoc.Add(new Paragraph("*) Tanpa PPN", fdefault));
                pdfDoc.Close();
                return File(stream.ToArray(), "application/pdf", "Laporan_Kartu_Stok.pdf");
            }
        }


        public ActionResult ReportPenjualanItem()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportPenjualanItemClass report = new ReportPenjualanItemClass();
            report.kodelokasi = Session["kode_lokasi"].ToString();
            report.musers = db.musers.Where(p => p.kodelokasi == report.kodelokasi).ToList();
            var cashiers = db.musers.Where(p => p.kodelokasi == report.kodelokasi).Select(c => new {
                UserId = c.muserid,
                UserName = c.muserid + "-" + c.musernama
            }).ToList();
            ViewBag.Cashiers = new MultiSelectList(cashiers, "UserId", "UserName");
            report.kategori = (from s in db.tblLokasiKategoris.Where(p => p.kodelokasi == report.kodelokasi)
                               from l in db.tblKategoris.Where(p => p.kodekategori == s.kodekategori)
                               select new GetKategoriClass
                               {
                                   kodekategori = l.kodekategori,
                                   namakategori = l.nama
                               }).ToList();
            report.customers = (from s in db.tblLokasiCustomerGroups.Where(p => p.kodelokasi == report.kodelokasi)
                                from l in db.tblCustomers.Where(p => p.kodecustomergroup == s.kodecustomergroup)
                                select new GetCustomerClass
                                {
                                    kodecustomer = l.kodecustomer,
                                    namacustomer = l.nama
                                }).ToList();
            return View(report);
        }

        public JsonResult GetReportPenjualanItem(string fromdate, string todate, string itemcode, string userid, string kodekategori, string customer)
        {
            List<GetReportPenjualanItem> journallist = new List<GetReportPenjualanItem>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    this.db.Database.CommandTimeout = 300;
                    var report = (from th in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdPenjualans.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblCustomers.Where(p => p.kodecustomer == th.kodecustomer)
                                  group new { th, td, b } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      kodelokasi = l1.kodelokasi,
                                      namalokasi = l1.nama,
                                      th.tgl,
                                      th.tgljatuhtempo,
                                      kodecustomer = l2.kodecustomer,
                                      namacustomer = l2.nama,
                                      kodebarang = b.kodebarang,
                                      namabarang = b.nama,
                                      td.qty,
                                      td.harga,
                                      th.userid,
                                      b.kodekategori
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.kodelokasi,
                                      g.Key.namalokasi,
                                      g.Key.tgl,
                                      g.Key.tgljatuhtempo,
                                      g.Key.kodecustomer,
                                      g.Key.namacustomer,
                                      g.Key.kodebarang,
                                      g.Key.namabarang,
                                      g.Key.qty,
                                      g.Key.harga,
                                      g.Key.userid,
                                      g.Key.kodekategori
                                  }).OrderBy(p => p.namabarang).ToList();
                    if (!userid.Equals(""))
                    {
                        string[] useridpart = userid.Split(',');
                        var tampreport = report.Where(p => p.userid == "").ToList();
                        for (int j = 0; j < useridpart.Length; j++)
                        {
                            var groupreport = report.Where(p => p.userid.ToLower() == useridpart[j].ToLower()).ToList();
                            for (int k = 0; k < groupreport.Count; k++)
                            {
                                var soloreport = groupreport[k];
                                tampreport.Add(soloreport);
                            }
                        }
                        report = tampreport;
                    }
                    if (!kodekategori.Equals("0"))
                    {
                        report = report.Where(p => p.kodekategori == kodekategori).ToList();
                    }
                    if (!itemcode.Equals(""))
                    {
                        report = report.Where(p => p.kodebarang == itemcode).ToList();
                    }
                    if (!customer.Equals("0"))
                    {
                        report = report.Where(p => p.kodecustomer == customer).ToList();
                    }
                    int i = 0;
                    string kodebarang = "";
                    int subtotalqty = 0;
                    int totalqty = 0;
                    double subttotaltotal = 0;
                    double totaltotal = 0;
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportPenjualanItem
                        {
                            nofaktur = reports.NoFaktur,
                            kodelokasi = reports.kodelokasi,
                            namalokasi = reports.namalokasi,
                            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            tgljatuhtempo = Convert.ToDateTime(reports.tgljatuhtempo).ToString("dd-MMM-yyyy"),
                            kodecustomer = reports.kodecustomer,
                            namacustomer = reports.namacustomer,
                            kodebarang = reports.kodebarang.Equals(kodebarang) ? "" : reports.kodebarang,
                            namabarang = reports.kodebarang.Equals(kodebarang) ? "" : reports.namabarang,
                            qty = (int)reports.qty,
                            total = (int)reports.qty * (int)reports.harga,
                            strtotal = Convert.ToDecimal((int)reports.qty * (int)reports.harga).ToString("#.###")
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        if (!reports.kodebarang.Equals(kodebarang))
                        {
                            if (i != 0)
                            {
                                GetReportPenjualanItem getReport = new GetReportPenjualanItem();
                                getReport.namacustomer = "SubTotal";
                                getReport.qty = subtotalqty;
                                getReport.total = subttotaltotal;
                                getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                                journallist.Add(getReport);
                                subtotalqty = 0;
                                subttotaltotal = 0;
                                subtotalqty = subtotalqty + (int)reports.qty;
                                subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                                totalqty = totalqty + (int)reports.qty;
                                totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                            }
                            else
                            {
                                subtotalqty = subtotalqty + (int)reports.qty;
                                subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                                totalqty = totalqty + (int)reports.qty;
                                totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                            }
                        }
                        else
                        {
                            subtotalqty = subtotalqty + (int)reports.qty;
                            subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                            totalqty = totalqty + (int)reports.qty;
                            totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                        }
                        kodebarang = reports.kodebarang;
                        journallist.Add(resultreport);
                        i++;
                        if (i == report.Count)
                        {
                            GetReportPenjualanItem getReport = new GetReportPenjualanItem();
                            getReport.namacustomer = "SubTotal";
                            getReport.qty = subtotalqty;
                            getReport.total = subttotaltotal;
                            getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                            journallist.Add(getReport);
                            GetReportPenjualanItem getReport2 = new GetReportPenjualanItem();
                            getReport2.namacustomer = "Total";
                            getReport2.qty = totalqty;
                            getReport2.total = totaltotal;
                            getReport2.strtotal = Convert.ToDecimal(totaltotal).ToString("#.###");
                            journallist.Add(getReport2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            //return Json(journallist, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult ReportPenjualanInvoice()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportPenjualanInvoiceClass report = new ReportPenjualanInvoiceClass();
            report.kodelokasi = Session["kode_lokasi"].ToString();
            report.kategori = (from s in db.tblLokasiKategoris.Where(p => p.kodelokasi == report.kodelokasi)
                               from l in db.tblKategoris.Where(p => p.kodekategori == s.kodekategori)
                               select new GetKategoriClass
                               {
                                    kodekategori = l.kodekategori,
                                    namakategori = l.nama
                               }).ToList();
            report.customers = (from s in db.tblLokasiCustomerGroups.Where(p => p.kodelokasi == report.kodelokasi)
                                from l in db.tblCustomers.Where(p => p.kodecustomergroup == s.kodecustomergroup)
                                select new GetCustomerClass
                                {
                                    kodecustomer = l.kodecustomer,
                                    namacustomer = l.nama
                                }).ToList();
            report.musers = db.musers.Where(p => p.kodelokasi == report.kodelokasi).ToList();
            var cashiers = db.musers.Where(p => p.kodelokasi == report.kodelokasi).Select(c => new {
                UserId = c.muserid,
                UserName = c.muserid + "-" + c.musernama
            }).ToList();
            ViewBag.Cashiers = new MultiSelectList(cashiers, "UserId", "UserName");
            return View(report);
        }

        public JsonResult GetReportPenjualanInvoice(string fromdate, string todate, string itemcode, string userid, string kodekategori, string customer)
        {
            List<GetReportPenjualanInvoice> journallist = new List<GetReportPenjualanInvoice>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    this.db.Database.CommandTimeout = 300;
                    var report = (from th in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdPenjualans.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblCustomers.Where(p => p.kodecustomer == th.kodecustomer)
                                  group new { th, td, b } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      kodelokasi = l1.kodelokasi,
                                      namalokasi = l1.nama,
                                      th.tgl,
                                      th.tgljatuhtempo,
                                      kodecustomer = l2.kodecustomer,
                                      namacustomer = l2.nama,
                                      kodebarang = b.kodebarang,
                                      namabarang = b.nama,
                                      td.qty,
                                      td.harga,
                                      td.disc,
                                      th.userid,
                                      b.kodekategori
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.kodelokasi,
                                      g.Key.namalokasi,
                                      g.Key.tgl,
                                      g.Key.tgljatuhtempo,
                                      g.Key.kodecustomer,
                                      g.Key.namacustomer,
                                      g.Key.kodebarang,
                                      g.Key.namabarang,
                                      g.Key.qty,
                                      g.Key.harga,
                                      g.Key.disc,
                                      g.Key.userid,
                                      g.Key.kodekategori,
                                      countbayar = db.tblPenjualanBayars.Where(p => p.nofaktur == g.Key.NoFaktur).ToList()
                                  }).OrderByDescending(p => p.NoFaktur).ToList();
                    if (!userid.Equals(""))
                    {
                        string[] useridpart = userid.Split(',');
                        var tampreport = report.Where(p => p.userid == "").ToList();
                        for (int j = 0; j < useridpart.Length; j++)
                        {
                            var groupreport = report.Where(p => p.userid.ToUpper().Trim() == useridpart[j]).ToList();
                            for (int k = 0; k < groupreport.Count; k++)
                            {
                                var soloreport = groupreport[k];
                                tampreport.Add(soloreport);
                            }
                        }
                        report = tampreport;
                    }
                    if (!kodekategori.Equals("0"))
                    {
                        report = report.Where(p => p.kodekategori == kodekategori).ToList();
                    }
                    if (!itemcode.Equals(""))
                    {
                        report = report.Where(p => p.kodebarang == itemcode).ToList();
                    }
                    if (!customer.Equals("0"))
                    {
                        report = report.Where(p => p.kodecustomer == customer).ToList();
                    }
                    int i = 0;
                    int no = 1;
                    string nofaktur = "";
                    int subtotalqty = 0;
                    int totalqty = 0;
                    double subttotaltotal = 0;
                    double totaltotal = 0;
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportPenjualanInvoice
                        {
                            nofaktur = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            kodepembayaran = reports.countbayar.Count > 1 ? "Split" : reports.countbayar.FirstOrDefault().kodepembayaran,
                            namapembayaran = reports.countbayar.Count > 1 ? "Split" : reports.countbayar.FirstOrDefault().kodepembayaran,
                            kodelokasi = reports.NoFaktur.Equals(nofaktur) ? "" : reports.kodelokasi,
                            namalokasi = reports.NoFaktur.Equals(nofaktur) ? "" : reports.namalokasi,
                            modidate = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            tgljatuhtempo = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgljatuhtempo).ToString("dd-MMM-yyyy"),
                            kodecustomer = reports.NoFaktur.Equals(nofaktur) ? "" : reports.kodecustomer,
                            namacustomer = reports.NoFaktur.Equals(nofaktur) ? "" : reports.namacustomer,
                            kodebarang = reports.kodebarang,
                            userid = reports.userid,
                            namabarang = reports.namabarang,
                            disc = (double)reports.disc,
                            price = (double)reports.harga,
                            strprice = Convert.ToDecimal((double)reports.harga).ToString("#.###"),
                            qty = (int)reports.qty,
                            total = (int)reports.qty * (int)reports.harga,
                            strtotal = Convert.ToDecimal((int)reports.qty * (int)reports.harga).ToString("#.###")
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        string getdatetofilter = Convert.ToDateTime(dtto).ToString("yyyyMMdd");
                        if (!reports.NoFaktur.Equals(nofaktur))
                        {
                            if (i != 0)
                            {
                                no++;
                                GetReportPenjualanInvoice getReport = new GetReportPenjualanInvoice();
                                getReport.namacustomer = "SubTotal";
                                getReport.qty = subtotalqty;
                                getReport.total = subttotaltotal;
                                getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                                journallist.Add(getReport);
                                subtotalqty = 0;
                                subttotaltotal = 0;
                                subtotalqty = subtotalqty + (int)reports.qty;
                                subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                                totalqty = totalqty + (int)reports.qty;
                                totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                            }
                            else
                            {
                                subtotalqty = subtotalqty + (int)reports.qty;
                                subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                                totalqty = totalqty + (int)reports.qty;
                                totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                            }
                        }
                        else
                        {
                            subtotalqty = subtotalqty + (int)reports.qty;
                            subttotaltotal = subttotaltotal + (double)((int)reports.qty * (int)reports.harga);
                            totalqty = totalqty + (int)reports.qty;
                            totaltotal = totaltotal + (double)((int)reports.qty * (int)reports.harga);
                        }
                        resultreport.strno = reports.NoFaktur.Equals(nofaktur) ? "" : no.ToString();
                        nofaktur = reports.NoFaktur;
                        journallist.Add(resultreport);
                        i++;
                        if (i == report.Count)
                        {
                            no++;
                            GetReportPenjualanInvoice getReport = new GetReportPenjualanInvoice();
                            getReport.namacustomer = "SubTotal";
                            getReport.qty = subtotalqty;
                            getReport.total = subttotaltotal;
                            getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                            journallist.Add(getReport);
                            GetReportPenjualanInvoice getReport2 = new GetReportPenjualanInvoice();
                            getReport2.namacustomer = "Total";
                            getReport2.qty = totalqty;
                            getReport2.total = totaltotal;
                            getReport2.strtotal = Convert.ToDecimal(totaltotal).ToString("#.###");
                            journallist.Add(getReport2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            //return Json(journallist, JsonRequestBehavior.AllowGet);
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult getItemFromStoreReport()
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi && p.kodeprodusen == m.kodeprodusen)
                          group new { m, l1, s, ls, lp } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              m.kodebarang,
                              m.nama,
                              l1.kodekategori,
                              namakategori = l1.nama,
                              //pr.kodeprodusen,
                              //namaprodusen = pr.nama,
                              m.hargajual,
                              m.satuan,
                              m.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodeprodusen,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode
                          }).ToList().OrderBy(p => p.nama);
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    //kodeprodusen = itemdbs.kodeprodusen,
                    //namaprodusen = itemdbs.namaprodusen,
                    //stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    satuan = itemdbs.satuan,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public ActionResult ReportPenjualanByHour()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportPenjualanByHourClass report = new ReportPenjualanByHourClass();
            return View(report);
        }

        public JsonResult GetReportPenjualanByHour(string fromdate, string todate)
        {
            List<GetReportPenjualanByHour> journallist = new List<GetReportPenjualanByHour>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var spdashboard = db.sp_dashboardsales(dtfrom, dtto, kodelokasi, false, "");
                    SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                    //SqlConnection conn = new SqlConnection("Data Source=116.254.101.239,3396;Initial Catalog=POSVIT_JT;Persist Security Info=True;User ID=sa;Password=P@ssw0rd");
                    SqlDataReader rdr = null;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_dashboardsales", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@startdate", dtfrom.ToString("MM-dd-yyyy")));
                    cmd.Parameters.Add(new SqlParameter("@enddate", dtto.ToString("MM-dd-yyyy")));
                    cmd.Parameters.Add(new SqlParameter("@kodelokasi", kodelokasi));
                    cmd.Parameters.Add(new SqlParameter("@bol622", 0));
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        GetReportPenjualanByHour solopo = new GetReportPenjualanByHour();
                        solopo.startdate = fromdate;
                        solopo.enddate = todate;
                        solopo.kodelokasi = rdr["kodelokasi"].ToString();
                        solopo.namalokasi = rdr["lokasi"].ToString();
                        solopo.kategori = rdr["kategori"].ToString();
                        solopo.salesqty1 = Convert.ToInt32(rdr["salesqty1"]);
                        solopo.salesqty2 = Convert.ToInt32(rdr["salesqty2"]);
                        solopo.salesqty3 = Convert.ToInt32(rdr["salesqty3"]);
                        solopo.salesqty4 = Convert.ToInt32(rdr["salesqty4"]);
                        solopo.salesqty5 = Convert.ToInt32(rdr["salesqty5"]);
                        solopo.salesqty6 = Convert.ToInt32(rdr["salesqty6"]);
                        solopo.salesqty7 = Convert.ToInt32(rdr["salesqty7"]);
                        solopo.salesqty8 = Convert.ToInt32(rdr["salesqty8"]);
                        solopo.salesqty9 = Convert.ToInt32(rdr["salesqty9"]);
                        solopo.salesqty10 = Convert.ToInt32(rdr["salesqty10"]);
                        solopo.salesqty11 = Convert.ToInt32(rdr["salesqty11"]);
                        solopo.salesqty12 = Convert.ToInt32(rdr["salesqty12"]);
                        solopo.salesqty13 = Convert.ToInt32(rdr["salesqty13"]);
                        solopo.salesqty14 = Convert.ToInt32(rdr["salesqty14"]);
                        solopo.salesqty15 = Convert.ToInt32(rdr["salesqty15"]);
                        solopo.salesqty16 = Convert.ToInt32(rdr["salesqty16"]);
                        solopo.salesqty17 = Convert.ToInt32(rdr["salesqty17"]);
                        solopo.salesqty18 = Convert.ToInt32(rdr["salesqty18"]);
                        solopo.salesqty19 = Convert.ToInt32(rdr["salesqty19"]);
                        solopo.salesqty20 = Convert.ToInt32(rdr["salesqty20"]);
                        solopo.salesqty21 = Convert.ToInt32(rdr["salesqty21"]);
                        solopo.salesqty22 = Convert.ToInt32(rdr["salesqty22"]);
                        solopo.salesqty23 = Convert.ToInt32(rdr["salesqty23"]);
                        solopo.salesqty24 = Convert.ToInt32(rdr["salesqty24"]);
                        solopo.salesnominal1 = Convert.ToDouble(rdr["salesnominal1"]);
                        solopo.salesnominal2 = Convert.ToDouble(rdr["salesnominal2"]);
                        solopo.salesnominal3 = Convert.ToDouble(rdr["salesnominal3"]);
                        solopo.salesnominal4 = Convert.ToDouble(rdr["salesnominal4"]);
                        solopo.salesnominal5 = Convert.ToDouble(rdr["salesnominal5"]);
                        solopo.salesnominal6 = Convert.ToDouble(rdr["salesnominal6"]);
                        solopo.salesnominal7 = Convert.ToDouble(rdr["salesnominal7"]);
                        solopo.salesnominal8 = Convert.ToDouble(rdr["salesnominal8"]);
                        solopo.salesnominal9 = Convert.ToDouble(rdr["salesnominal9"]);
                        solopo.salesnominal10 = Convert.ToDouble(rdr["salesnominal10"]);
                        solopo.salesnominal11 = Convert.ToDouble(rdr["salesnominal11"]);
                        solopo.salesnominal12 = Convert.ToDouble(rdr["salesnominal12"]);
                        solopo.salesnominal13 = Convert.ToDouble(rdr["salesnominal13"]);
                        solopo.salesnominal14 = Convert.ToDouble(rdr["salesnominal14"]);
                        solopo.salesnominal15 = Convert.ToDouble(rdr["salesnominal15"]);
                        solopo.salesnominal16 = Convert.ToDouble(rdr["salesnominal16"]);
                        solopo.salesnominal17 = Convert.ToDouble(rdr["salesnominal17"]);
                        solopo.salesnominal18 = Convert.ToDouble(rdr["salesnominal18"]);
                        solopo.salesnominal19 = Convert.ToDouble(rdr["salesnominal19"]);
                        solopo.salesnominal20 = Convert.ToDouble(rdr["salesnominal20"]);
                        solopo.salesnominal21 = Convert.ToDouble(rdr["salesnominal21"]);
                        solopo.salesnominal22 = Convert.ToDouble(rdr["salesnominal22"]);
                        solopo.salesnominal23 = Convert.ToDouble(rdr["salesnominal23"]);
                        solopo.salesnominal24 = Convert.ToDouble(rdr["salesnominal24"]);
                        solopo.totalqty = solopo.salesqty1 + solopo.salesqty2 + solopo.salesqty3 + solopo.salesqty4 + solopo.salesqty5 +
                            solopo.salesqty6 + solopo.salesqty7 + solopo.salesqty8 + solopo.salesqty9 + solopo.salesqty10 + solopo.salesqty11 +
                            solopo.salesqty12 + solopo.salesqty13 + solopo.salesqty14 + solopo.salesqty15 + solopo.salesqty16 + solopo.salesqty17 +
                            solopo.salesqty18 + solopo.salesqty19 + solopo.salesqty20 + solopo.salesqty21 + solopo.salesqty22 + solopo.salesqty23 +
                            solopo.salesqty24;
                        solopo.strtotalqty = solopo.totalqty == 0 ? "0" : Convert.ToDecimal(solopo.totalqty).ToString("#,###");
                        solopo.totalamount = solopo.salesnominal1 + solopo.salesnominal2 + solopo.salesnominal3 + solopo.salesnominal4 +
                            solopo.salesnominal5 + solopo.salesnominal6 + solopo.salesnominal7 + solopo.salesnominal8 + solopo.salesnominal9 +
                            solopo.salesnominal10 + solopo.salesnominal11 + solopo.salesnominal12 + solopo.salesnominal13 + solopo.salesnominal14 +
                            solopo.salesnominal15 + solopo.salesnominal16 + solopo.salesnominal17 + solopo.salesnominal18 + solopo.salesnominal19 +
                            solopo.salesnominal20 + solopo.salesnominal21 + solopo.salesnominal22 + solopo.salesnominal23 + solopo.salesnominal24;
                        solopo.strtotalamount = solopo.totalamount == 0 ? "0" : Convert.ToDecimal(solopo.totalamount).ToString("#,###");
                        journallist.Add(solopo);
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportPaymentSummary()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            ReportPaymentSummary reportMutasiStokLokasi = new ReportPaymentSummary();
            reportMutasiStokLokasi.musers = db.musers.Where(p => p.kodelokasi == kodelokasi).ToList();
            var cashiers = db.musers.Where(p => p.kodelokasi == kodelokasi).Select(c => new {
                UserId = c.muserid,
                UserName = c.muserid + "-" + c.musernama
            }).ToList();
            ViewBag.Cashiers = new MultiSelectList(cashiers, "UserId", "UserName");
            return View(reportMutasiStokLokasi);
        }

        public JsonResult GetReportPaymentSummary(string fromdate, string userid)
        {
            List<GetReportPaymentSummary> journallist = new List<GetReportPaymentSummary>();
            if (!fromdate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && (p.tgl.Day == dtfrom.Day && p.tgl.Month == dtfrom.Month &&
                                  p.tgl.Year == dtfrom.Year))
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblCustomers.Where(p => p.kodecustomer == th.kodecustomer)
                                  from pb in db.tblPenjualanBayars.Where(p => p.nofaktur == th.NoFaktur)
                                  from p in db.tblPembayarans.Where(p => p.kodepembayaran == pb.kodepembayaran)
                                  group new { th } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      kodelokasi = l1.kodelokasi,
                                      namalokasi = l1.nama,
                                      th.tgl,
                                      th.tgljatuhtempo,
                                      kodecustomer = l2.kodecustomer,
                                      namacustomer = l2.nama,
                                      kodepembayaran = p.kodepembayaran,
                                      namapembayaran = p.nama,
                                      pb.nominal,
                                      th.userid
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.modidate,
                                      g.Key.kodelokasi,
                                      g.Key.namalokasi,
                                      g.Key.tgl,
                                      g.Key.tgljatuhtempo,
                                      g.Key.kodecustomer,
                                      g.Key.namacustomer,
                                      g.Key.kodepembayaran,
                                      g.Key.namapembayaran,
                                      g.Key.nominal,
                                      g.Key.userid,
                                      qty = db.tbltdPenjualans.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                                  }).OrderBy(p => p.namapembayaran).ToList();
                    if (!userid.Equals(""))
                    {
                        string[] useridpart = userid.Split(',');
                        var tampreport = report.Where(p => p.userid == "").ToList();
                        for (int j = 0; j < useridpart.Length; j++)
                        {
                            var groupreport = report.Where(p => p.userid.ToLower() == useridpart[j].ToLower()).ToList();
                            for (int k = 0; k < groupreport.Count; k++)
                            {
                                var soloreport = groupreport[k];
                                tampreport.Add(soloreport);
                            }
                        }
                        report = tampreport;
                    }
                    int i = 0;
                    string kodepembayaran = "";
                    int subtotalqty = 0;
                    int totalqty = 0;
                    double subttotaltotal = 0;
                    double totaltotal = 0;
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportPaymentSummary
                        {
                            nofaktur = reports.NoFaktur,
                            kodelokasi = reports.kodelokasi,
                            namalokasi = reports.namalokasi,
                            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            tgljatuhtempo = Convert.ToDateTime(reports.tgljatuhtempo).ToString("dd-MMM-yyyy"),
                            kodecustomer = reports.kodecustomer,
                            namacustomer = reports.namacustomer,
                            qty = 1,
                            total = (int)reports.nominal,
                            strtotal = Convert.ToDecimal((int)reports.nominal).ToString("#.###"),
                            kodepembayaran = reports.kodepembayaran.Equals(kodepembayaran) ? "" : reports.kodepembayaran,
                            namapembayaran = reports.kodepembayaran.Equals(kodepembayaran) ? "" : reports.namapembayaran
                        };
                        string startshift = Convert.ToDateTime(reports.tgl).ToString("yyyyMMdd");
                        string getdatefromfilter = Convert.ToDateTime(dtfrom).ToString("yyyyMMdd");
                        if (!reports.kodepembayaran.Equals(kodepembayaran))
                        {
                            if (i != 0)
                            {
                                GetReportPaymentSummary getReport = new GetReportPaymentSummary();
                                getReport.namacustomer = "SubTotal";
                                getReport.qty = subtotalqty;
                                getReport.total = subttotaltotal;
                                getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                                journallist.Add(getReport);
                                subtotalqty = 0;
                                subttotaltotal = 0;
                                subtotalqty = subtotalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                                subttotaltotal = subttotaltotal + (double)reports.nominal;
                                totalqty = totalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                                totaltotal = totaltotal + (double)reports.nominal;
                            }
                            else
                            {
                                subtotalqty = subtotalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                                subttotaltotal = subttotaltotal + (double)reports.nominal;
                                totalqty = totalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                                totaltotal = totaltotal + (double)reports.nominal;
                            }
                        }
                        else
                        {
                            subtotalqty = subtotalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                            subttotaltotal = subttotaltotal + (double)reports.nominal;
                            totalqty = totalqty + (int)(reports.qty.Count == 0 ? 0 : reports.qty.Sum(p => p.qty));
                            totaltotal = totaltotal + (double)reports.nominal;
                        }
                        kodepembayaran = reports.kodepembayaran;
                        journallist.Add(resultreport);
                        i++;
                        if (i == report.Count)
                        {
                            GetReportPaymentSummary getReport = new GetReportPaymentSummary();
                            getReport.namacustomer = "SubTotal";
                            getReport.qty = subtotalqty;
                            getReport.total = subttotaltotal;
                            getReport.strtotal = Convert.ToDecimal(subttotaltotal).ToString("#.###");
                            journallist.Add(getReport);
                            GetReportPaymentSummary getReport2 = new GetReportPaymentSummary();
                            getReport2.namacustomer = "Total";
                            getReport2.qty = totalqty;
                            getReport2.total = totaltotal;
                            getReport2.strtotal = Convert.ToDecimal(totaltotal).ToString("#.###");
                            journallist.Add(getReport2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportEndofShift()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            ReportEndofShift reportMutasiStokLokasi = new ReportEndofShift();
            reportMutasiStokLokasi.musers = db.musers.Where(p => p.kodelokasi == kodelokasi).ToList();
            return View(reportMutasiStokLokasi);
        }

        public JsonResult GetReportEndofShift(string fromdate, string todate, string userid)
        {
            List<GetReportEndofShift> journallist = new List<GetReportEndofShift>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblSettlementEndofShifts.Where(p => p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from l1 in db.musers.Where(p => p.muserid == th.userid)
                                  from p in db.tblPembayarans.Where(p => p.kodepembayaran == th.kodepembayaran)
                                  from sa in db.tblSaldoAwalShifts.Where(p => p.idshift == th.idshift)
                                  group new { th } by new
                                  {
                                      l1.muserid,
                                      l1.musernama,
                                      saldoawal = sa.nominal,
                                      p.kodepembayaran,
                                      p.nama,
                                      nominalsystem = th.nominal,
                                      nominalfisik = th.fisik,
                                      th.beritaacara,
                                      th.tgl,
                                      th.startdate,
                                      sa.idshift,
                                      sa.startshift,
                                      th.userid
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.muserid,
                                      g.Key.musernama,
                                      g.Key.saldoawal,
                                      g.Key.kodepembayaran,
                                      g.Key.nama,
                                      g.Key.nominalsystem,
                                      g.Key.nominalfisik,
                                      g.Key.beritaacara,
                                      g.Key.tgl,
                                      g.Key.startdate,
                                      g.Key.idshift,
                                      g.Key.startshift,
                                      g.Key.userid
                                  }).OrderBy(p => p.idshift).ToList();
                    if (!userid.Equals("0"))
                    {
                        report = report.Where(p => p.userid.ToLower() == userid.ToLower()).ToList();
                    }
                    int i = 0;
                    long idshift = 0;
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportEndofShift
                        {
                            userid = reports.muserid,
                            username = reports.musernama,
                            saldoawal = (double)reports.saldoawal,
                            strsaldoawal = Convert.ToDecimal((double)reports.saldoawal).ToString("#,###"),
                            kodepembayaran = reports.kodepembayaran,
                            namapembayaran = reports.nama,
                            nominalfisik = (double)reports.nominalfisik,
                            strnominalfisik = Convert.ToDecimal((double)reports.nominalfisik).ToString("#.###"),
                            nominalsystem = (double)reports.nominalsystem,
                            strnominalsystem = Convert.ToDecimal((double)reports.nominalsystem).ToString("#.###"),
                            nominaldiff = (double)reports.nominalfisik - (double)reports.nominalsystem,
                            strnominaldiff = Convert.ToDecimal((double)reports.nominalfisik - (double)reports.nominalsystem).ToString("#.###"),
                            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy HH:mm"),
                            startshift = Convert.ToDateTime(reports.startshift).ToString("dd-MMM-yyyy HH:mm.ss"),
                            idshift = reports.idshift
                        };
                        idshift = reports.idshift;
                        journallist.Add(resultreport);
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportEndofDay()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            ReportEndofDay reportMutasiStokLokasi = new ReportEndofDay();
            return View(reportMutasiStokLokasi);
        }

        public JsonResult GetReportEndofDay(string fromdate, string todate)
        {
            List<GetReportEndofDay> journallist = new List<GetReportEndofDay>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblSettlements.Where(p => p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from p in db.tblPembayarans.Where(p => p.kodepembayaran == th.kodepembayaran)
                                  group new { th, p } by new
                                  {
                                      p.kodepembayaran,
                                      p.nama,
                                      nominalsystem = th.nominal,
                                      nominalfisik = th.fisik,
                                      th.beritaacara,
                                      th.tgl
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.kodepembayaran,
                                      g.Key.nama,
                                      g.Key.nominalsystem,
                                      g.Key.nominalfisik,
                                      g.Key.beritaacara,
                                      g.Key.tgl
                                  }).OrderBy(p => p.tgl).ToList();
                    int i = 0;
                    string date = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportEndofDay
                        {
                            kodepembayaran = reports.kodepembayaran,
                            namapembayaran = reports.nama,
                            beritaacara = reports.beritaacara,
                            nominalfisik = (double)reports.nominalfisik,
                            strnominalfisik = Convert.ToDecimal((double)reports.nominalfisik).ToString("#.###"),
                            nominalsystem = (double)reports.nominalsystem,
                            strnominalsystem = Convert.ToDecimal((double)reports.nominalsystem).ToString("#.###"),
                            nominaldiff = (int)reports.nominalfisik - (int)reports.nominalsystem,
                            strnominaldiff = Convert.ToDecimal((int)reports.nominalfisik - (int)reports.nominalsystem).ToString("#.###"),
                            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy")
                        };
                        date = resultreport.tgl;
                        journallist.Add(resultreport);
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportTransferRequest()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            ReportTransferRequest reportMutasiStokLokasi = new ReportTransferRequest();
            reportMutasiStokLokasi.lokasis = db.tblLokasis.Where(p => p.InTransit != null && p.InTransit != "" && p.aktif == true).ToList();
            reportMutasiStokLokasi.statuses = new List<GetStatusReportTransferRequest>();
            GetStatusReportTransferRequest getStatus = new GetStatusReportTransferRequest();
            getStatus.kodestatus = "0";
            getStatus.namastatus = "Open";
            reportMutasiStokLokasi.statuses.Add(getStatus);
            getStatus = new GetStatusReportTransferRequest();
            getStatus.kodestatus = "1";
            getStatus.namastatus = "Close";
            reportMutasiStokLokasi.statuses.Add(getStatus);
            reportMutasiStokLokasi.tipes = new List<GetTypeReportTransferRequest>();
            GetTypeReportTransferRequest gettipe = new GetTypeReportTransferRequest();
            gettipe.kodetipe = "1";
            gettipe.namatipe = "TR From This Location";
            reportMutasiStokLokasi.tipes.Add(gettipe);
            gettipe = new GetTypeReportTransferRequest();
            gettipe.kodetipe = "2";
            gettipe.namatipe = "TR For This Location";
            reportMutasiStokLokasi.tipes.Add(gettipe);
            return View(reportMutasiStokLokasi);
        }

        public JsonResult GetReportTransferRequest(string fromdate, string todate, string store, string status, string location)
        {
            List<GetReportTransferRequest> journallist = new List<GetReportTransferRequest>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    string kodefrom = Session["kode_lokasi"].ToString();
                    string kodetos = Session["kode_lokasi"].ToString();
                    var checklokasis = db.tblLokasis.Where(p => p.kodelokasi == kodetos).FirstOrDefault();
                    if (checklokasis != null)
                    {
                        kodetos = checklokasis.InTransit;
                    }
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:01", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var report = (from th in db.tblthMutasiStokTRs.Where(p =>  
                                  (p.tgl >= dtfrom) &&
                                  (p.tgl <= dtto))
                                  from td in db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == th.NoFaktur)
                                  from l1 in db.tblLokasis.Where(p => p.kodelokasi == th.kodelokasi)
                                  from l2 in db.tblLokasis.Where(p => p.InTransit == th.tujuan)
                                  from l3 in db.musers.Where(p => p.muserid == th.userid)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  group new { th } by new
                                  {
                                      th.NoFaktur,
                                      th.tgl,
                                      fromnama = l1.nama,
                                      fromkode = l1.kodelokasi,
                                      tonama = l2.nama,
                                      tokode = l2.kodelokasi,
                                      th.tipeMutasiStokTR,
                                      kodebarang = b.kodebarang,
                                      namabarang = b.nama,
                                      qty = (int)td.qty,
                                      kirim = (int)td.kirim,
                                      open = (int)td.qty - (int)td.kirim,
                                      th.keterangan,
                                      th.closed,
                                      th.kodelokasi,
                                      th.tujuan,
                                      th.userid,
                                      l3.musernama,
                                      th.tgljatuhtempo
                                  }
                                  into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.tgl,
                                      g.Key.fromnama,
                                      g.Key.fromkode,
                                      g.Key.tonama,
                                      g.Key.tokode,
                                      g.Key.tipeMutasiStokTR,
                                      g.Key.kodebarang,
                                      g.Key.namabarang,
                                      g.Key.qty,
                                      g.Key.kirim,
                                      g.Key.open,
                                      g.Key.keterangan,
                                      g.Key.closed,
                                      g.Key.kodelokasi,
                                      g.Key.tujuan,
                                      g.Key.userid,
                                      g.Key.musernama,
                                      g.Key.tgljatuhtempo,
                                      checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur && p.closed == true).ToList().Count,
                                      checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList().Count
                                  }).OrderBy(p => p.tgl).ToList();
                    if (location == "1")
                    {
                        report = report.Where(p => p.tujuan == kodetos).ToList();
                    }
                    else if (location == "2")
                    {
                        report = report.Where(p => p.kodelokasi == kodefrom).ToList();
                    }
                    //if (!store.Equals("0"))
                    //{
                    //    report = report.Where(p => p.tokode.ToLower() == store.ToLower()).ToList();
                    //}
                    if (store != "0")
                    {
                        string transitlocation = "";
                        var checktransit = db.tblLokasis.Where(p => p.kodelokasi == store).FirstOrDefault();
                        if (checklokasis != null)
                        {
                            transitlocation = checktransit.InTransit;
                        }
                        report = location == "1" ? report.Where(p => p.fromkode == store).ToList() : report.Where(p => p.tokode == store).ToList();
                    }
                    if (!status.Equals("-1"))
                    {
                        bool convertstatus = (status == "0" ? false : true);
                        report = report.Where(p => p.closed == convertstatus).ToList();
                    }
                    int i = 0;
                    string date = "";
                    foreach (var reports in report)
                    {
                        string nofaktur = reports.NoFaktur;
                        var resultreport = new GetReportTransferRequest
                        {
                            nofaktur = reports.NoFaktur,
                            tgl = Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy HH:mm"),
                            fromnama = reports.fromnama,
                            fromcode = reports.fromkode,
                            tonama = location == "1" ? reports.fromnama : reports.tonama,
                            tocode = location == "1" ? reports.fromkode : reports.tokode,
                            userid = reports.userid,
                            tipe = reports.tipeMutasiStokTR,
                            kodebarang = reports.kodebarang,
                            namabarang = reports.namabarang,
                            qty = reports.qty,
                            kirim = reports.kirim,
                            open = reports.open,
                            comment = reports.keterangan,
                            usernama = reports.musernama,
                            status = reports.checktbltdmutasistoktrclose == reports.checktbltdmutasistoktrall ? "Close" : "Open",
                            requireddate = reports.tgljatuhtempo.ToString("dd-MMM-yyyy")
                        };
                        date = resultreport.tgl;
                        journallist.Add(resultreport);
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult ReportGRPO()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportGRPO report = new ReportGRPO();
            return View(report);
        }

        public JsonResult GetReportGRPO(string fromdate, string todate)
        {
            List<GetReportGRPO> journallist = new List<GetReportGRPO>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthPemakaians.Where(p => p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdPemakaians.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from s in db.tblSuppliers.Where(p => p.kodesupplier == th.kodecustomer)
                                  group new { th, td, b, u, s } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      th.tgl,
                                      th.keterangan,
                                      b.satuan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      td.docentry,
                                      u.musernama,
                                      u.muserid,
                                      kodesupplier = s.kodesupplier,
                                      namasupplier = s.nama
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.satuan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.docentry,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate,
                                      g.Key.kodesupplier,
                                      g.Key.namasupplier
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportGRPO
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.qty,
                            docentry = reports.docentry,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama,
                            kodesupplier = reports.NoFaktur.Equals(nofaktur) ? "" : reports.kodesupplier,
                            namasupplier = reports.NoFaktur.Equals(nofaktur) ? "" : reports.namasupplier
                        };
                        journallist.Add(resultreport);
                        nofaktur = reports.NoFaktur;
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportGoodsReturn()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportGoodsReturn report = new ReportGoodsReturn();
            return View(report);
        }

        public JsonResult GetReportGoodsReturn(string fromdate, string todate, string type)
        {
            List<GetReportGoodsReturn> journallist = new List<GetReportGoodsReturn>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthOpnames.Where(p => p.NoFaktur.Contains("GRN") && p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdOpnames.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l in db.tblSuppliers.Where(p => p.kodesupplier == th.kodesupplier)
                                  group new { th, td, b, u, l } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      suppliercode = l.kodesupplier,
                                      suppliername = l.nama,
                                      th.tgl,
                                      th.keterangan,
                                      b.kodebarang,
                                      b.nama,
                                      td.stok,
                                      td.harga,
                                      u.musernama,
                                      u.muserid
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.suppliercode,
                                      g.Key.suppliername,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.stok,
                                      g.Key.harga,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportGoodsReturn
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            suppliercode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.suppliercode,
                            suppliername = reports.NoFaktur.Equals(nofaktur) ? "" : reports.suppliername,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.stok,
                            price = (double)reports.harga,
                            value = (int)reports.stok * (double)reports.harga,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                        };
                        journallist.Add(resultreport);
                        nofaktur = reports.NoFaktur;
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReportPurchaseRequest()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ReportPurchaseRequest report = new ReportPurchaseRequest();
            return View(report);
        }

        public JsonResult GetReportPurchaseRequest(string fromdate, string todate)
        {
            List<GetReportPurchaseRequest> journallist = new List<GetReportPurchaseRequest>();
            if (!fromdate.Equals("") && !todate.Equals(""))
            {
                try
                {
                    var dtfrom = DateTime.ParseExact(fromdate + " 00:00:00", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    var dtto = DateTime.ParseExact(todate + " 23:59:59", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string kodelokasi = Session["kode_lokasi"].ToString();
                    var report = (from th in db.tblthPRs.Where(p => p.NoFaktur.Contains("PRO") && p.kodelokasi == kodelokasi && (p.tgl >= dtfrom) && (p.tgl <= dtto))
                                  from td in db.tbltdPRs.Where(p => p.NoFaktur == th.NoFaktur)
                                  from b in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                  from u in db.musers.Where(p => p.muserid == th.userid)
                                  from l in db.tblSuppliers.Where(p => p.kodesupplier == th.kodesupplier)
                                  group new { th, td, b, u, l } by new
                                  {
                                      th.NoFaktur,
                                      th.modidate,
                                      suppliercode = l.kodesupplier,
                                      suppliername = l.nama,
                                      th.tgl,
                                      th.keterangan,
                                      b.kodebarang,
                                      b.nama,
                                      td.qty,
                                      td.hargajual,
                                      u.musernama,
                                      u.muserid,
                                      th.tglrequired
                                  }
                                       into g
                                  select new
                                  {
                                      g.Key.NoFaktur,
                                      g.Key.suppliercode,
                                      g.Key.suppliername,
                                      g.Key.tgl,
                                      g.Key.keterangan,
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.qty,
                                      g.Key.hargajual,
                                      g.Key.musernama,
                                      g.Key.muserid,
                                      g.Key.modidate,
                                      g.Key.tglrequired
                                  }).OrderBy(p => p.NoFaktur).ToList();
                    int i = 0;
                    string nofaktur = "";
                    foreach (var reports in report)
                    {
                        var resultreport = new GetReportPurchaseRequest
                        {
                            nofaktur = reports.NoFaktur,
                            nobukti = reports.NoFaktur.Equals(nofaktur) ? "" : reports.NoFaktur,
                            suppliercode = reports.NoFaktur.Equals(nofaktur) ? "" : reports.suppliercode,
                            suppliername = reports.NoFaktur.Equals(nofaktur) ? "" : reports.suppliername,
                            tgl = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tgl).ToString("dd-MMM-yyyy"),
                            tglrequired = reports.NoFaktur.Equals(nofaktur) ? "" : Convert.ToDateTime(reports.tglrequired).ToString("dd-MMM-yyyy"),
                            keterangan = reports.NoFaktur.Equals(nofaktur) ? "" : reports.keterangan,
                            modidate = Convert.ToDateTime(reports.modidate).ToString("dd-MMM-yyyy HH:mm"),
                            kodebarang = reports.kodebarang,
                            namabarang = reports.nama,
                            qty = (int)reports.qty,
                            price = (double)reports.hargajual,
                            value = (int)reports.qty * (double)reports.hargajual,
                            userid = reports.NoFaktur.Equals(nofaktur) ? "" : reports.muserid,
                            username = reports.NoFaktur.Equals(nofaktur) ? "" : reports.musernama
                        };
                        journallist.Add(resultreport);
                        nofaktur = reports.NoFaktur;
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                }
            }
            return Json(journallist, JsonRequestBehavior.AllowGet);
        }
    }
}