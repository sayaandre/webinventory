﻿using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class HomeController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult Welcome(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            WelcomeClass welcomeClass = new WelcomeClass();
            try
            {
               
                ViewBag.Message = message;
                string kodelokasi = Session["kode_lokasi"].ToString();
                string initialkodelokasi = "";
                int trpending = 0;
                int tsopending = 0;
                int tsipending = 0;
                int popending = 0;
                var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checklokasi != null)
                {
                    initialkodelokasi = checklokasi.InTransit;
                }
                var transferreqlist = (from m in db.tblthMutasiStokTRs.Where(p => p.kodelokasi == kodelokasi)
                                       group new { m } by new
                                       {
                                           m.NoFaktur
                                       }
                              into g
                                       select new
                                       {
                                           g.Key.NoFaktur,
                                           checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur && p.closed == true).ToList().Count,
                                           checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList().Count
                                       }).ToList();
                for (int i = 0; i < transferreqlist.Count; i++)
                {
                    if (transferreqlist[i].checktbltdmutasistoktrclose != transferreqlist[i].checktbltdmutasistoktrall)
                    {
                        trpending++;
                    }
                }
                //var transferreqlist = db.tblthMutasiStokTRs.Where(p => p.kodelokasi == kodelokasi).OrderByDescending(p => p.nourut)
                //    .ToList();
                //for (int i = 0; i < transferreqlist.Count; i++)
                //{
                //    string nofakturtr = transferreqlist[i].NoFaktur;
                //    var checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofakturtr && p.closed == true).ToList().Count;
                //    var checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofakturtr).ToList().Count;
                //    if (checktbltdmutasistoktrclose != checktbltdmutasistoktrall)
                //    {
                //        trpending++;
                //    }
                //}
                var dt = DateTime.Now.AddDays(-60);
                welcomeClass.trpending = trpending;
                var tolist = (from m in db.vw_opentsout.Where(p => p.tujuan == initialkodelokasi && p.tgl >= dt)
                              group new { m } by new
                              {
                                  m.NoFaktur,
                                  m.tgl
                              }
                              into g
                              select new
                              {
                                  g.Key.NoFaktur,
                                  g.Key.tgl
                              }).ToList();
                for (int i = 0; i < tolist.Count; i++)
                {
                    string nofaktur = "";
                    if (nofaktur != tolist[i].NoFaktur)
                    {
                        tsopending++;
                        nofaktur = tolist[i].NoFaktur;
                    }
                }
                //var tolist = (from m in db.tblthMutasiStoks.Where(p => p.tujuan == initialkodelokasi && p.tgl >= dt)
                //              group new { m } by new
                //              {
                //                  m.NoFaktur
                //              }
                //              into g
                //              select new
                //              {
                //                  g.Key.NoFaktur,
                //                  checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                //              }).ToList();
                //for (int i = 0; i < tolist.Count; i++)
                //{
                //    int countdetailmutasi = 0;
                //    string faktur = tolist[i].NoFaktur;
                //    for (int j = 0; j < tolist[i].checkdetailmutasi.Count; j++)
                //    {
                //        if (tolist[i].checkdetailmutasi[j].qty <= tolist[i].checkdetailmutasi[j].kirim)
                //        {
                //            countdetailmutasi++;
                //        }
                //    }
                //    if (countdetailmutasi != tolist[i].checkdetailmutasi.Count)
                //    {
                //        tsopending++;
                //    }
                //}
                //var tolist = db.tblthMutasiStoks.Where(p => p.tujuan == initialkodelokasi).OrderByDescending(p => p.nourut).ToList();
                //for (int i = 0; i < tolist.Count; i++)
                //{
                //    int countdetailmutasi = 0;
                //    string faktur = tolist[i].NoFaktur;
                //    var checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == faktur).ToList();
                //    for (int j = 0; j < checkdetailmutasi.Count; j++)
                //    {
                //        if (checkdetailmutasi[j].qty == checkdetailmutasi[j].kirim)
                //        {
                //            countdetailmutasi++;
                //        }
                //    }
                //    if (countdetailmutasi != checkdetailmutasi.Count)
                //    {
                //        tsopending++;
                //    }
                //}
                welcomeClass.tsopending = tsopending;
                var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
                if (checkwhscode != null)
                {
                    SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                    try
                    {
                        SqlDataReader rdr = null;
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                        cmd.Parameters.Add(new SqlParameter("@kodesupplier", ""));
                        cmd.Parameters.Add(new SqlParameter("@rekap", 1));
                        cmd.Parameters.Add(new SqlParameter("@docnum", 0));
                        rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            popending++;
                        }
                        welcomeClass.popending = popending;
                    }
                    catch (Exception e)
                    {
                        welcomeClass.popending = 0;
                        string errormassage = e.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }

                var checkstocktemp = db.tblthOpnameTemps.Where(p => p.NoFaktur.Contains("STT") && p.kodelokasi == kodelokasi).ToList();
                welcomeClass.stocktemppending = checkstocktemp.Count;
                welcomeClass.store = new List<tblLokasi>();
                if (Session["real_kode_lokasi"].ToString().Equals("PUSAT"))
                {
                    string currkodelokasi = Session["real_kode_lokasi"].ToString();
                    string userid = Session["user_id"].ToString();
                    string otorisasi = Session["otorisasi_super_user"].ToString();
                    if (otorisasi == "True")
                    {
                        welcomeClass.store = db.tblLokasis
                            .Where(p => (!p.kodelokasi.Contains("000") && !p.kodelokasi.Contains("PUSAT")) && p.initial.Length > 0 &&
                            !p.kodelokasi.Equals(kodelokasi) && p.aktif == true).ToList();
                        welcomeClass.storesize = welcomeClass.store.Count;
                        var checkcurrlokasi = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
                        if (checkcurrlokasi != null)
                        {
                            welcomeClass.currkodelokasi = checkcurrlokasi.kodelokasi;
                            welcomeClass.currnamalokasi = checkcurrlokasi.nama;
                        }
                    }
                    else
                    {
                        var checklocation = db.tblLokasiUsers.Where(p => p.muserid == userid).ToList();
                        if(checklocation.Count > 0)
                        {
                            var checkcsstore = (from m in db.tblLokasiUsers.Where(p => p.muserid == userid)
                                                from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi && p.kodelokasi != kodelokasi)
                                                group new { m, l1 } by new
                                                {
                                                    l1.kodelokasi,
                                                    l1.nama,
                                                    l1.mainwarehouse
                                                }
                                                into g
                                                select new
                                                {
                                                    g.Key.kodelokasi,
                                                    g.Key.nama,
                                                    g.Key.mainwarehouse
                                                }).OrderByDescending(p => p.mainwarehouse).ToList();
                            welcomeClass.store = new List<tblLokasi>();
                            for (int i = 0; i < checkcsstore.Count; i++)
                            {
                                tblLokasi tblLokasi = new tblLokasi();
                                tblLokasi.kodelokasi = checkcsstore[i].kodelokasi;
                                tblLokasi.nama = checkcsstore[i].nama;
                                welcomeClass.store.Add(tblLokasi);
                            }
                            welcomeClass.storesize = welcomeClass.store.Count;
                        }
                        else
                        {
                            var checkcsstore = (from l1 in db.tblLokasis.Where(p => p.kodelokasi != kodelokasi)
                                                group new { l1 } by new
                                                {
                                                    l1.kodelokasi,
                                                    l1.nama,
                                                    l1.mainwarehouse
                                                }
                                                into g
                                                select new
                                                {
                                                    g.Key.kodelokasi,
                                                    g.Key.nama,
                                                    g.Key.mainwarehouse
                                                }).OrderByDescending(p => p.mainwarehouse).ToList();
                            welcomeClass.store = new List<tblLokasi>();
                            for (int i = 0; i < checkcsstore.Count; i++)
                            {
                                tblLokasi tblLokasi = new tblLokasi();
                                tblLokasi.kodelokasi = checkcsstore[i].kodelokasi;
                                tblLokasi.nama = checkcsstore[i].nama;
                                welcomeClass.store.Add(tblLokasi);
                            }
                            welcomeClass.storesize = welcomeClass.store.Count;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string emessage = e.Message;
            }
            return View(welcomeClass);
        }

        public int checkLinkedServer()
        {
            int a = 0;
            SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("sys.sp_testlinkedserver", conn);
                command.CommandText = "SELECT [sys.sp_testlinkedserver](@sData)";
                command.Parameters.AddWithValue("@sData", "POSVIT_SBO");
                var afterdecrypt = command.ExecuteScalar();
                a = 1;
            }
            catch (Exception e)
            {
                string errormassage = e.Message;
                a = 0;
            }
            finally
            {
                conn.Close();
            }
            return a;
        }

        public ActionResult ChooseStoreFromWelcome(string kodelokasi)
        {
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            if (checklokasi != null)
            {
                Session["kode_lokasi"] = checklokasi.kodelokasi;
                Session["nama_lokasi"] = checklokasi.nama;
                Session["initial_lokasi"] = checklokasi.initial;
            }
            return RedirectToAction("Welcome", new { message = "Choose Store " + kodelokasi });
        }

        [AllowAnonymous]
        public ActionResult Login(string message)
        {
            //hernandez y1V8IC
            ViewBag.Message = message;
            if (Session["user_id"] != null)
            {
                return RedirectToAction("Welcome");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(muser user)
        {
            string pass = user.muserpass;
            SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
            List<muser> userdb = db.musers
                    .Where(p => p.muserid == user.muserid)
                    .ToList();

            for (int i = 0; i < userdb.Count; i++)
            {
                conn.Open();
                SqlCommand command = new SqlCommand("fDecryptPass", conn);
                command.CommandText = "SELECT [dbo].[fDecryptPass](@sData)";
                command.Parameters.AddWithValue("@sData", userdb[i].passchecker2);
                var afterdecrypt = command.ExecuteScalar();
                conn.Close();
                if (afterdecrypt.Equals(pass.ToUpper()))
                {
                    Session["user_id"] = userdb[i].muserid;
                    Session["role_id"] = userdb[i].posisi;
                    Session["user_name"] = userdb[i].musernama;
                    Session["role_name"] = userdb[i].posisi;
                    Session["otorisasi_super_user"] = userdb[i].otorisasisuperuser;
                    Session["lock_date"] = userdb[i].lockdate;
                    string kodelokasi = userdb[i].kodelokasi;
                    string lockdate = Session["lock_date"].ToString();
                    var checklokasi = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
                    if (checklokasi != null)
                    {
                        if (checklokasi.kodelokasi.Equals("PUSAT"))
                        {
                            var checklokasiother = db.tblLokasis.Where(p => !p.kodelokasi.Equals("PUSAT")).FirstOrDefault();
                            if (checklokasiother != null)
                            {
                                string userid = Session["user_id"].ToString();
                                string otorisasi = Session["otorisasi_super_user"].ToString();
                                if (otorisasi == "True")
                                {
                                    Session["kode_lokasi"] = checklokasiother.kodelokasi;
                                    Session["real_kode_lokasi"] = checklokasi.kodelokasi;
                                    Session["nama_lokasi"] = checklokasiother.nama;
                                    Session["real_nama_lokasi"] = checklokasi.nama;
                                    Session["initial_lokasi"] = checklokasiother.initial;
                                    Session["real_initial_lokasi"] = checklokasi.initial;
                                }
                                else {
                                    var checkcsstore = (from m in db.tblLokasiUsers.Where(p => p.muserid == userid)
                                                        from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                                                        group new { m, l1 } by new
                                                        {
                                                            l1.kodelokasi,
                                                            l1.nama,
                                                            l1.mainwarehouse,
                                                            l1.initial
                                                        }
                                                        into g
                                                        select new
                                                        {
                                                            g.Key.kodelokasi,
                                                            g.Key.nama,
                                                            g.Key.mainwarehouse,
                                                            g.Key.initial
                                                        }).OrderByDescending(p => p.mainwarehouse).FirstOrDefault();
                                    if (checkcsstore != null)
                                    {
                                        Session["kode_lokasi"] = checkcsstore.kodelokasi;
                                        Session["real_kode_lokasi"] = checklokasi.kodelokasi;
                                        Session["nama_lokasi"] = checkcsstore.nama;
                                        Session["real_nama_lokasi"] = checklokasi.nama;
                                        Session["initial_lokasi"] = checkcsstore.initial;
                                        Session["real_initial_lokasi"] = checklokasi.initial;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Session["kode_lokasi"] = checklokasi.kodelokasi;
                            Session["real_kode_lokasi"] = checklokasi.kodelokasi;
                            Session["nama_lokasi"] = checklokasi.nama;
                            Session["real_nama_lokasi"] = checklokasi.nama;
                            Session["initial_lokasi"] = checklokasi.initial;
                            Session["real_initial_lokasi"] = checklokasi.initial;
                        }
                    }
                    string dbname = WebConfigurationManager.AppSettings["DBName"].ToString();
                    var checkconfig = db.tblKonfigurasis.Where(p => p.Nama == dbname).FirstOrDefault();
                    if (checkconfig != null)
                    {
                        Session["tr_partial"] = checkconfig.autocloseTRpartial;
                        Session["tr_edit"] = checkconfig.treditsetelahkirim;
                        Session["tso_gt_tr"] = checkconfig.tsoutlebihbesartr;
                        Session["tso_eq_tr"] = checkconfig.tsouteqtr;
                        Session["tr_cek_stok"] = checkconfig.trcekstokpengirim;
                        Session["tso_copy_tr"] = checkconfig.tsoutcopytr;
                    }
                    Session["server_name"] = WebConfigurationManager.AppSettings["ServerName"].ToString();
                    Session["db_name"] = WebConfigurationManager.AppSettings["DBName"].ToString();
                    Session["sql_db_name"] = WebConfigurationManager.AppSettings["SQLDBName"].ToString();
                    var menuCode = "";
                    var menuName = "";
                    var userId = Session["user_id"].ToString();
                    var checkMenu = db.musermenus.Where(p => p.muserid == userId).ToList();
                    for (int j = 0; j < checkMenu.Count; j++)
                    {
                        var code = checkMenu[j].mmenuscreen;
                        var checkMenuName = db.mmenus.Where(p => p.mmenuscreen == code).FirstOrDefault();
                        if (checkMenuName != null)
                        {
                            if (j == 0)
                            {
                                menuCode = checkMenu[j].mmenuscreen.ToString();
                                menuName = checkMenuName.mmenuname;
                            }
                            else
                            {
                                menuCode = menuCode + ";" + checkMenu[j].mmenuscreen.ToString();
                                menuName = menuName + ";" + checkMenuName.mmenuname;
                            }
                        }
                    }
                    Session["menu_code"] = menuCode;
                    Session["menu_name"] = menuName;
                    return RedirectToAction("Welcome", new { message = "Login Success"});
                }
            }
            ViewBag.Message = "Wrong UserId or Password";
            return RedirectToAction("Login", new { message = ViewBag.Message });
        }

        public ActionResult Logout()
        {
            Session["user_id"] = null;
            Session["role_id"] = null;
            Session["user_name"] = null;
            Session["role_name"] = null;
            Session["kode_lokasi"] = null;
            Session.RemoveAll();
            return RedirectToAction("Login");
        }

        public ActionResult ChangePassword(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            ChangePasswordClass changePassword = new ChangePasswordClass();
            string userid = Session["user_id"].ToString();
            var userdb = db.musers.Where(p => p.muserid == userid).FirstOrDefault();
            if (userdb != null)
            {
                changePassword.userid = userdb.muserid;
                changePassword.username = userdb.musernama;
            }
            return View(changePassword);
        }
        
        public ActionResult SavePassword(string userid, string oldpass, string newpass, string confnewpass)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (newpass == confnewpass)
            {
                var userdb = db.musers.Where(p => p.muserid == userid).FirstOrDefault();
                if (userdb != null)
                {
                    SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                    conn.Open();
                    SqlCommand command2 = new SqlCommand("fDecryptPass", conn);
                    command2.CommandText = "SELECT [dbo].[fDecryptPass](@sData)";
                    command2.Parameters.AddWithValue("@sData", userdb.passchecker2);
                    var afterdecrypt2 = command2.ExecuteScalar();
                    conn.Close();
                    if (afterdecrypt2.ToString() == oldpass)
                    {
                        conn.Open();
                        SqlCommand command = new SqlCommand("fEncryptPass", conn);
                        command.CommandText = "SELECT [dbo].[fEncryptPass](@sData)";
                        command.Parameters.AddWithValue("@sData", newpass);
                        var afterdecrypt = command.ExecuteScalar();
                        conn.Close();
                        userdb.passchecker2 = afterdecrypt.ToString();
                        db.SaveChanges();
                        return RedirectToAction("Welcome", new { message = "Change Password Success" });
                    }
                    else
                    {
                        return RedirectToAction("ChangePassword", new { message = "Wrong Password" });
                    }
                }
            }
            else
            {
                return RedirectToAction("ChangePassword", new { message = "Check Confirmation Password" });
            }
            return RedirectToAction("Welcome", new { message = "Change Password Failed" });
        }

        public ActionResult JustForYou()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}