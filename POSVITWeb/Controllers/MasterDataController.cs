﻿
using ImageResizer;
using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class MasterDataController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult ItemMaster()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            List<ItemMasterClass> chooseitems = new List<ItemMasterClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true && p.FirmCode != -1)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodefrom && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodefrom && p.kodeprodusen == m.kodeprodusen)
                          from st in db.tblStoks.Where(p => p.kodelokasi == kodefrom && p.kodebarang == m.kodebarang)
                          group new { m, l1 } by new
                          {
                              m.kodebarang,
                              m.nama,
                              kodekategori = l1.kodekategori,
                              namaketegori = l1.nama,
                              m.satuan,
                              m.hargajual,
                              stok = (int)st.stokmasuk - (int)st.stokkeluar
                          }
                          into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namaketegori,
                              g.Key.satuan,
                              g.Key.hargajual,
                              g.Key.stok
                          }).OrderBy(p => p.kodebarang).ToList();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ItemMasterClass
                {
                    kodebarang = itemdbs.kodebarang,
                    nama = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namaketegori,
                    satuan = itemdbs.satuan,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    stok = itemdbs.stok
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult DetailsItemMaster(string kodebarang)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ItemMasterDetailsClass chooseitems = new ItemMasterDetailsClass();
            string kodefrom = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.kodebarang == kodebarang)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          group new { m, l1 } by new
                          {
                              m.kodebarang,
                              m.nama,
                              kodekategori = l1.kodekategori,
                              namaketegori = l1.nama,
                              m.satuan,
                              m.hargajual,
                              kodevendor = m.kodesupplier,
                              m.itemcode
                          }
                          into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namaketegori,
                              g.Key.satuan,
                              g.Key.hargajual,
                              g.Key.kodevendor,
                              g.Key.itemcode,
                              supplier = db.tblSuppliers.Where(p => p.kodesupplier == g.Key.kodevendor).FirstOrDefault()
                          }).FirstOrDefault();
            if (itemdb != null)
            {
                chooseitems.kodebarang = itemdb.kodebarang;
                chooseitems.realkodebarang = itemdb.kodebarang;
                chooseitems.nama = itemdb.nama;
                chooseitems.realnama = itemdb.nama;
                chooseitems.kodekategori = itemdb.kodekategori;
                chooseitems.namakategori = itemdb.namaketegori;
                chooseitems.satuan = itemdb.satuan;
                chooseitems.strharga = Convert.ToDecimal((double)itemdb.hargajual).ToString("#,###");
                chooseitems.harga = (double)itemdb.hargajual;
                chooseitems.stok = 0;
                if (itemdb.supplier != null)
                {
                    chooseitems.kodevendor = itemdb.supplier.kodesupplier;
                    chooseitems.namavendor = itemdb.supplier.nama;
                }
                else
                {
                    chooseitems.kodevendor = "";
                    chooseitems.namavendor = "-";
                }
                chooseitems.itemcode = itemdb.itemcode;
                chooseitems.stocks = new List<ItemMasterStockDetailsClass>();
                var checkstokloc = (from m in db.tblStoks.Where(p => p.kodebarang == itemdb.kodebarang)
                                    from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                                    group new { m, l1 } by new
                                    {
                                        l1.kodelokasi,
                                        l1.nama,
                                        stok = m.stokmasuk - m.stokkeluar
                                    }
                                    into g
                                    select new
                                    {
                                        g.Key.kodelokasi,
                                        g.Key.nama,
                                        g.Key.stok
                                    }).OrderByDescending(p => p.nama).ToList();
                for (int i = 0; i < checkstokloc.Count; i++)
                {
                    if ((int)checkstokloc[i].stok > 0)
                    {
                        ItemMasterStockDetailsClass itemMasterStock = new ItemMasterStockDetailsClass();
                        itemMasterStock.kodelokasi = checkstokloc[i].kodelokasi;
                        itemMasterStock.namalokasi = checkstokloc[i].nama;
                        itemMasterStock.stok = (int)checkstokloc[i].stok;
                        chooseitems.stocks.Add(itemMasterStock);
                    }
                }
                chooseitems.prices = new List<ItemMasterPriceListClass>();
                var pricelistlokasi = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
                if (pricelistlokasi != null)
                {
                    var checkpriceloc = (from m in db.tblLokasiCustomerGroups.Where(p => p.kodelokasi == kodefrom)
                                         from l1 in db.tblCustomerGroups.Where(p => p.kodecustomergroup == m.kodecustomergroup)
                                         from p in db.tblDPriceLists.Where(p => p.kodepricelist ==
                                        ((l1.kodepricelist.Equals("") || l1.kodepricelist == null) ? pricelistlokasi.kodepricelist : l1.kodepricelist) &&
                                        p.kodebarang == kodebarang)
                                         group new { m, l1 } by new
                                         {
                                             l1.kodecustomergroup,
                                             l1.nama,
                                             p.hargajual,
                                             p.discjual
                                         }
                                    into g
                                         select new
                                         {
                                             g.Key.kodecustomergroup,
                                             g.Key.nama,
                                             g.Key.hargajual,
                                             g.Key.discjual
                                         }).OrderByDescending(p => p.kodecustomergroup).ToList();
                    for (int i = 0; i < checkpriceloc.Count; i++)
                    {
                        ItemMasterPriceListClass itemMasterPriceList = new ItemMasterPriceListClass();
                        itemMasterPriceList.kodecustomergroup = checkpriceloc[i].kodecustomergroup;
                        itemMasterPriceList.namacustomergroup = checkpriceloc[i].nama;
                        itemMasterPriceList.harga = (double)checkpriceloc[i].hargajual;
                        itemMasterPriceList.strharga = Convert.ToDecimal((double)checkpriceloc[i].hargajual).ToString("#,###");
                        itemMasterPriceList.disc = (double)checkpriceloc[i].discjual;
                        itemMasterPriceList.hargaNett = itemMasterPriceList.disc == 0 ? itemMasterPriceList.harga : 
                            (itemMasterPriceList.harga - (itemMasterPriceList.harga * itemMasterPriceList.disc / 100));
                        itemMasterPriceList.strHargaNett = Convert.ToDecimal(itemMasterPriceList.hargaNett).ToString("#,###");
                        chooseitems.prices.Add(itemMasterPriceList);
                    }
                }
            }
            return View(chooseitems);
        }

        [HttpPost]
        public ActionResult DetailsItemMaster(ItemMasterDetailsClass items, HttpPostedFileBase file)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            if (file != null)
            {
                try
                {
                    if (file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        var path = Path.Combine(Server.MapPath("~/Content/images"), fileName);
                        file.SaveAs(path);
                        ResizeSettings resizeSetting = new ResizeSettings
                        {
                            Width = 300,
                            Height = 300,
                            Format = "png"
                        };
                        ImageBuilder.Current.Build(path, path, resizeSetting);
                        byte[] data = System.IO.File.ReadAllBytes(path);
                        var checkfoto = db.tblFotoBarangs.Where(p => p.kodebarang == items.realkodebarang).FirstOrDefault();
                        if (checkfoto != null)
                        {
                            checkfoto.foto = data;
                        }
                        else
                        {
                            tblFotoBarang tblFotoBarang = new tblFotoBarang();
                            tblFotoBarang.kodebarang = items.realkodebarang;
                            tblFotoBarang.namafile = items.realnama;
                            tblFotoBarang.foto = data;
                            db.tblFotoBarangs.Add(tblFotoBarang);
                        }
                        db.SaveChanges();
                    }
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
            return RedirectToAction("ItemMaster");
        }

        public FileContentResult getItemImage(string kodebarang)
        {
            var checkfoto = db.tblFotoBarangs.Where(p => p.kodebarang == kodebarang).FirstOrDefault();
            if (checkfoto != null)
            {
                byte[] data = checkfoto.foto;
                if (data != null)
                {
                    string myImageBase64StringData = Convert.ToBase64String(data);
                    return File(data, "image/jpg");
                }
                else
                    return null;
            }
            else
                return null;
        }

        public JsonResult getAllItem()
        {
            List<ItemMasterClass> chooseitems = new List<ItemMasterClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true && p.dijual == true && p.FirmCode != -1)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodefrom && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodefrom && p.kodeprodusen == m.kodeprodusen)
                          from st in db.tblStoks.Where(p => p.kodelokasi == kodefrom && p.kodebarang == m.kodebarang)
                          group new { m, l1 } by new
                          {
                              m.kodebarang,
                              m.nama,
                              kodekategori = l1.kodekategori,
                              namaketegori = l1.nama,
                              m.satuan,
                              m.hargajual,
                              stok = (int)st.stokmasuk - (int)st.stokkeluar,
                              m.itemcode
                          }
                          into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namaketegori,
                              g.Key.satuan,
                              g.Key.hargajual,
                              g.Key.stok,
                              g.Key.itemcode
                          }).OrderByDescending(p => p.nama).ToList();
            var checkCat = db.tblLokasiKategoris.Where(p => p.kodelokasi == kodefrom).ToList();
            var checkProd = db.tblLokasiProdusens.Where(p => p.kodelokasi == kodefrom).ToList();
            if (checkCat.Count == 0 && checkProd.Count == 0)
            {
                itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true && p.dijual == true && p.FirmCode != -1)
                              from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                              from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodefrom && p.kodekategori == m.kodekategori).DefaultIfEmpty()
                              from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodefrom && p.kodeprodusen == m.kodeprodusen).DefaultIfEmpty()
                              from st in db.tblStoks.Where(p => p.kodelokasi == kodefrom && p.kodebarang == m.kodebarang)
                              group new { m, l1 } by new
                              {
                                  m.kodebarang,
                                  m.nama,
                                  kodekategori = l1.kodekategori,
                                  namaketegori = l1.nama,
                                  m.satuan,
                                  m.hargajual,
                                  stok = (int)st.stokmasuk - (int)st.stokkeluar,
                                  m.itemcode
                              }
                          into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namaketegori,
                                  g.Key.satuan,
                                  g.Key.hargajual,
                                  g.Key.stok,
                                  g.Key.itemcode
                              }).OrderByDescending(p => p.nama).ToList();
            }
            foreach (var itemdbs in itemdb)
            {
                string strStock = "";
                var checkstokloc = (from m in db.tblStoks.Where(p => p.kodebarang == itemdbs.kodebarang)
                                    from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                                    group new { m, l1 } by new
                                    {
                                        l1.kodelokasi,
                                        l1.nama,
                                        stok = m.stokmasuk - m.stokkeluar
                                    }
                                into g
                                    select new
                                    {
                                        g.Key.kodelokasi,
                                        g.Key.nama,
                                        g.Key.stok
                                    }).OrderByDescending(p => p.nama).ToList();
                for (int i = 0; i < checkstokloc.Count; i++)
                {
                    if ((int)checkstokloc[i].stok > 0)
                    {
                        if (strStock.Length == 0)
                        {
                            strStock = checkstokloc[i].kodelokasi + ": " + (int)checkstokloc[i].stok;
                        }
                        else
                        {
                            strStock = strStock + ", " + checkstokloc[i].kodelokasi + " : " + (int)checkstokloc[i].stok;
                        }
                    }
                }
                var soloitem = new ItemMasterClass
                {
                    kodebarang = itemdbs.kodebarang,
                    nama = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namaketegori,
                    satuan = itemdbs.satuan,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    stok = itemdbs.stok,
                    strStock = strStock,
                    barcode = itemdbs.itemcode
                };
                chooseitems.Add(soloitem);
            }
            
            var jsonResult = Json(chooseitems, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
    }
}