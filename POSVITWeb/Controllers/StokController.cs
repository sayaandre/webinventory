﻿using Microsoft.Ajax.Utilities;
using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class StokController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult AddStokOpname()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checkfrom = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            transferOutDetail.fromstore = checkfrom.nama;
            transferOutDetail.kodefrom = kodelokasi;
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && !p.kodelokasi.Contains("T")).ToList();
            transferOutDetail.type = new List<ChooseTypeClass>();
            var typeopname = db.tblIntegrasiOpnames.ToList();
            for (int i = 0; i < typeopname.Count; i++)
            {
                ChooseTypeClass chooseType = new ChooseTypeClass();
                chooseType.typename = typeopname[i].kode;
                transferOutDetail.type.Add(chooseType);
            }
            return View(transferOutDetail);
        }

        public ActionResult AddGoodsReturn()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddGoodsReturnClass transferOutDetail = new AddGoodsReturnClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
            if (checklokasi != null)
            {
                transferOutDetail.kodelokasi = checklokasi.kodelokasi;
                transferOutDetail.namalokasi = checklokasi.nama;
            }
            var checksupplier = db.tblSuppliers.Where(p => p.aktif == true).OrderBy(p => p.nama).ToList();
            transferOutDetail.suppliers = new List<GetSupplierClass>();
            for (int i = 0; i < checksupplier.Count; i++)
            {
                GetSupplierClass chooseType = new GetSupplierClass();
                chooseType.suppliercode = checksupplier[i].kodesupplier;
                chooseType.suppliername = checksupplier[i].nama;
                chooseType.supplierPIC = checksupplier[i].namacontact1;
                transferOutDetail.suppliers.Add(chooseType);
            }
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        public ActionResult AddStokOpnameTemporary()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checkfrom = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            transferOutDetail.fromstore = checkfrom.nama;
            transferOutDetail.kodefrom = kodelokasi;
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && !p.kodelokasi.Contains("T")).ToList();
            return View(transferOutDetail);
        }

        public ActionResult AddStokMasuk()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checkfrom = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            transferOutDetail.fromstore = checkfrom.nama;
            transferOutDetail.kodefrom = kodelokasi;
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && !p.kodelokasi.Contains("T")).ToList();
            transferOutDetail.type = new List<ChooseTypeClass>();
            var typeopname = db.tblIntegrasiOpnames.ToList();
            for (int i = 0; i < typeopname.Count; i++)
            {
                ChooseTypeClass chooseType = new ChooseTypeClass();
                chooseType.typename = typeopname[i].kode;
                transferOutDetail.type.Add(chooseType);
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsStokOpname(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            var solotr = (from m in db.tblthOpnames.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              m.tipeopname,
                              m.userid,
                              m.modidate,
                              m.total
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.tipeopname,
                              g.Key.modidate,
                              g.Key.userid,
                              g.Key.total
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.fromstore = solotr.namafrom;
                transferOutDetail.tipe = solotr.tipeopname;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.userid = solotr.userid;
                transferOutDetail.total = solotr.total;
                transferOutDetail.createtime = solotr.modidate.ToString("dd-MMM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var detailstr = (from k in db.tbltdOpnames.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.harga,
                                     k.stok,
                                     k.qty
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.harga,
                                     g.Key.stok,
                                     g.Key.qty,
                                     totalharga = g.Key.qty * g.Key.harga
                                 }).ToList();
                int totalstok = 0;
                int totalqty = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    ViewCartClass viewCartClass = new ViewCartClass();
                    viewCartClass.itemcode = detailstr[i].kodebarang;
                    viewCartClass.itemname = detailstr[i].nama;
                    viewCartClass.itemprice = (double)detailstr[i].harga;
                    viewCartClass.itemstok = (int)detailstr[i].stok;
                    viewCartClass.itemqty = (int)detailstr[i].qty;
                    viewCartClass.itemtotalprice = (int)detailstr[i].totalharga;
                    transferOutDetail.carts.Add(viewCartClass);
                    totalstok = totalstok + (int)detailstr[i].stok;
                    totalqty = totalqty + (int)detailstr[i].qty;
                }
                transferOutDetail.totalqty = totalstok;
                transferOutDetail.totalqtykirim = totalqty;
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsGoodsReturn(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddGoodsReturnClass transferOutDetail = new AddGoodsReturnClass();
            var solotr = (from m in db.tblthOpnames.Where(p => p.NoFaktur == nofaktur)
                          from l2 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l3 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              l2.kodesupplier,
                              l2.nama,
                              l2.alamat,
                              l2.telp,
                              l3.musernama
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.kodesupplier,
                              g.Key.nama,
                              g.Key.alamat,
                              g.Key.telp,
                              g.Key.musernama
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.namalokasi = solotr.namafrom;
                transferOutDetail.supplier = solotr.nama;
                transferOutDetail.keterangan = solotr.keterangan;
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.email = solotr.alamat;
                transferOutDetail.phone = solotr.telp;
                transferOutDetail.username = solotr.musernama;
                transferOutDetail.carts = new List<ViewCartClass>();
                var detailstr = (from k in db.tbltdOpnames.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.harga,
                                     k.stok,
                                     k.qty
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.harga,
                                     g.Key.stok,
                                     g.Key.qty,
                                     totalharga = g.Key.qty * g.Key.harga
                                 }).ToList();
                int totalstok = 0;
                int totalqty = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    ViewCartClass viewCartClass = new ViewCartClass();
                    viewCartClass.itemcode = detailstr[i].kodebarang;
                    viewCartClass.itemname = detailstr[i].nama;
                    viewCartClass.itemprice = (double)detailstr[i].harga;
                    viewCartClass.itemstok = (float)detailstr[i].stok;
                    viewCartClass.itemqty = (float)detailstr[i].qty;
                    viewCartClass.itemtotalprice = (int)detailstr[i].totalharga;
                    transferOutDetail.carts.Add(viewCartClass);
                    totalstok = totalstok + (int)detailstr[i].stok;
                    totalqty = totalqty + (int)detailstr[i].qty;
                }
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsStokOpnameTemporary(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            var solotr = (from m in db.tblthOpnameTemps.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.fromstore = solotr.namafrom;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var detailstr = (from k in db.tbltdOpnameTemps.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.harga,
                                     k.stok,
                                     k.qty
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.harga,
                                     g.Key.stok,
                                     g.Key.qty
                                 }).ToList();
                int totalstok = 0;
                int totalqty = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    ViewCartClass viewCartClass = new ViewCartClass();
                    viewCartClass.itemcode = detailstr[i].kodebarang;
                    viewCartClass.itemname = detailstr[i].nama;
                    viewCartClass.itemprice = (double)detailstr[i].harga;
                    viewCartClass.itemstok = (int)detailstr[i].stok;
                    viewCartClass.itemqty = (int)detailstr[i].qty;
                    transferOutDetail.carts.Add(viewCartClass);
                    totalstok = totalstok + (int)detailstr[i].stok; ;
                    totalqty = totalqty + (int)detailstr[i].qty;
                }
                transferOutDetail.totalqty = totalstok;
                transferOutDetail.totalqtykirim = totalqty;
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsStokMasuk(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            var solotr = (from m in db.tblthOpnames.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              m.tipeopname
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.tipeopname
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.fromstore = solotr.namafrom;
                transferOutDetail.tipe = solotr.tipeopname;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var detailstr = (from k in db.tbltdOpnames.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.harga,
                                     k.stok,
                                     k.qty
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.harga,
                                     g.Key.stok,
                                     g.Key.qty
                                 }).ToList();
                int totalstok = 0;
                int totalqty = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    ViewCartClass viewCartClass = new ViewCartClass();
                    viewCartClass.itemcode = detailstr[i].kodebarang;
                    viewCartClass.itemname = detailstr[i].nama;
                    viewCartClass.itemprice = (double)detailstr[i].harga;
                    viewCartClass.itemstok = (int)detailstr[i].stok;
                    viewCartClass.itemqty = (int)detailstr[i].qty;
                    transferOutDetail.carts.Add(viewCartClass);
                    totalstok = totalstok + (int)detailstr[i].stok;
                    totalqty = totalqty + (int)detailstr[i].qty;
                }
                transferOutDetail.totalqty = totalstok;
                transferOutDetail.totalqtykirim = totalqty;
            }
            return View(transferOutDetail);
        }

        public ActionResult StokOpname(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblthOpnames.Where(p => p.NoFaktur.Contains("STO") && p.kodelokasi == kodelokasi)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              m.tipeopname,
                              m.nourut,
                              l2.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.tipeopname,
                              g.Key.nourut,
                              g.Key.musernama
                          }).ToList().OrderByDescending(p => p.tgl);
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    keterangan = itemdbs.keterangan,
                    kodeawal = itemdbs.kodefrom,
                    namaawal = itemdbs.namafrom,
                    tipe = itemdbs.tipeopname,
                    nourut = itemdbs.nourut,
                    usernama = itemdbs.musernama
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult StokOpnameTemporary(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblthOpnameTemps.Where(p => p.NoFaktur.Contains("STT") && p.kodelokasi == kodelokasi)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              m.nourut
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.nourut
                          }).ToList().OrderByDescending(p => p.tgl);
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    keterangan = itemdbs.keterangan,
                    kodeawal = itemdbs.kodefrom,
                    namaawal = itemdbs.namafrom,
                    nourut = itemdbs.nourut
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult StokMasuk(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblthOpnames.Where(p => p.NoFaktur.Contains("STM") && p.kodelokasi == kodelokasi)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              m.tipeopname,
                              m.nourut,
                              l2.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.tipeopname,
                              g.Key.nourut,
                              g.Key.musernama
                          }).ToList().OrderByDescending(p => p.tgl);

            foreach (var itemdbs in itemdb)
            {
                string nofaktur = itemdbs.NoFaktur;
                var totalitemdetail = db.tbltdOpnames.Where(p => p.NoFaktur == nofaktur).DefaultIfEmpty().Sum(p => (decimal?)p.qty);
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy HH:mm"),
                    keterangan = itemdbs.keterangan,
                    kodeawal = itemdbs.kodefrom,
                    namaawal = itemdbs.namafrom,
                    tipe = itemdbs.tipeopname,
                    nourut = itemdbs.nourut,
                    usernama = itemdbs.musernama,
                    totalitemdetail = (int?)totalitemdetail == null ? 0 : (int)totalitemdetail
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult SubmitStokOpname(string tanggal, string keterangan, string type,
            string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "STO/" + initialfrom + "/";
            var checkpenjualan = db.tblthOpnames
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("STO")).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            string[] splitcart = cart.Split(';');
            double total = 0;
            for (int i = 0; i < splitcart.Length; i++)
            {
                string[] splitrowcart = splitcart[i].Split('$');
                double subtotal = (Double.Parse(splitrowcart[1]) - Double.Parse(splitrowcart[3])) * Double.Parse(splitrowcart[2]);
                total = total + subtotal;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderOpname(nofaktur, dt, keterangan, (decimal)total, Session["user_id"].ToString(), "1",
                    kodefrom, "450.01.01", "CASH", 20);
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        double subtotal = (Double.Parse(splitrowcart[1]) - Double.Parse(splitrowcart[3])) * Double.Parse(splitrowcart[2]);
                        total = total + subtotal;
                        db.sp_SimpanDetailOpname(nofaktur, splitrowcart[0], (decimal)Int32.Parse(splitrowcart[3]),
                            (decimal)Int32.Parse(splitrowcart[1]), (decimal)Int32.Parse(splitrowcart[2]), 20,
                            (decimal)subtotal, (decimal)Int32.Parse(splitrowcart[2]));
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Stock Adjustment " + nofaktur + " Has Been Created..";
                    //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
                    //for (int i = 0; i < userdb.Count; i++)
                    //{
                    //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
                    //}
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Stock Adjustment";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult SubmitGoodsReturn(string tanggal, string keterangan, string type, string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            //dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "GRN/" + initialfrom + "/";
            var checkpenjualan = db.tblthOpnames
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("GRN") && p.tgl.Day == dt.Day &&
                    p.tgl.Month == dt.Month && p.tgl.Year == dt.Year).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            string[] splitcart = cart.Split(';');
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderOpname(nofaktur, dt, keterangan, (decimal)0, Session["user_id"].ToString(), "1",
                    kodefrom, "450.01.01", type, 3);
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        db.sp_SimpanDetailOpname(nofaktur, splitrowcart[0], (decimal)(float.Parse(splitrowcart[1])),
                            (decimal)0, (decimal)float.Parse(splitrowcart[5]), 12,
                            (decimal)0, (decimal)0);
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Goods Return " + nofaktur + " Has Been Created..";
                    //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
                    //for (int i = 0; i < userdb.Count; i++)
                    //{
                    //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
                    //}
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Goods Return";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult SubmitStokOpnameTemporary(string tanggal, string keterangan, string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "STT/" + initialfrom + "/";
            var checkpenjualan = db.tblthOpnameTemps
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("STT")).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            string[] splitcart = cart.Split(';');
            double total = 0;
            for (int i = 0; i < splitcart.Length; i++)
            {
                string[] splitrowcart = splitcart[i].Split('$');
                double subtotal = (Double.Parse(splitrowcart[1]) - Double.Parse(splitrowcart[3])) * Double.Parse(splitrowcart[2]);
                total = total + subtotal;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderOpnameTemp(nofaktur, dt, keterangan, (decimal)total, Session["user_id"].ToString(), "1",
                    kodefrom, "450.01.01", "CASH");
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        double subtotal = (Double.Parse(splitrowcart[1]) - Double.Parse(splitrowcart[3])) * Double.Parse(splitrowcart[2]);
                        total = total + subtotal;
                        db.sp_SimpanDetailOpnameTemp(nofaktur, splitrowcart[0], (decimal)Int32.Parse(splitrowcart[3]),
                            (decimal)Int32.Parse(splitrowcart[1]), (decimal)Int32.Parse(splitrowcart[2]), 1,
                            (decimal)subtotal, "");
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Stock Opname Temporary " + nofaktur + " Has Been Created..";
                    //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
                    //for (int i = 0; i < userdb.Count; i++)
                    //{
                    //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
                    //}
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Stock Opname Temporary";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult SubmitStokMasuk(string tanggal, string keterangan, string type,
            string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "STM/" + initialfrom + "/";
            var checkpenjualan = db.tblthOpnames
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("STM")).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            string[] splitcart = cart.Split(';');
            double total = 0;
            for (int i = 0; i < splitcart.Length; i++)
            {
                string[] splitrowcart = splitcart[i].Split('$');
                double subtotal = (Double.Parse(splitrowcart[1]) - Double.Parse(splitrowcart[3])) * Double.Parse(splitrowcart[2]);
                total = total + subtotal;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderOpname(nofaktur, dt, keterangan, (decimal)total, Session["user_id"].ToString(), "1",
                    kodefrom, "450.01.01", "CASH", 0);
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        double subtotal = Double.Parse(splitrowcart[3]) * Double.Parse(splitrowcart[2]);
                        total = total + subtotal;
                        db.sp_SimpanDetailOpname(nofaktur, splitrowcart[0], (decimal)Int32.Parse(splitrowcart[3]),
                            (decimal)(Int32.Parse(splitrowcart[1]) + Int32.Parse(splitrowcart[3])),
                            (decimal)Int32.Parse(splitrowcart[2]), 1,
                            (decimal)subtotal, (decimal)Int32.Parse(splitrowcart[2]));
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Goods Receipt " + nofaktur + " Has Been Created..";
                    //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
                    //for (int i = 0; i < userdb.Count; i++)
                    //{
                    //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
                    //}
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Goods Receipt";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult PurchaseRequest(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<PurchaseRequestClass> chooseitems = new List<PurchaseRequestClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var itemdb = (from m in db.tblthPRs.Where(p => p.kodelokasi == kodefrom)
                          from l1 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier)
                          from l2 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.modidate,
                              m.tgl,
                              m.keterangan,
                              l1.nama,
                              m.nourut,
                              m.kodestatus,
                              l2.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.modidate,
                              g.Key.keterangan,
                              g.Key.nama,
                              g.Key.nourut,
                              g.Key.kodestatus,
                              g.Key.musernama
                          }).ToList().OrderByDescending(p => p.nourut);
            foreach (var itemdbs in itemdb)
            {
                string nofaktur = itemdbs.NoFaktur;
                var countdetailgrpo = db.tbltdPRs.Where(p => p.NoFaktur.Equals(nofaktur)).DefaultIfEmpty().Sum(p => (decimal?)p.qty);
                var soloitem = new PurchaseRequestClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    modidate = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy HH:mm"),
                    keterangan = itemdbs.keterangan,
                    vendor = itemdbs.nama,
                    nourut = itemdbs.nourut,
                    totalitem = (float?)countdetailgrpo == null ? 0 : (float)countdetailgrpo,
                    status = itemdbs.kodestatus,
                    usernama = itemdbs.musernama
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult GRPO(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<GoodReceiptPOClass> chooseitems = new List<GoodReceiptPOClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            DateTime dtnow = DateTime.Now.AddDays(-10);
            var itemdb = (from m in db.tblthPemakaians.Where(p => p.NoFaktur.Contains("GPO") && p.kodelokasi == kodefrom && p.tgl >= dtnow)
                          from l1 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodecustomer)
                          from l2 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              l1.nama,
                              m.nourut,
                              l2.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.nama,
                              g.Key.nourut,
                              g.Key.musernama
                          }).ToList().OrderByDescending(p => p.nourut);
            foreach (var itemdbs in itemdb)
            {
                string nofaktur = itemdbs.NoFaktur;
                var countdetailgrpo = db.tbltdPemakaians.Where(p => p.NoFaktur.Equals(nofaktur)).DefaultIfEmpty().Sum(p => (decimal?)p.qty);
                var soloitem = new GoodReceiptPOClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy HH:mm:ss"),
                    keterangan = itemdbs.keterangan,
                    vendor = itemdbs.nama,
                    nourut = itemdbs.nourut,
                    usernama = itemdbs.musernama,
                    totalitem = (int?)countdetailgrpo == null ? 0 : (int)countdetailgrpo
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult GoodsReturn(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<GoodReturnClass> chooseitems = new List<GoodReturnClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            DateTime dtnow = DateTime.Now.AddDays(-7);
            var itemdb = (from m in db.tblthOpnames.Where(p => p.NoFaktur.Contains("GRN") && p.kodelokasi == kodefrom && p.tgl >= dtnow)
                          from l1 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier)
                          from l2 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              l1.nama,
                              m.nourut,
                              l2.musernama,
                              m.tipeopname
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.nama,
                              g.Key.nourut,
                              g.Key.musernama,
                              g.Key.tipeopname,
                              countdetailgoodsreturn = db.tbltdOpnames.Where(p => p.NoFaktur == g.Key.NoFaktur).Sum(p => p.qty)
                          }).ToList().OrderByDescending(p => p.nourut);
            foreach (var itemdbs in itemdb)
            {
                string nofaktur = itemdbs.NoFaktur;
                var soloitem = new GoodReturnClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tgl = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    keterangan = itemdbs.keterangan,
                    suppliername = itemdbs.nama,
                    nourut = itemdbs.nourut,
                    username = itemdbs.musernama,
                    qtydetail = (int)itemdbs.countdetailgoodsreturn
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult AddPurchaseRequest()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            PurchaseRequestDetailClass transferOutDetail = new PurchaseRequestDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
            if (checklokasi != null)
            {
                transferOutDetail.kodelokasi = checklokasi.kodelokasi;
                transferOutDetail.namalokasi = checklokasi.nama;
            }
            var suppliers = db.tblSuppliers.Where(p => p.aktif == true).ToList();
            transferOutDetail.suppliers = new List<ChooseVendorClass>();
            for (int i = 0; i < suppliers.Count; i++)
            {
                ChooseVendorClass solovendor = new ChooseVendorClass();
                solovendor.vendorcode = suppliers[i].kodesupplier;
                solovendor.vendorname = suppliers[i].nama;
                transferOutDetail.suppliers.Add(solovendor);
            }
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        public ActionResult AddGRPO()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            GoodReceiptPODetailClass transferOutDetail = new GoodReceiptPODetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var suppliers = db.tblSuppliers.Where(p => p.aktif == true).ToList();
            transferOutDetail.suppliers = new List<ChooseVendorClass>();
            for (int i = 0; i < suppliers.Count; i++)
            {
                ChooseVendorClass solovendor = new ChooseVendorClass();
                solovendor.vendorcode = suppliers[i].kodesupplier;
                solovendor.vendorname = suppliers[i].nama;
                transferOutDetail.suppliers.Add(solovendor);
            }
            transferOutDetail.poes = new List<ChoosePOClass>();
            var poes = db.vw_grpo.DistinctBy(p => p.DocNum).ToList();
            for (int i = 0; i < poes.Count; i++)
            {
                ChoosePOClass solopo = new ChoosePOClass();
                solopo.nopo = poes[i].DocNum.ToString();
                transferOutDetail.poes.Add(solopo);
            }
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        public JsonResult getVendor()
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            var suppliers = db.tblSuppliers.Where(p => p.aktif == true && p.kodesupplier != "CASH").ToList();
            List<ChooseVendorClass> listsupp = new List<ChooseVendorClass>();
            for (int i = 0; i < suppliers.Count; i++)
            {
                ChooseVendorClass solovendor = new ChooseVendorClass();
                solovendor.vendorcode = suppliers[i].kodesupplier;
                solovendor.vendorname = suppliers[i].nama;
                listsupp.Add(solovendor);
            }
            var jsonResult = Json(listsupp, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult SubmitGRPO(string tanggal, string keterangan, string vendor, string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                string userid = Session["user_id"].ToString();
                nofaktur = "GPO/" + initialfrom + "/";
                var checkpenjualan = db.tblthPemakaians
                    .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("GPO") && p.tgl.Day == dt.Day && 
                    p.tgl.Month == dt.Month && p.tgl.Year == dt.Year).OrderByDescending(p => p.nourut).ToList();
                if (checkpenjualan.Count() != 0)
                {
                    if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                    {
                        string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                        nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                    }
                    else
                    {
                        nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                    }
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
                var txOptions = new System.Transactions.TransactionOptions();
                txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                    Timeout = TimeSpan.FromSeconds(30)
                }))
                {
                    try
                    {
                        db.sp_SimpanHeaderPemakaian(nofaktur, dt, keterangan, userid, "1", kodefrom, vendor, 0, "110.01.01",
                                                    "GOODS RECEIPT PO");
                        string[] splitcart = cart.Split(';');
                        int checkindex = 0;
                        for (int i = 0; i < splitcart.Length; i++)
                        {
                            string[] splitrowcart = splitcart[i].Split('$');
                            string kodebarang = splitrowcart[0];
                            int pono = Int32.Parse(splitrowcart[2]);
                            int linenum = Int32.Parse(splitrowcart[3]);
                            var checkpo = db.vw_grpo.Where(p => p.DocNum == pono && p.ItemCode.Equals(kodebarang) && p.LineNum == linenum && p.U_VIT_ToStr == checkwhscode.WhsCode && p.OpenQty != 0)
                                .DistinctBy(p => p.DocNum).FirstOrDefault();
                            if (checkpo != null)
                            {
                                db.sp_SimpanDetailPemakaian(nofaktur, splitrowcart[0], (decimal)Int32.Parse(splitrowcart[1]), 0, checkpo.DocNum,
                                    checkpo.DocEntry, checkpo.DocDate, checkpo.LineNum, checkpo.AcctCode, "");
                                checkindex++;
                            }
                        }
                        if (checkindex == splitcart.Length)
                        {
                            //tblTracing tblTracing = new tblTracing();
                            //tblTracing.modidate = dt;
                            //tblTracing.nofaktur = nofaktur;
                            //tblTracing.activity = "Success";
                            //tblTracing.param = cart;
                            //db.tblTracings.Add(tblTracing);
                            //db.SaveChanges();
                            transaction.Complete();
                            messages = "GRPO " + nofaktur + " Has Been Created..";
                        }
                        else
                        {
                            //tblTracing tblTracing = new tblTracing();
                            //tblTracing.modidate = dt;
                            //tblTracing.nofaktur = nofaktur;
                            //tblTracing.activity = "Count PO : " + checkindex;
                            //tblTracing.param = cart;
                            //db.tblTracings.Add(tblTracing);
                            //db.SaveChanges();
                            messages = "Created GRPO Failed";
                        }
                        transaction.Dispose();
                    }
                    catch (Exception ex)
                    {
                        //tblTracing tblTracing = new tblTracing();
                        //tblTracing.modidate = dt;
                        //tblTracing.nofaktur = nofaktur;
                        //tblTracing.activity = ex.Message;
                        //tblTracing.param = cart;
                        //db.tblTracings.Add(tblTracing);
                        //db.SaveChanges();
                        string message = ex.Message;
                        messages = "Error Failed To Create GRPO";
                        transaction.Dispose();
                        isValid = false;
                    }
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult SubmitPurchaseRequest(string tanggal, string keterangan, string vendor, string cart, string reqdate)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            var reqdt = DateTime.ParseExact(reqdate, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
            string kodefrom = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "PRO/" + initialfrom + "/";
            var checkpenjualan = db.tblthPRs
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("PRO")).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderPR(nofaktur, dt, keterangan, vendor, userid, "1", kodefrom, "OPEN", reqdt);
                    string[] splitcart = cart.Split(';');
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        int pono = Int32.Parse(splitrowcart[2]);
                        db.sp_SimpanDetailPR(nofaktur, splitrowcart[0], (decimal)float.Parse(splitrowcart[1]), 0, (decimal)0.0,
                                 "", "", (decimal)Double.Parse(splitrowcart[2]), (decimal)0.0);
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Purchase Request " + nofaktur + " Has Been Created..";
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Purchase Request";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult DetailsGRPO(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            GoodReceiptPODetailClass transferOutDetail = new GoodReceiptPODetailClass();
            var solotr = (from m in db.tblthPemakaians.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodecustomer)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              l1.kodesupplier,
                              l1.nama
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodesupplier,
                              g.Key.nama
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.keterangan = solotr.keterangan;
                transferOutDetail.vendorcode = solotr.kodesupplier;
                transferOutDetail.vendorname = solotr.nama;
                transferOutDetail.totalpo = db.tbltdPemakaians.Where(p => p.NoFaktur == nofaktur).DistinctBy(p => p.reff).ToList().Count;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.items = new List<GoodReceiptPOItemDetailClass>();
                var detailstr = (from k in db.tbltdPemakaians.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.qty,
                                     qtyterima = (int)k.qty,
                                     e.satuan,
                                     k.reff
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.qty,
                                     g.Key.qtyterima,
                                     g.Key.satuan,
                                     g.Key.reff
                                 }).ToList();
                int totalqty = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    GoodReceiptPOItemDetailClass viewCartClass = new GoodReceiptPOItemDetailClass();
                    viewCartClass.kodebarang = detailstr[i].kodebarang;
                    viewCartClass.namabarang = detailstr[i].nama;
                    viewCartClass.order = (int)detailstr[i].qty;
                    viewCartClass.qtyterima = (int)detailstr[i].qtyterima;
                    viewCartClass.satuan = detailstr[i].satuan;
                    viewCartClass.poreff = detailstr[i].reff;
                    transferOutDetail.items.Add(viewCartClass);
                    totalqty = totalqty + detailstr[i].qtyterima;
                }
                transferOutDetail.totalqty = totalqty;
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsPurchaseRequest(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            PurchaseRequestDetailClass transferOutDetail = new PurchaseRequestDetailClass();
            var solotr = (from m in db.tblthPRs.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier)
                          group new { m, l1 } by new
                          {
                              m.NoFaktur,
                              m.modidate,
                              m.tgl,
                              m.keterangan,
                              l1.kodesupplier,
                              l1.nama,
                              m.tglrequired
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.modidate,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodesupplier,
                              g.Key.nama,
                              g.Key.tglrequired
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.keterangan = solotr.keterangan;
                transferOutDetail.vendorcode = solotr.kodesupplier;
                transferOutDetail.vendorname = solotr.nama;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.modidate = solotr.tgl.ToString("dd-MMM-yyyy HH:mm");
                transferOutDetail.reqdate = solotr.tglrequired.ToString("dd-MMM-yyyy");
                transferOutDetail.items = new List<PurchaseRequestItemDetailClass>();
                var detailstr = (from k in db.tbltdPRs.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.qty,
                                     qtyterima = (int)k.qty,
                                     e.satuan,
                                     k.reffso,
                                     k.hargajual
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.qty,
                                     g.Key.qtyterima,
                                     g.Key.satuan,
                                     g.Key.reffso,
                                     g.Key.hargajual
                                 }).ToList();
                float totalqty = 0;
                double totalamount = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    PurchaseRequestItemDetailClass viewCartClass = new PurchaseRequestItemDetailClass();
                    viewCartClass.kodebarang = detailstr[i].kodebarang;
                    viewCartClass.namabarang = detailstr[i].nama;
                    viewCartClass.order = (float)detailstr[i].qty;
                    viewCartClass.qtyterima = detailstr[i].qtyterima;
                    viewCartClass.satuan = detailstr[i].satuan;
                    viewCartClass.poreff = detailstr[i].reffso;
                    viewCartClass.harga = (double)detailstr[i].hargajual;
                    viewCartClass.strharga = Convert.ToDecimal((double)detailstr[i].hargajual).ToString("#,###");
                    totalqty = totalqty + (float)detailstr[i].qty;
                    totalamount = totalamount + ((double)detailstr[i].qty * (double)detailstr[i].hargajual);
                    transferOutDetail.items.Add(viewCartClass);
                }
                transferOutDetail.totalqty = totalqty;
                transferOutDetail.totalamount = totalamount;
                transferOutDetail.strtotalamount = Convert.ToDecimal(totalamount).ToString("#,###");
            }
            return View(transferOutDetail);
        }

        public JsonResult getPOFromVendor(string vendorcode)
        {
            List<ChoosePOClass> polist = new List<ChoosePOClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                //SqlConnection conn = new SqlConnection("Data Source=116.254.101.239,3396;Initial Catalog=POSVIT_JT;Persist Security Info=True;User ID=sa;Password=P@ssw0rd");
                SqlDataReader rdr = null;
                conn.Open();
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                    cmd.Parameters.Add(new SqlParameter("@kodesupplier", vendorcode));
                    cmd.Parameters.Add(new SqlParameter("@rekap", 1));
                    cmd.Parameters.Add(new SqlParameter("@docnum", 0));
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        string docnum = rdr["DocNum"].ToString();
                        ChoosePOClass solopo = new ChoosePOClass();
                        solopo.nopo = docnum;
                        polist.Add(solopo);
                    }
                }
                catch (Exception e)
                {
                    string errormassage = e.Message;
                }
                finally
                {
                    conn.Close();
                }


                //var checkpo = db.vw_grpo.Where(p => p.basecard == vendorcode && p.U_VIT_ToStr == kodefrom).DistinctBy(p => p.DocNum).ToList();
                //for (int i = 0; i < checkpo.Count ; i++)
                //{
                //    int docnum = checkpo[i].DocNum;
                //    ChoosePOClass solopo = new ChoosePOClass();
                //    solopo.nopo = checkpo[i].DocNum.ToString();
                //    solopo.itempo = db.vw_grpo.Where(p => p.DocNum == docnum).ToList().Count;
                //    polist.Add(solopo);
                //}
            }
            return Json(polist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPOWithoutVendor()
        {
            List<ChoosePOWithoutClass> polist = new List<ChoosePOWithoutClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                try
                {
                    SqlDataReader rdr = null;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                    cmd.Parameters.Add(new SqlParameter("@kodesupplier", ""));
                    cmd.Parameters.Add(new SqlParameter("@rekap", 0));
                    cmd.Parameters.Add(new SqlParameter("@docnum", 0));
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        string docnum = rdr["DocNum"].ToString();
                        ChoosePOWithoutClass solopo = new ChoosePOWithoutClass();
                        solopo.docentry = Convert.ToInt32(rdr["docentry"]);
                        solopo.pocode = docnum;
                        solopo.itemcode = rdr["ItemCode"].ToString();
                        solopo.itemname = rdr["nama"].ToString();
                        solopo.openqty = Convert.ToInt32(rdr["OpenQty"]);
                        solopo.qty = Convert.ToInt32(rdr["Quantity"]);
                        solopo.vendorcode = rdr["CardCode"].ToString();
                        solopo.vendorname = rdr["CardName"].ToString();
                        DateTime dt = Convert.ToDateTime(rdr["DocDate"].ToString());
                        solopo.docdate = dt.ToString("dd-MMM-yyyy");
                        string vc = solopo.vendorcode;
                        var checkvendor = db.tblSuppliers.Where(p => p.kodesupplier == vc && p.aktif == true).FirstOrDefault();
                        if (checkvendor != null)
                        {
                            polist.Add(solopo);
                        }
                    }
                }
                catch (Exception e)
                {
                    string errormassage = e.Message;
                }
                finally
                {
                    conn.Close();
                }


                //var checkpo = db.vw_grpo.Where(p => p.basecard == vendorcode && p.U_VIT_ToStr == kodefrom).DistinctBy(p => p.DocNum).ToList();
                //for (int i = 0; i < checkpo.Count ; i++)
                //{
                //    int docnum = checkpo[i].DocNum;
                //    ChoosePOClass solopo = new ChoosePOClass();
                //    solopo.nopo = checkpo[i].DocNum.ToString();
                //    solopo.itempo = db.vw_grpo.Where(p => p.DocNum == docnum).ToList().Count;
                //    polist.Add(solopo);
                //}
            }
            return Json(polist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPOWithVendor(string vendorcode)
        {
            List<ChoosePOWithoutClass> polist = new List<ChoosePOWithoutClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                try
                {
                    SqlDataReader rdr = null;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                    cmd.Parameters.Add(new SqlParameter("@kodesupplier", vendorcode));
                    cmd.Parameters.Add(new SqlParameter("@rekap", 0));
                    cmd.Parameters.Add(new SqlParameter("@docnum", 0));
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        string docnum = rdr["DocNum"].ToString();
                        ChoosePOWithoutClass solopo = new ChoosePOWithoutClass();
                        solopo.docentry = Convert.ToInt32(rdr["docentry"]);
                        solopo.pocode = docnum;
                        solopo.itemcode = rdr["ItemCode"].ToString();
                        solopo.itemname = rdr["nama"].ToString();
                        solopo.openqty = Convert.ToInt32(rdr["OpenQty"]);
                        solopo.qty = Convert.ToInt32(rdr["Quantity"]);
                        solopo.vendorcode = rdr["CardCode"].ToString();
                        solopo.vendorname = rdr["CardName"].ToString();
                        DateTime dt = Convert.ToDateTime(rdr["DocDate"].ToString());
                        solopo.docdate = dt.ToString("dd-MMM-yyyy");
                        polist.Add(solopo);
                    }
                }
                catch (Exception e)
                {
                    string errormassage = e.Message;
                }
                finally
                {
                    conn.Close();
                }
                

                //var checkpo = db.vw_grpo.Where(p => p.basecard == vendorcode && p.U_VIT_ToStr == kodefrom).DistinctBy(p => p.DocNum).ToList();
                //for (int i = 0; i < checkpo.Count ; i++)
                //{
                //    int docnum = checkpo[i].DocNum;
                //    ChoosePOClass solopo = new ChoosePOClass();
                //    solopo.nopo = checkpo[i].DocNum.ToString();
                //    solopo.itempo = db.vw_grpo.Where(p => p.DocNum == docnum).ToList().Count;
                //    polist.Add(solopo);
                //}
            }
            return Json(polist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getItemFromVendor(string vendorcode)
        {
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var itemdb = (from k in db.tblKategoris
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori && p.aktif == true)
                              //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodefrom)
                          group new { e, k } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              //pr.kodesupplier,
                              //namaprodusen = pr.nama,
                              e.satuan,
                              e.hargajual,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodesupplier,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.satuan,
                              g.Key.hargajual,
                              g.Key.itemcode
                          }).ToList().OrderBy(p => p.nama);
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    kodeprodusen = "",
                    namaprodusen = "",
                    //stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                    satuan = itemdbs.satuan,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult getPurchaseItem(string vendorcode)
        {
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            try
            {
                var itemdb = (from k in db.tblKategoris
                              from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori && p.aktif == true && p.dibeli == true)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier && p.aktif == true)
                              from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodefrom && p.kodekategori == k.kodekategori)
                              from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodefrom && p.kodeprodusen == e.kodeprodusen)
                                  //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodefrom)
                              group new { e, k } by new
                              {
                                  //u.stokkeluar,
                                  //u.stokmasuk,
                                  e.kodebarang,
                                  e.nama,
                                  k.kodekategori,
                                  namakategori = k.nama,
                                  s.kodesupplier,
                                  namasupplier = s.nama,
                                  s.aktif,
                                  e.satuan,
                                  e.hargajual,
                                  e.itemcode
                              }
                       into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  g.Key.kodesupplier,
                                  g.Key.namasupplier,
                                  g.Key.aktif,
                                  //g.Key.stokkeluar,
                                  //g.Key.stokmasuk,
                                  g.Key.satuan,
                                  g.Key.hargajual,
                                  g.Key.itemcode,
                                  stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodefrom).FirstOrDefault()
                              }).ToList().OrderBy(p => p.kodebarang);
                int i = 0;
                int count = itemdb.Count();
                if (count > 0)
                {
                    foreach (var itemdbs in itemdb)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodesupplier,
                            namaprodusen = itemdbs.namasupplier,
                            aktif = itemdbs.aktif,
                            stock = itemdbs.stocks != null ? (int)itemdbs.stocks.stokmasuk - (int)itemdbs.stocks.stokkeluar : 0,
                            satuan = itemdbs.satuan,
                            harga = (double)itemdbs.hargajual,
                            strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                            itemcode = itemdbs.itemcode
                        };
                        journallist.Add(soloitem);
                        i++;
                    };
                }
                else
                {
                    var itemdbcheck = (from k in db.tblKategoris
                                  from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori && p.aktif == true && p.dibeli == true)
                                  from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier && p.aktif == true)
                                  group new { e, k } by new
                                  {
                                      e.kodebarang,
                                      e.nama,
                                      k.kodekategori,
                                      namakategori = k.nama,
                                      s.kodesupplier,
                                      namasupplier = s.nama,
                                      s.aktif,
                                      e.satuan,
                                      e.hargajual,
                                      e.itemcode
                                  }
                       into g
                                  select new
                                  {
                                      g.Key.kodebarang,
                                      g.Key.nama,
                                      g.Key.kodekategori,
                                      g.Key.namakategori,
                                      g.Key.kodesupplier,
                                      g.Key.namasupplier,
                                      g.Key.aktif,
                                      g.Key.satuan,
                                      g.Key.hargajual,
                                      g.Key.itemcode,
                                      stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodefrom).FirstOrDefault()
                                  }).ToList().OrderBy(p => p.kodebarang);
                    foreach (var itemdbs in itemdbcheck)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodesupplier,
                            namaprodusen = itemdbs.namasupplier,
                            aktif = itemdbs.aktif,
                            stock = itemdbs.stocks != null ? (int)itemdbs.stocks.stokmasuk - (int)itemdbs.stocks.stokkeluar : 0,
                            satuan = itemdbs.satuan,
                            harga = (double)itemdbs.hargajual,
                            strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                            itemcode = itemdbs.itemcode
                        };
                        journallist.Add(soloitem);
                        i++;
                    };
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult getPODetailFromPO(string pono)
        {
            int intpono = Int32.Parse(pono);
            ChoosePOClass solopo = new ChoosePOClass();
            solopo.nopo = pono.ToString();
            solopo.details = new List<GoodReceiptPOItemDetailClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                //SqlConnection conn = new SqlConnection("Data Source=116.254.101.239,3396;Initial Catalog=POSVIT_JT;Persist Security Info=True;User ID=sa;Password=P@ssw0rd");
                SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                SqlDataReader rdr = null;
                conn.Open();
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                    cmd.Parameters.Add(new SqlParameter("@kodesupplier", ""));
                    cmd.Parameters.Add(new SqlParameter("@rekap", 0));
                    cmd.Parameters.Add(new SqlParameter("@docnum", intpono));
                    rdr = cmd.ExecuteReader();
                    int i = 1;
                    while (rdr.Read())
                    {
                        string itemcode = rdr["ItemCode"].ToString();
                        var checkitem = db.tblHBarangs.Where(p => p.kodebarang == itemcode).FirstOrDefault();
                        if (checkitem != null)
                        {
                            var soloitem = new GoodReceiptPOItemDetailClass
                            {
                                kodebarang = checkitem.kodebarang,
                                namabarang = checkitem.nama,
                                open = Convert.ToInt32(rdr["OpenQty"]),
                                order = Convert.ToInt32(rdr["Quantity"]),
                                satuan = rdr["DocNum"].ToString(),
                                lineNum = Convert.ToInt32(rdr["LineNum"])
                            };
                            solopo.details.Add(soloitem);
                            solopo.itempo = i;
                            solopo.docentry = Convert.ToInt32(rdr["docentry"]);
                            i++;
                        }
                    }
                }
                catch (Exception e)
                {
                    string errormassage = e.Message;
                }
                finally
                {
                    conn.Close();
                }
                
                //var checkpo = db.vw_grpo.Where(p => p.DocNum == intpono).ToList();
                //solopo.nopo = pono.ToString();
                //solopo.itempo = checkpo.Count();
                //solopo.details = new List<GoodReceiptPOItemDetailClass>();
                //foreach (var itemdbs in checkpo)
                //{
                //    string kodebarang = itemdbs.ItemCode;
                //    var checkitem = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).FirstOrDefault();
                //    if (checkitem != null)
                //    {
                //        var soloitem = new GoodReceiptPOItemDetailClass
                //        {
                //            kodebarang = checkitem.kodebarang,
                //            namabarang = checkitem.nama,
                //            order = (int)itemdbs.Quantity,
                //            satuan = itemdbs.DocNum.ToString()
                //        };
                //        solopo.details.Add(soloitem);
                //    }
                //};
            }
            return Json(solopo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPODetailFromPOList(string polist)
        {
            List<GoodReceiptPOItemDetailClass> podetails = new List<GoodReceiptPOItemDetailClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            var checkwhscode = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            if (checkwhscode != null)
            {
                string[] solopo = polist.Split(';');
                for (int i = 0; i < solopo.Length; i++)
                {
                    int intsolopo = solopo[i].Equals("") ? 0 : Int32.Parse(solopo[i]);
                    GoodReceiptPOItemDetailClass podetail = new GoodReceiptPOItemDetailClass();
                    //SqlConnection conn = new SqlConnection("Data Source=116.254.101.239,3396;Initial Catalog=POSVIT_JT;Persist Security Info=True;User ID=sa;Password=P@ssw0rd");
                    SqlConnection conn = new SqlConnection(WebConfigurationManager.AppSettings["connectionStringOSA"].ToString());
                    SqlDataReader rdr = null;
                    conn.Open();
                    try
                    {
                        SqlCommand cmd = new SqlCommand("sp_opengrpo", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@VIT_ToStr", checkwhscode.WhsCode));
                        cmd.Parameters.Add(new SqlParameter("@kodesupplier", ""));
                        cmd.Parameters.Add(new SqlParameter("@rekap", 0));
                        cmd.Parameters.Add(new SqlParameter("@docnum", intsolopo));
                        rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            string itemcode = rdr["ItemCode"].ToString();
                            var checkitem = db.tblHBarangs.Where(p => p.kodebarang == itemcode).FirstOrDefault();
                            if (checkitem != null)
                            {
                                var soloitem = new GoodReceiptPOItemDetailClass
                                {
                                    kodebarang = checkitem.kodebarang,
                                    namabarang = checkitem.nama,
                                    open = Convert.ToInt32(rdr["OpenQty"]),
                                    order = Convert.ToInt32(rdr["Quantity"]),
                                    satuan = rdr["DocNum"].ToString()
                                };
                                podetails.Add(soloitem);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        string errormassage = e.Message;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    
                    //var checkpo = db.vw_grpo.Where(p => p.DocNum == intsolopo).ToList();
                    //foreach (var itemdbs in checkpo)
                    //{
                    //    string kodebarang = itemdbs.ItemCode;
                    //    var checkitem = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).FirstOrDefault();
                    //    if (checkitem != null)
                    //    {
                    //        var soloitem = new GoodReceiptPOItemDetailClass
                    //        {
                    //            kodebarang = checkitem.kodebarang,
                    //            namabarang = checkitem.nama,
                    //            order = (int)itemdbs.Quantity,
                    //            satuan = itemdbs.DocNum.ToString()
                    //        };
                    //        podetails.Add(soloitem);
                    //    }
                    //};
                }
            }
            return Json(podetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getItemFromStoreSTO()
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi && p.kodeprodusen == m.kodeprodusen)
                              //from p in db.tblProdusens.Where(p => p.kodeprodusen == m.kodeprodusen)
                              //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelokasi)
                          group new { m, l1, s, ls, lp } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              m.kodebarang,
                              m.nama,
                              ls.kodekategori,
                              namakategori = l1.nama,
                              //p.kodeprodusen,
                              //namaprodusen = p.nama,
                              m.hargajual,
                              m.satuan,
                              m.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodeprodusen,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode
                          }).OrderBy(p => p.kodebarang).ToList();
            int i = 0;
            int count = itemdb.Count();
            if (count > 0)
            {
                foreach (var itemdbs in itemdb)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        //kodeprodusen = itemdbs.kodeprodusen,
                        //namaprodusen = itemdbs.namaprodusen,
                        //stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            else
            {
                var itemdbcheck = (from m in db.tblHBarangs.Where(p => p.aktif == true)
                              from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                              group new { m, l1, s } by new
                              {
                                  m.kodebarang,
                                  m.nama,
                                  l1.kodekategori,
                                  namakategori = l1.nama,
                                  m.hargajual,
                                  m.satuan,
                                  m.itemcode
                              }
                       into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  g.Key.hargajual,
                                  g.Key.satuan,
                                  g.Key.itemcode
                              }).OrderBy(p => p.kodebarang).ToList();
                foreach (var itemdbs in itemdbcheck)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromStoreSTOS()
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi && p.kodeprodusen == m.kodeprodusen)
                              //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelokasi)
                          group new { m, l1, s, ls, lp } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              m.kodebarang,
                              m.nama,
                              l1.kodekategori,
                              namakategori = l1.nama,
                              //pr.kodeprodusen,
                              //namaprodusen = pr.nama,
                              m.hargajual,
                              m.satuan,
                              m.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodeprodusen,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode,
                              stoks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                          }).OrderBy(p => p.kodebarang).ToList();
            int i = 0;
            int count = itemdb.Count();
            if (count > 0)
            {
                foreach (var itemdbs in itemdb)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        //kodeprodusen = itemdbs.kodeprodusen,
                        //namaprodusen = itemdbs.namaprodusen,
                        stock = itemdbs.stoks != null ? (int)itemdbs.stoks.stokmasuk - (int)itemdbs.stoks.stokkeluar : 0,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            else
            {
                var itemdbcheck = (from m in db.tblHBarangs.Where(p => p.aktif == true)
                                   from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                                   from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                                   group new { m, l1, s } by new
                                   {
                                       m.kodebarang,
                                       m.nama,
                                       l1.kodekategori,
                                       namakategori = l1.nama,
                                       m.hargajual,
                                       m.satuan,
                                       m.itemcode
                                   }
                       into g
                                   select new
                                   {
                                       g.Key.kodebarang,
                                       g.Key.nama,
                                       g.Key.kodekategori,
                                       g.Key.namakategori,
                                       g.Key.hargajual,
                                       g.Key.satuan,
                                       g.Key.itemcode
                                   }).OrderBy(p => p.kodebarang).ToList();
                foreach (var itemdbs in itemdbcheck)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemForGoodsReturn()
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from m in db.tblHBarangs.Where(p => p.aktif == true && p.dibeli == true)
                          from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                          from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == m.kodekategori)
                          from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi && p.kodeprodusen == m.kodeprodusen)
                              //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelokasi)
                          group new { m, l1, s, ls, lp } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              m.kodebarang,
                              m.nama,
                              l1.kodekategori,
                              namakategori = l1.nama,
                              //pr.kodeprodusen,
                              //namaprodusen = pr.nama,
                              m.hargajual,
                              m.satuan,
                              m.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodeprodusen,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode,
                              stoks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                          }).OrderBy(p => p.kodebarang).ToList();
            int i = 0;
            int count = itemdb.Count();
            if (count > 0)
            {
                foreach (var itemdbs in itemdb)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        //kodeprodusen = itemdbs.kodeprodusen,
                        //namaprodusen = itemdbs.namaprodusen,
                        stock = itemdbs.stoks != null ? (int)itemdbs.stoks.stokmasuk - (int)itemdbs.stoks.stokkeluar : 0,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            else
            {
                var itemdbcheck = (from m in db.tblHBarangs.Where(p => p.aktif == true && p.dibeli == true)
                              from l1 in db.tblKategoris.Where(p => p.kodekategori == m.kodekategori)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == m.kodesupplier).DefaultIfEmpty()
                              group new { m, l1, s } by new
                              {
                                  //u.stokkeluar,
                                  //u.stokmasuk,
                                  m.kodebarang,
                                  m.nama,
                                  l1.kodekategori,
                                  namakategori = l1.nama,
                                  //pr.kodeprodusen,
                                  //namaprodusen = pr.nama,
                                  m.hargajual,
                                  m.satuan,
                                  m.itemcode
                              }
                       into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  //g.Key.kodeprodusen,
                                  //g.Key.namaprodusen,
                                  //g.Key.stokkeluar,
                                  //g.Key.stokmasuk,
                                  g.Key.hargajual,
                                  g.Key.satuan,
                                  g.Key.itemcode,
                                  stoks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                              }).OrderBy(p => p.kodebarang).ToList();
                foreach (var itemdbs in itemdbcheck)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        //kodeprodusen = itemdbs.kodeprodusen,
                        //namaprodusen = itemdbs.namaprodusen,
                        stock = itemdbs.stoks != null ? (int)itemdbs.stoks.stokmasuk - (int)itemdbs.stoks.stokkeluar : 0,
                        harga = (double)itemdbs.hargajual,
                        strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                        satuan = itemdbs.satuan,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromBarcode(string barcode)
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen && p.aktif == true && p.itemcode == barcode)
                          from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelokasi)
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == k.kodekategori)
                          group new { e, u, k } by new
                          {
                              u.stokkeluar,
                              u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama,
                              e.hargajual,
                              e.satuan,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.stokkeluar,
                              g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode
                          }).OrderBy(p => p.nama).ToList();
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    kodeprodusen = itemdbs.kodeprodusen,
                    namaprodusen = itemdbs.namaprodusen,
                    stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    satuan = itemdbs.satuan,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromBarcodeWithoutStok(string barcode)
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen && p.aktif == true && p.itemcode == barcode)
                          group new { e, k } by new
                          {
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama,
                              e.hargajual,
                              e.satuan,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode,
                              stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                          }).OrderBy(p => p.nama).ToList();
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    kodeprodusen = itemdbs.kodeprodusen,
                    namaprodusen = itemdbs.namaprodusen,
                    stock = itemdbs.stocks != null ? (int)itemdbs.stocks.stokmasuk - (int)itemdbs.stocks.stokkeluar : 0,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    satuan = itemdbs.satuan,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromBarcodeInPR(string barcode)
        {
            string kodelokasi = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori && p.aktif == true && p.dibeli == true && p.itemcode == barcode)
                              //from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodefrom)
                          group new { e, k } by new
                          {
                              //u.stokkeluar,
                              //u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              //pr.kodesupplier,
                              //namaprodusen = pr.nama,
                              e.satuan,
                              e.hargajual,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              //g.Key.kodesupplier,
                              //g.Key.namaprodusen,
                              //g.Key.stokkeluar,
                              //g.Key.stokmasuk,
                              g.Key.satuan,
                              g.Key.hargajual,
                              g.Key.itemcode,
                              stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                          }).ToList().OrderBy(p => p.nama);
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    stock = itemdbs.stocks != null ? (int)itemdbs.stocks.stokmasuk - (int)itemdbs.stocks.stokkeluar : 0,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    satuan = itemdbs.satuan,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }
    }
}