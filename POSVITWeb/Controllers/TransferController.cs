﻿using Newtonsoft.Json;
using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class TransferController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult AddTransferOut()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checkfrom = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            transferOutDetail.fromstore = checkfrom.nama;
            transferOutDetail.kodefrom = kodelokasi;
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => (!p.kodelokasi.Contains("000") && p.kodelokasi != "PUSAT") && p.initial != "" && 
                !p.kodelokasi.Equals(kodelokasi) && p.aktif == true).ToList();
            transferOutDetail.type = new List<ChooseTypeClass>();
            var typemutasi = db.tblIntegrasiMutasiStoks.ToList();
            for (int i = 0; i < typemutasi.Count; i++)
            {
                ChooseTypeClass chooseType = new ChooseTypeClass();
                chooseType.typename = typemutasi[i].kode;
                transferOutDetail.type.Add(chooseType);
            }
            transferOutDetail.items = getItemFromStore();
            transferOutDetail.trlist = new List<ChooseTransferRequestClass>();
            //var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
            //if (checklokasi != null)
            //{
            //    kodelokasi = checklokasi.InTransit;
            //}
            var transferreqlist = (from m in db.tblthMutasiStokTRs.Where(p => p.kodelokasi == kodelokasi)
                                   from fs in db.tblLokasis.Where(p => p.InTransit == m.tujuan)
                                   group new { m } by new
                                   {
                                       m.NoFaktur,
                                       m.nourut,
                                       fs.nama,
                                       m.tgl,
                                       m.keterangan
                                   }
                                   into g
                                   select new
                                   {
                                       g.Key.NoFaktur,
                                       g.Key.nourut,
                                       g.Key.nama,
                                       g.Key.tgl,
                                       g.Key.keterangan,
                                       checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur && p.closed == true).ToList().Count,
                                       checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                                   }).OrderByDescending(p => p.nourut).ToList();
            //var transferreqlist = db.tblthMutasiStokTRs.Where(p => p.kodelokasi == kodelokasi).OrderByDescending(p => p.nourut).ToList();
            for (int i = 0; i < transferreqlist.Count; i++)
            {
                //string nofakturtr = transferreqlist[i].NoFaktur;
                //var checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofakturtr && p.closed == true).ToList().Count;
                //var checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofakturtr).ToList().Count;
                if (transferreqlist[i].checktbltdmutasistoktrclose != transferreqlist[i].checktbltdmutasistoktrall.Count)
                {
                    ChooseTransferRequestClass transferRequestClass = new ChooseTransferRequestClass();
                    transferRequestClass.nofaktur = transferreqlist[i].NoFaktur;
                    transferRequestClass.fromstore = transferreqlist[i].nama;
                    transferRequestClass.qty = (float)transferreqlist[i].checktbltdmutasistoktrall.Sum(p => p.qty) - (float)transferreqlist[i].checktbltdmutasistoktrall.Sum(p => p.kirim);
                    transferRequestClass.tgl = transferreqlist[i].tgl.ToString("dd-MMM-yyyy HH:mm");
                    transferRequestClass.notes = transferreqlist[i].keterangan;
                    transferOutDetail.trlist.Add(transferRequestClass);
                }
            }
            transferOutDetail.tsomorethantr = Session["tso_eq_tr"].ToString() == "True" ? "t" : "f";
            transferOutDetail.tsogreaterthantr = Session["tso_gt_tr"].ToString() == "True" ? "t" : "f";
            transferOutDetail.tsocopytr = Session["tso_copy_tr"].ToString() == "True" ? "t" : "f";
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        //[HttpPost]
        //public ActionResult AddTransferOut(TransferOutDetailClass param)
        //{
        //    if (Session["role_id"] == null)
        //    {
        //        return RedirectToAction("Login");
        //    }
        //    try
        //    {
        //        var dt = DateTime.ParseExact(param.tanggal, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //        string nofaktur = "TSO/" + param.kodefrom + "/";
        //        var checkpenjualan = db.tblthMutasiStoks
        //            .Where(p => p.kodelokasi == param.kodefrom).ToList().OrderByDescending(p => p.NoFaktur);
        //        if (checkpenjualan.Count() != 0)
        //        {
        //            if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM")))
        //            {
        //                string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
        //                nofaktur = nofaktur + (Int32.Parse(splitfaktur[2]) + 1);
        //            }
        //            else
        //            {
        //                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + "00001";
        //            }
        //        }
        //        db.sp_SimpanHeaderMutasiStok(nofaktur, dt, param.comment, Session["user_id"].ToString(), "1",
        //            param.kodefrom, param.kodeto, "", param.tipe);
        //        List<ViewCartClass> PSJD = JsonConvert.DeserializeObject<List<ViewCartClass>>(param.cartstring);
        //        foreach (var dataset in PSJD)
        //        {
        //            db.sp_SimpanDetailMutasiStok(nofaktur, dataset.itemcode, (decimal)dataset.itemqty, "",
        //                dt, Session["kode_lokasi"].ToString(), "");
        //        }
        //        //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
        //        //for (int i = 0; i < userdb.Count; i++)
        //        //{
        //        //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return RedirectToAction("TransferOut");
        //}

        public ActionResult SubmitTransferOut(string tanggal, string keterangan, string noreference, string tostore, string type,
            string cart, string mode)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "TSO/" + initialfrom + "/";
            var checkpenjualan = db.tblthMutasiStoks
                .Where(p => p.kodelokasi == kodefrom && p.NoFaktur.Contains("TSO")).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == tostore).FirstOrDefault();
            string transittostore = tostore;
            if (checklokasi != null)
            {
                transittostore = checklokasi.InTransit;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderMutasiStok(nofaktur, dt, keterangan, Session["user_id"].ToString(), "1",
                        kodefrom, transittostore, noreference, type);
                    string[] splitcart = cart.Split(';');
                    string cartdetail = "";
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        if (float.Parse(splitrowcart[1]) > 0)
                        {
                            if (!cartdetail.Contains(splitrowcart[0]))
                            {
                                db.sp_SimpanDetailMutasiStok(nofaktur, splitrowcart[0], (decimal)float.Parse(splitrowcart[1]), noreference,
                                    dt, kodefrom, noreference);
                            }
                            if (cartdetail == "")
                            {
                                cartdetail = splitrowcart[0];
                            }
                            else
                            {
                                cartdetail = cartdetail + ";" + splitrowcart[0];
                            }
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Transaction Out " + nofaktur + " Has Been Created..";
                    var userdb = db.musers.Where(p => p.kodelokasi == kodefrom && p.tokenFB != null).Select(item => item.tokenFB).Distinct().ToArray();
                    for (int i = 0; i < userdb.Length; i++)
                    {
                        //SendNotifOutput notifresult = sendNotif("Transfer Out", "Stock Changed", "e1k2jTDgxFE:APA91bHqtRZxkOpHWEW4IqYy93FG5Rf9-v61nh_kBTJX8UdLBuAwfeYRz3-vmjb0yLJfd5l44rD69-Di_-hwdgo27qwhjxxTwsNrNHLmCk8Iuja8M8zV7FgZEkL-8U2koCkqoDxg4Jmf",
                        //            cart, "1");
                        SendNotifOutput notifresult = sendNotif("Transfer Out", "Stock Changed", userdb[i], cart, "1");
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    messages = "Error Failed To Create Transfer Out";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult SubmitTransferIn(string tanggal, string keterangan, string noreference, string fromstore,
            string tostore, string type, string cart, string mode)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isTrue = true;
            var checkrefftso = db.tblthMutasiStoks.Where(p => p.refftr == noreference).FirstOrDefault();
            if (checkrefftso != null)
            {
                int checkqty = 0;
                var checktso = db.tbltdMutasiStoks.Where(p => p.NoFaktur == noreference).ToList();
                for (int i = 0; i < checktso.Count; i++)
                {
                    if (checktso[i].qty <= checktso[i].kirim)
                    {
                        checkqty++;
                    }
                }
                if (checkqty == checktso.Count)
                {
                    isTrue = false;
                }
                else
                {
                    isTrue = true;
                }
            }
            else
            {
                isTrue = true;
            }
            if (isTrue)
            {
                bool isValid = true;
                string nofaktur = "";
                string messages = "";
                var dt = DateTime.ParseExact(tanggal, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dt = DateTime.Now;
                string kodefrom = Session["kode_lokasi"].ToString();
                string initialfrom = Session["initial_lokasi"].ToString();
                nofaktur = "TSI/" + initialfrom + "/";
                var checkpenjualan = db.tblthMutasiStoks
                    .Where(p => p.NoFaktur.Contains(nofaktur)).OrderByDescending(p => p.nourut).ToList();
                if (checkpenjualan.Count() != 0)
                {
                    if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                    {
                        string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                        nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                    }
                    else
                    {
                        nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                    }
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
                var checktransitlokasi = db.tblLokasis.Where(p => p.kodelokasi == tostore).FirstOrDefault();
                if (checktransitlokasi != null)
                {
                    fromstore = checktransitlokasi.InTransit;
                }
                var txOptions = new System.Transactions.TransactionOptions();
                txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                    Timeout = TimeSpan.FromSeconds(30)
                }))
                {
                    try
                    {
                        db.sp_SimpanHeaderMutasiStok(nofaktur, dt, keterangan, Session["user_id"].ToString(), "1",
                            fromstore, tostore, noreference, type);
                        string[] splitcart = cart.Split(';');
                        if (splitcart.Length > 0)
                        {
                            int countItem = 0;
                            string checkItem = "";
                            for (int i = 0; i < splitcart.Length; i++)
                            {
                                string[] splitrowcart = splitcart[i].Split('$');
                                if (!checkItem.Contains(splitrowcart[0]))
                                {
                                    var checktdmutasistok = db.tbltdMutasiStoks.Where(p => p.NoFaktur == noreference).ToList();
                                    for (int j = 0; j < checktdmutasistok.Count; j++)
                                    {
                                        if (splitrowcart[0] == checktdmutasistok[j].kodebarang)
                                        {
                                            db.sp_SimpanDetailMutasiStok(nofaktur, splitrowcart[0], (decimal)float.Parse(splitrowcart[1]), noreference,
                                        dt, fromstore, "");
                                            countItem = countItem + 1;
                                        }
                                    }
                                    checkItem = checkItem + ";" + splitrowcart[0];
                                }
                            }
                            if (countItem == splitcart.Length)
                            {
                                transaction.Complete();
                                transaction.Dispose();
                                messages = "Transaction In " + nofaktur + " Has Been Created..";
                                var userdb = db.musers.Where(p => p.kodelokasi == tostore && p.tokenFB != null).Select(item => item.tokenFB).Distinct().ToArray();
                                for (int i = 0; i < userdb.Length; i++)
                                {
                                    //SendNotifOutput notifresult = sendNotif("Transfer In", "New Stock Has Been Received", "e1k2jTDgxFE:APA91bHqtRZxkOpHWEW4IqYy93FG5Rf9-v61nh_kBTJX8UdLBuAwfeYRz3-vmjb0yLJfd5l44rD69-Di_-hwdgo27qwhjxxTwsNrNHLmCk8Iuja8M8zV7FgZEkL-8U2koCkqoDxg4Jmf",
                                    //            cart, "2");
                                    SendNotifOutput notifresult = sendNotif("Transfer In", "New Stock Has Been Received", userdb[i], cart, "2");
                                }
                            }
                            else
                            {
                                messages = "Error Failed To Create Transfer In";
                                transaction.Dispose();
                                isValid = false;
                            }
                            //}
                        }
                        else
                        {
                            messages = "Error Failed To Create Transfer In";
                            transaction.Dispose();
                            isValid = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                        messages = "Error Failed To Create Transfer In";
                        transaction.Dispose();
                        isValid = false;
                    }
                }
                var obj = new
                {
                    valid = isValid,
                    pesansingkat = messages
                };
                return Json(obj);
            }
            else
            {
                var obj = new
                {
                    valid = false,
                    pesansingkat = "Error Reference TSO"
                };
                return Json(obj);
            }
        }

        public ActionResult AddTransferIn()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferInDetailClass transferOutDetail = new TransferInDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodelokasi).FirstOrDefault();
            if (checklokasi != null)
            {
                kodelokasi = checklokasi.InTransit;
            }
            var dt = DateTime.Now.AddDays(-60);
            //var tolist = (from m in db.tblthMutasiStoks.Where(p => p.tujuan == kodelokasi && p.tgl >= dt)
            //            from fs in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
            //            group new { m } by new
            //            {
            //                m.NoFaktur,
            //                m.nourut,
            //                m.kodelokasi,
            //                m.tujuan,
            //                fs.nama,
            //                m.tgl,
            //                m.keterangan
            //            }
            //            into g
            //            select new
            //            {
            //                g.Key.NoFaktur,
            //                g.Key.nourut,
            //                g.Key.kodelokasi,
            //                g.Key.tujuan,
            //                g.Key.nama,
            //                g.Key.tgl,
            //                g.Key.keterangan,
            //                checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
            //            }).OrderByDescending(p => p.nourut).ToList();
            //transferOutDetail.transferouts = new List<ChooseTransferOutClass>();
            //for (int i = 0; i < tolist.Count; i++)
            //{
            //    int countdetailmutasi = 0;
            //    //string faktur = tolist[i].NoFaktur;
            //    //var checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == faktur).ToList();
            //    for (int j = 0; j < tolist[i].checkdetailmutasi.Count; j++)
            //    {
            //        if (tolist[i].checkdetailmutasi[j].qty <= tolist[i].checkdetailmutasi[j].kirim)
            //        {
            //            countdetailmutasi++;
            //        }
            //    }
            //    if (countdetailmutasi != tolist[i].checkdetailmutasi.Count)
            //    {
            //        ChooseTransferOutClass chooseTransferOutClass = new ChooseTransferOutClass();
            //        chooseTransferOutClass.kodefaktur = tolist[i].NoFaktur;
            //        chooseTransferOutClass.fromstore = tolist[i].nama;
            //        chooseTransferOutClass.qty = (float)tolist[i].checkdetailmutasi.Sum(p => p.qty) - (float)tolist[i].checkdetailmutasi.Sum(p => p.kirim);
            //        chooseTransferOutClass.notes = tolist[i].keterangan;
            //        chooseTransferOutClass.tgl = tolist[i].tgl.ToString("dd-MMM-yyyy HH:mm");
            //        transferOutDetail.transferouts.Add(chooseTransferOutClass);
            //    }
            //}
            var tolist = (from m in db.vw_opentsout.Where(p => p.tujuan == kodelokasi && p.tgl >= dt)
                          from fs in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          group new { m } by new
                          {
                              m.NoFaktur,
                              m.kodelokasi,
                              m.tujuan,
                              fs.nama,
                              m.tgl,
                              m.keterangan
                          }
                        into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.kodelokasi,
                              g.Key.tujuan,
                              g.Key.nama,
                              g.Key.tgl,
                              g.Key.keterangan
                          }).OrderByDescending(p => p.tgl).ToList();
            transferOutDetail.transferouts = new List<ChooseTransferOutClass>();
            for (int i = 0; i < tolist.Count; i++)
            {
                string nofaktur = "";
                if (nofaktur != tolist[i].NoFaktur)
                {
                    ChooseTransferOutClass chooseTransferOutClass = new ChooseTransferOutClass();
                    chooseTransferOutClass.kodefaktur = tolist[i].NoFaktur;
                    chooseTransferOutClass.fromstore = tolist[i].nama;
                    chooseTransferOutClass.qty = 0;
                    chooseTransferOutClass.notes = tolist[i].keterangan;
                    chooseTransferOutClass.tgl = tolist[i].tgl.ToString("dd-MMM-yyyy HH:mm");
                    transferOutDetail.transferouts.Add(chooseTransferOutClass);
                    nofaktur = tolist[i].NoFaktur;
                }
            }
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => !p.kodelokasi.Contains("000") && !p.kodelokasi.Contains("T")).ToList();
            transferOutDetail.type = new List<ChooseTypeClass>();
            var typemutasi = db.tblIntegrasiMutasiStoks.ToList();
            for (int i = 0; i < typemutasi.Count; i++)
            {
                ChooseTypeClass chooseType = new ChooseTypeClass();
                chooseType.typename = typemutasi[i].kode;
                transferOutDetail.type.Add(chooseType);
            }
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        public JsonResult getFromStore(string nofaktur)
        {
            GetTransferOutClass getTransferOutClass = new GetTransferOutClass();
            getTransferOutClass.carts = new List<ViewCartClass>();
            var tsodetail = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                             from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                             from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                             group new { m, l1, l2 } by new
                             {
                                 m.NoFaktur,
                                 m.tgl,
                                 m.keterangan,
                                 kodefrom = l1.kodelokasi,
                                 namafrom = l1.nama,
                                 kodeto = l2.kodelokasi,
                                 namato = l2.nama,
                                 m.refftr
                             }
                          into g
                             select new
                             {
                                 g.Key.NoFaktur,
                                 g.Key.tgl,
                                 g.Key.keterangan,
                                 g.Key.kodefrom,
                                 g.Key.namafrom,
                                 g.Key.kodeto,
                                 g.Key.namato,
                                 g.Key.refftr
                             }).FirstOrDefault();
            if (tsodetail != null)
            {
                getTransferOutClass.kodelokasi = tsodetail.kodefrom;
                getTransferOutClass.namalokasi = tsodetail.namafrom;
                getTransferOutClass.keterangan = tsodetail.keterangan;
                var checklokasi = db.tblLokasis.Where(p => p.InTransit == tsodetail.kodeto).FirstOrDefault();
                if (checklokasi != null)
                {
                    getTransferOutClass.kodelokasito = checklokasi.kodelokasi;
                    getTransferOutClass.namalokasito = checklokasi.nama;
                }
                getTransferOutClass.refftr = (tsodetail.refftr.Equals("0") || tsodetail.refftr.Equals("")) ? "-" : tsodetail.refftr;
            }
            getTransferOutClass.carts = (from td in db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                                         from i in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                                         //from l in db.tblLokasis.Where(p => p.kodelokasi == td.kodelokasi)
                                         select new ViewCartClass
                                         {
                                             itemcode = i.kodebarang,
                                             itemname = i.nama,
                                             itemqty = (float)td.qty
                                         }).ToList();
            return Json(getTransferOutClass, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getFromStoreTR(string nofaktur)
        {
            GetTransferOutClass getTransferOutClass = new GetTransferOutClass();
            getTransferOutClass.carts = new List<ViewCartClass>();
            var tsodetail = (from m in db.tblthMutasiStokTRs.Where(p => p.NoFaktur == nofaktur)
                             from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                             from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                             group new { m, l1, l2 } by new
                             {
                                 m.NoFaktur,
                                 m.tgl,
                                 m.keterangan,
                                 kodefrom = l1.kodelokasi,
                                 namafrom = l1.nama,
                                 kodeto = l2.kodelokasi,
                                 namato = l2.nama,
                                 m.tgljatuhtempo
                             }
                           into g
                             select new
                             {
                                 g.Key.NoFaktur,
                                 g.Key.tgl,
                                 g.Key.keterangan,
                                 g.Key.kodefrom,
                                 g.Key.namafrom,
                                 g.Key.kodeto,
                                 g.Key.namato,
                                 g.Key.tgljatuhtempo
                             }).FirstOrDefault();
            if (tsodetail != null)
            {
                var checklokasi = db.tblLokasis.Where(p => p.InTransit == tsodetail.kodeto).FirstOrDefault();
                if (checklokasi != null)
                {
                    getTransferOutClass.kodelokasi = checklokasi.kodelokasi;
                    getTransferOutClass.namalokasi = checklokasi.nama;
                    getTransferOutClass.kodelokasito = tsodetail.kodefrom;
                    getTransferOutClass.namalokasito = tsodetail.namafrom;
                    getTransferOutClass.keterangan = tsodetail.keterangan;
                    getTransferOutClass.requireddate = tsodetail.tgljatuhtempo.ToString("dd-MMM-yyyy");
                }
            }
            string kodefrom = Session["kode_lokasi"].ToString();
            getTransferOutClass.carts = new List<ViewCartClass>();
            var itemdb = (from td in db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur)
                          from i in db.tblHBarangs.Where(p => p.kodebarang == td.kodebarang)
                          group new { td, i } by new
                          {
                              i.kodebarang,
                              i.nama,
                              td.qty,
                              td.kirim,
                              i.itemcode
                          }
                                         into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              itemqty = (float)g.Key.qty - (float)g.Key.kirim,
                              itemrealqty = (float)g.Key.qty,
                              g.Key.itemcode,
                              stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodefrom).FirstOrDefault()
                          }).ToList();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ViewCartClass
                {
                    itemcode = itemdbs.kodebarang,
                    itemname = itemdbs.nama,
                    itemqty = itemdbs.itemqty,
                    itemrealqty = itemdbs.itemrealqty,
                    barcode = itemdbs.itemcode,
                    itemstok = (float)(itemdbs.stocks != null ? itemdbs.stocks.stokmasuk - itemdbs.stocks.stokkeluar : 0)
                };
                getTransferOutClass.carts.Add(soloitem);
            }
            return Json(getTransferOutClass, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TransferOut(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            string kodefrom = Session["kode_lokasi"].ToString();
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            DateTime dtnow = DateTime.Now.AddDays(-1);
            var itemdb = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains("TSO") && p.kodelokasi == kodefrom && p.tgl >= dtnow)
                          from n in db.tblthMutasiStokTRs.Where(p => p.NoFaktur == m.refftr).DefaultIfEmpty()
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          from l3 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1, l2, n } by new
                          {
                              m.NoFaktur,
                              m.modidate,
                              m.tgl,
                              m.keterangan,
                              m.refftr,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              m.nourut,
                              l3.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.modidate,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.refftr,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.nourut,
                              g.Key.musernama,
                              checklokasi = db.tblLokasis.Where(p => p.InTransit == g.Key.kodeto).FirstOrDefault(),
                              checkdetailmutasi = db.tbltdMutasiStoks.Where(p => p.NoFaktur == g.Key.NoFaktur).ToList()
                          }).ToList().OrderByDescending(p => p.nourut);
            foreach (var itemdbs in itemdb)
            {
                string kodeto = itemdbs.kodeto;
                string namato = itemdbs.namato;
                string nofaktur = itemdbs.NoFaktur;
                if (itemdbs.checklokasi != null)
                {
                    kodeto = itemdbs.checklokasi.kodelokasi;
                    namato = itemdbs.checklokasi.nama;
                }
                int countdetailmutasi = 0;
                for (int j = 0; j < itemdbs.checkdetailmutasi.Count; j++)
                {
                    if (itemdbs.checkdetailmutasi[j].qty == itemdbs.checkdetailmutasi[j].kirim)
                    {
                        countdetailmutasi++;
                    }
                }
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    totalitemdetail = 0,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    modidate = Convert.ToDateTime(itemdbs.modidate).ToString("dd-MMM-yyyy HH:mm"),
                    keterangan = itemdbs.keterangan,
                    nofakturtr = (itemdbs.refftr.Equals("0") || itemdbs.refftr.Equals("")) ? "-" :
                    itemdbs.refftr,
                    kodeawal = itemdbs.kodefrom,
                    namaawal = itemdbs.namafrom,
                    kodeakhir = kodeto,
                    namaakhir = namato,
                    nourut = itemdbs.nourut,
                    usernama = itemdbs.musernama,
                    status = itemdbs.checkdetailmutasi.Count == countdetailmutasi ? "Close" : "Open"
                };
                //if (soloitem.status.Equals("Open"))
                //{
                chooseitems.Add(soloitem);
                //}
            }
            return View(chooseitems);
        }

        public ActionResult TransferIn(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            DateTime dtnow = DateTime.Now.AddDays(-7);
            var itemdb = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur.Contains("TSI") && p.tujuan == kodefrom && p.tgl >= dtnow)
                          from n in db.tblthMutasiStoks.Where(p => p.NoFaktur == m.refftr).DefaultIfEmpty()
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          from l3 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1, l2 } by new
                          {
                              m.NoFaktur,
                              m.modidate,
                              //m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              //tgltr = n.tgl,
                              m.nourut,
                              m.refftr,
                              l3.musernama
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.modidate,
                              //g.Key.tgl,
                              //g.Key.tgltr,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.nourut,
                              g.Key.refftr,
                              g.Key.musernama
                          }).ToList().OrderByDescending(p => p.nourut);
            foreach (var itemdbs in itemdb)
            {
                string solokodefrom = itemdbs.kodefrom;
                string solonamafrom = itemdbs.namafrom;
                string nofaktur = itemdbs.NoFaktur;
                var totalitemdetail = db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur).DefaultIfEmpty().Sum(p => (decimal?)p.qty);
                if (!itemdbs.refftr.Equals(""))
                {
                    string refftr = itemdbs.refftr;
                    var checktsout = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur.Equals(refftr))
                                      from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                                      group new { m, l1 } by new
                                      {
                                          l1.kodelokasi,
                                          l1.nama
                                      }
                                    into g
                                      select new
                                      {
                                          g.Key.kodelokasi,
                                          g.Key.nama
                                      }).FirstOrDefault();
                    if (checktsout != null)
                    {
                        solokodefrom = checktsout.kodelokasi;
                        solonamafrom = checktsout.nama;
                    }
                    string tgl = "-";
                    if (!itemdbs.refftr.Equals("0") && !itemdbs.refftr.Equals(""))
                    {
                        var checktgltr = db.tblthMutasiStoks.Where(p => p.NoFaktur == itemdbs.refftr).FirstOrDefault();
                        if (checktgltr != null)
                        {
                            tgl = Convert.ToDateTime(checktgltr.tgl).ToString("dd-MMM-yyyy");
                        }
                    }
                }
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    totalitemdetail = (float?)totalitemdetail == null ? 0 : (float)totalitemdetail,
                    //tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    modidate = Convert.ToDateTime(itemdbs.modidate).ToString("dd-MMM-yyyy HH:mm"),
                    //tanggaltr = Convert.ToDateTime(itemdbs.tgltr).ToString("dd-MMM-yyyy"),
                    keterangan = itemdbs.keterangan,
                    nofakturtr = itemdbs.refftr,
                    kodeawal = solokodefrom,
                    namaawal = solonamafrom,
                    kodeakhir = itemdbs.kodeto,
                    namaakhir = itemdbs.namato,
                    nourut = itemdbs.nourut,
                    usernama = itemdbs.musernama
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult TransferRequest(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            List<TransferOutClass> chooseitems = new List<TransferOutClass>();
            string kodefrom = Session["kode_lokasi"].ToString();
            string kodetos = Session["kode_lokasi"].ToString();
            var checklokasis = db.tblLokasis.Where(p => p.kodelokasi == kodetos).FirstOrDefault();
            if (checklokasis != null)
            {
                kodetos = checklokasis.InTransit;
            }
            DateTime dtnow = DateTime.Now.AddDays(-7);
            var itemdb = (from m in db.tblthMutasiStokTRs.Where(p => p.NoFaktur.Contains("TSR") && p.tujuan == kodetos && p.tgl >= dtnow)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          from l3 in db.musers.Where(p => p.muserid == m.userid)
                          group new { m, l1, l2 } by new
                          {
                              m.NoFaktur,
                              m.modidate,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              m.nourut,
                              m.closed,
                              m.userid,
                              l3.musernama,
                          }
                          into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.modidate,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.nourut,
                              g.Key.closed,
                              g.Key.userid,
                              g.Key.musernama
                          }).OrderByDescending(p => p.nourut).ToList();
            foreach (var itemdbs in itemdb)
            {
                string kodeto = itemdbs.kodeto;
                string namato = itemdbs.namato;
                string nofaktur = itemdbs.NoFaktur;
                var checklokasi = db.tblLokasis.Where(p => p.InTransit == itemdbs.kodeto).FirstOrDefault();
                var totalitemdetail = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur).DefaultIfEmpty().Sum(p => (decimal?)p.qty);
                if (checklokasi != null)
                {
                    kodeto = checklokasi.kodelokasi;
                    namato = checklokasi.nama;
                }
                var checktbltdmutasistoktrclose = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur && p.closed == true).ToList().Count;
                var checktbltdmutasistoktrall = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur).ToList().Count;
                var soloitem = new TransferOutClass
                {
                    nofaktur = itemdbs.NoFaktur,
                    tanggal = Convert.ToDateTime(itemdbs.tgl).ToString("dd-MMM-yyyy"),
                    modidate = Convert.ToDateTime(itemdbs.modidate).ToString("dd-MMM-yyyy HH:mm"),
                    keterangan = itemdbs.keterangan,
                    kodeawal = itemdbs.kodefrom,
                    namaawal = itemdbs.namafrom,
                    kodeakhir = kodeto,
                    namaakhir = namato,
                    nourut = itemdbs.nourut,
                    status = checktbltdmutasistoktrclose == checktbltdmutasistoktrall ? "Close" : "Open",
                    userid = itemdbs.userid,
                    usernama = itemdbs.musernama,
                    totalitemdetail = (float?)totalitemdetail == null ? 0 : (float)totalitemdetail
                };
                chooseitems.Add(soloitem);
            }
            return View(chooseitems);
        }

        public ActionResult AddTransferRequest()
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            var checkfrom = db.tblLokasis.Where(p => p.kodelokasi.Equals(kodelokasi)).FirstOrDefault();
            transferOutDetail.fromstore = checkfrom.nama;
            transferOutDetail.kodefrom = kodelokasi;
            transferOutDetail.tostore = db.tblLokasis
                .Where(p => (!p.kodelokasi.Contains("000") && p.kodelokasi != "PUSAT") && p.initial.Length > 0 && 
                !p.kodelokasi.Equals(kodelokasi) && p.aktif == true).ToList();
            transferOutDetail.type = new List<ChooseTypeClass>();
            var typemutasi = db.tblIntegrasiMutasiStoks.ToList();
            for (int i = 0; i < typemutasi.Count; i++)
            {
                ChooseTypeClass chooseType = new ChooseTypeClass();
                chooseType.typename = typemutasi[i].kode;
                transferOutDetail.type.Add(chooseType);
            }
            transferOutDetail.lockdate = Session["lock_date"].ToString() == "True" ? "t" : "f";
            return View(transferOutDetail);
        }

        public ActionResult DetailsTransferRequest(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            var solotr = (from m in db.tblthMutasiStokTRs.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          group new { m, l1, l2 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              alamatfrom = l1.alamat,
                              alamatfrom2 = l1.alamat2,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              alamatto = l2.alamat,
                              alamatto2 = l2.alamat2,
                              m.tipeMutasiStokTR,
                              m.modidate,
                              m.userid
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.alamatfrom,
                              g.Key.alamatfrom2,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.alamatto,
                              g.Key.alamatto2,
                              g.Key.tipeMutasiStokTR,
                              g.Key.modidate,
                              g.Key.userid
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.fromstore = solotr.namafrom;
                string tostore = solotr.kodeto;
                var checklokasi = db.tblLokasis.Where(p => p.InTransit == tostore).FirstOrDefault();
                if (checklokasi != null)
                {
                    transferOutDetail.tostorename = checklokasi.nama;
                    transferOutDetail.kodeto = checklokasi.kodelokasi;
                    transferOutDetail.alamatto = checklokasi.alamat;
                    transferOutDetail.alamatto2 = checklokasi.alamat2;
                }
                transferOutDetail.kodefrom = solotr.kodefrom;
                transferOutDetail.alamatfrom = solotr.alamatfrom;
                transferOutDetail.alamatfrom2 = solotr.alamatfrom2;
                transferOutDetail.tipe = solotr.tipeMutasiStokTR;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.userid = solotr.userid;
                transferOutDetail.createtime = solotr.modidate.ToString("dd-MM-yyyy");
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var detailstr = (from k in db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur)
                                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                                 group new { e, k } by new
                                 {
                                     e.kodebarang,
                                     e.nama,
                                     k.qty,
                                     k.closed,
                                     k.kirim
                                 }
                                 into g
                                 select new
                                 {
                                     g.Key.kodebarang,
                                     g.Key.nama,
                                     g.Key.qty,
                                     g.Key.closed,
                                     g.Key.kirim
                                 }).ToList();
                float totalqty = 0;
                float totalqtykirim = 0;
                int checkclosed = 0;
                int checkpartial = 0;
                for (int i = 0; i < detailstr.Count; i++)
                {
                    ViewCartClass viewCartClass = new ViewCartClass();
                    viewCartClass.itemcode = detailstr[i].kodebarang;
                    viewCartClass.itemname = detailstr[i].nama;
                    viewCartClass.itemqty = (float)detailstr[i].qty;
                    viewCartClass.itemkirim = (float)detailstr[i].kirim;
                    totalqty = totalqty + (float)detailstr[i].qty;
                    totalqtykirim = totalqtykirim + (float)detailstr[i].kirim;
                    transferOutDetail.carts.Add(viewCartClass);
                    if (detailstr[i].closed == true) checkclosed = checkclosed + 1;
                }
                transferOutDetail.totalqty = totalqty;
                transferOutDetail.totalqtykirim = totalqtykirim;
                if (checkclosed == detailstr.Count)
                {
                    transferOutDetail.status = "Close";
                }
                else 
                {
                    transferOutDetail.status = "Open";
                }
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsTransferOut(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferOutDetailClass transferOutDetail = new TransferOutDetailClass();
            var solotr = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          group new { m, l1, l2 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              alamatfrom = l1.alamat,
                              alamatfrom2 = l1.alamat2,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              alamatto = l2.alamat,
                              alamatto2 = l2.alamat2,
                              m.tipemutasistok,
                              m.refftr,
                              m.nourut,
                              m.modidate,
                              m.userid
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.alamatfrom,
                              g.Key.alamatfrom2,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.alamatto,
                              g.Key.alamatto2,
                              g.Key.tipemutasistok,
                              g.Key.refftr,
                              g.Key.nourut,
                              g.Key.modidate,
                              g.Key.userid
                          }).FirstOrDefault();
            if (solotr != null)
            {
                transferOutDetail.fromstore = solotr.namafrom;
                string tostore = solotr.kodeto;
                var checklokasi = db.tblLokasis.Where(p => p.InTransit == tostore).FirstOrDefault();
                if (checklokasi != null)
                {
                    transferOutDetail.tostorename = checklokasi.nama;
                    transferOutDetail.alamatto = checklokasi.alamat;
                    transferOutDetail.alamatto2 = checklokasi.alamat2;
                }
                transferOutDetail.fromstore = solotr.namafrom;
                transferOutDetail.alamatfrom = solotr.alamatfrom;
                transferOutDetail.alamatfrom2 = solotr.alamatfrom2;
                transferOutDetail.tipe = solotr.tipemutasistok;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.userid = solotr.userid;
                transferOutDetail.createtime = solotr.modidate.ToString("dd-MMM-yyyy");
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.nofakturtr = (solotr.refftr.Equals("0") || solotr.refftr.Equals("")) ? "-" :
                    solotr.refftr;
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var check = db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur).ToList();
                float totalqty = 0;
                int totalitem = 0;
                for (int i = 0; i < check.Count; i++)
                {
                    string kodebarang = check[i].kodebarang;
                    var checkdetail = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).First();
                    if (checkdetail != null)
                    {
                        ViewCartClass viewCartClass = new ViewCartClass();
                        viewCartClass.itemcode = checkdetail.kodebarang;
                        viewCartClass.itemname = checkdetail.nama;
                        viewCartClass.itemqty = (float)check[i].qty;
                        totalqty = totalqty + (float)check[i].qty;
                        totalitem = totalitem + 1;
                        transferOutDetail.carts.Add(viewCartClass);
                    }
                }
                //var detailstr = (from k in db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                //                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                //                 group new { e, k } by new
                //                 {
                //                     e.kodebarang,
                //                     e.nama,
                //                     k.qty
                //                 }
                //                 into g
                //                 select new
                //                 {
                //                     g.Key.kodebarang,
                //                     g.Key.nama,
                //                     g.Key.qty
                //                 }).ToList();
                //int totalqty = 0;
                //for (int i = 0; i < detailstr.Count; i++)
                //{
                //    ViewCartClass viewCartClass = new ViewCartClass();
                //    viewCartClass.itemcode = detailstr[i].kodebarang;
                //    viewCartClass.itemname = detailstr[i].nama;
                //    viewCartClass.itemqty = (int)detailstr[i].qty;
                //    totalqty = totalqty + (int)detailstr[i].qty;
                //    transferOutDetail.carts.Add(viewCartClass);
                //}
                transferOutDetail.totalqty = totalqty;
            }
            return View(transferOutDetail);
        }

        public ActionResult DetailsTransferIn(string nofaktur)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            TransferInDetailClass transferOutDetail = new TransferInDetailClass();
            var solotr = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                          from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                          from l2 in db.tblLokasis.Where(p => p.kodelokasi == m.tujuan)
                          group new { m, l1, l2 } by new
                          {
                              m.NoFaktur,
                              m.tgl,
                              m.keterangan,
                              kodefrom = l1.kodelokasi,
                              namafrom = l1.nama,
                              alamatfrom = l1.alamat,
                              alamatfrom2 = l1.alamat2,
                              kodeto = l2.kodelokasi,
                              namato = l2.nama,
                              alamatto = l2.alamat,
                              alamatto2 = l2.alamat2,
                              m.tipemutasistok,
                              m.refftr,
                              m.nourut,
                              m.modidate,
                              m.userid
                          }
                           into g
                          select new
                          {
                              g.Key.NoFaktur,
                              g.Key.tgl,
                              g.Key.keterangan,
                              g.Key.kodefrom,
                              g.Key.namafrom,
                              g.Key.alamatfrom,
                              g.Key.alamatfrom2,
                              g.Key.kodeto,
                              g.Key.namato,
                              g.Key.alamatto,
                              g.Key.alamatto2,
                              g.Key.tipemutasistok,
                              g.Key.refftr,
                              g.Key.nourut,
                              g.Key.modidate,
                              g.Key.userid
                          }).FirstOrDefault();
            if (solotr != null)
            {
                string solokodefrom = solotr.kodefrom;
                string solonamafrom = solotr.namafrom;
                string soloalamatfrom = solotr.alamatfrom;
                string soloalamatfrom2 = solotr.alamatfrom2;
                if (!solotr.refftr.Equals("") && !solotr.refftr.Equals("0"))
                {
                    string refftr = solotr.refftr;
                    var checktsout = (from m in db.tblthMutasiStoks.Where(p => p.NoFaktur.Equals(refftr))
                                      from l1 in db.tblLokasis.Where(p => p.kodelokasi == m.kodelokasi)
                                      group new { m, l1 } by new
                                      {
                                          l1.kodelokasi,
                                          l1.nama,
                                          l1.alamat,
                                          l1.alamat2
                                      }
                                    into g
                                      select new
                                      {
                                          g.Key.kodelokasi,
                                          g.Key.nama,
                                          g.Key.alamat,
                                          g.Key.alamat2
                                      }).FirstOrDefault();
                    if (checktsout != null)
                    {
                        solokodefrom = checktsout.kodelokasi;
                        solonamafrom = checktsout.nama;
                        soloalamatfrom = checktsout.alamat;
                        soloalamatfrom2 = checktsout.alamat2;
                    }
                }
                transferOutDetail.fromstore = solonamafrom;
                transferOutDetail.alamatfrom = soloalamatfrom;
                transferOutDetail.alamatfrom2 = soloalamatfrom2;
                transferOutDetail.tostorename = solotr.namato;
                transferOutDetail.alamatto = solotr.alamatto;
                transferOutDetail.alamatto2 = solotr.alamatto2;
                transferOutDetail.tipe = solotr.tipemutasistok;
                transferOutDetail.comment = solotr.keterangan;
                transferOutDetail.userid = solotr.userid;
                transferOutDetail.createtime = solotr.modidate.ToString("dd-MMM-yyyy");
                transferOutDetail.nofaktur = solotr.NoFaktur;
                transferOutDetail.nofakturtr = (solotr.refftr.Equals("0") || solotr.refftr.Equals("")) ? "-" :
                    solotr.refftr;
                if (transferOutDetail.nofakturtr != "-")
                {
                    var checktr = db.tblthMutasiStoks.Where(p => p.NoFaktur == transferOutDetail.nofakturtr).FirstOrDefault();
                    if (checktr != null)
                    {
                        transferOutDetail.nofakturtrreff = (checktr.refftr.Equals("0") || checktr.refftr.Equals("")) ? "-" : checktr.refftr;
                    }
                    else
                    {
                        transferOutDetail.nofakturtrreff = "-";
                    }
                }
                else
                {
                    transferOutDetail.nofakturtrreff = "-";
                }
                transferOutDetail.tanggal = solotr.tgl.ToString("dd-MMM-yyyy");
                transferOutDetail.carts = new List<ViewCartClass>();
                var check = db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur).ToList();
                int totalqty = 0;
                int totalitem = 0;
                for (int i = 0; i < check.Count; i++)
                {
                    string kodebarang = check[i].kodebarang;
                    var checkdetail = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).First();
                    if (checkdetail != null)
                    {
                        ViewCartClass viewCartClass = new ViewCartClass();
                        viewCartClass.itemcode = checkdetail.kodebarang;
                        viewCartClass.itemname = checkdetail.nama;
                        viewCartClass.itemqty = (int)check[i].qty;
                        totalqty = totalqty + (int)check[i].qty;
                        totalitem = totalitem + 1;
                        transferOutDetail.carts.Add(viewCartClass);
                    }
                }
                //var detailstr = (from k in db.tbltdMutasiStoks.Where(p => p.NoFaktur == nofaktur)
                //                 from e in db.tblHBarangs.Where(p => p.kodebarang == k.kodebarang)
                //                 group new { } by new
                //                 {
                //                     e.kodebarang,
                //                     e.nama,
                //                     k.qty
                //                 }
                //                 into g
                //                 select new
                //                 {
                //                     g.Key.kodebarang,
                //                     g.Key.nama,
                //                     g.Key.qty
                //                 }).ToList();
                //for (int i = 0; i < detailstr.Count; i++)
                //{
                //    ViewCartClass viewCartClass = new ViewCartClass();
                //    viewCartClass.itemcode = detailstr[i].kodebarang;
                //    viewCartClass.itemname = detailstr[i].nama;
                //    viewCartClass.itemqty = (int)detailstr[i].qty;
                //    totalqty = totalqty + (int)detailstr[i].qty;
                //    totalitem = totalitem + 1;
                //    transferOutDetail.carts.Add(viewCartClass);
                //}
                transferOutDetail.totalqty = totalqty;
                transferOutDetail.totalitem = totalitem;
            }
            return View(transferOutDetail);
        }

        public ActionResult SubmitTransferRequest(string tanggal, string keterangan, string tostore, string type, string reqdate,
            string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            var dt = DateTime.ParseExact(tanggal, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var reqdt = DateTime.ParseExact(reqdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dt = DateTime.Now;
            string kodefrom = Session["kode_lokasi"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "TSR/" + initialfrom + "/";
            var checkpenjualan = db.tblthMutasiStokTRs
                .Where(p => p.NoFaktur.Contains(nofaktur)).OrderByDescending(p => p.NoFaktur).ToList();
            //.Where(p => p.kodelokasi == tostore).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int64.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            string transittostore = tostore;
            if (checklokasi != null)
            {
                transittostore = checklokasi.InTransit;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_SimpanHeaderMutasiStokTR(nofaktur, dt, keterangan, Session["user_id"].ToString(), "1",
                    tostore, transittostore, type, reqdt);
                    string[] splitcart = cart.Split(';');
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        db.sp_SimpanDetailMutasiStokTR(nofaktur, splitrowcart[0], (decimal)float.Parse(splitrowcart[1]), "",
                            dt, kodefrom);
                    }
                    //var userdb = db.musers.Where(p => p.kodelokasi == param.kodelokasi && p.tokenFB != null).ToList();
                    //for (int i = 0; i < userdb.Count; i++)
                    //{
                    //    SendNotifOutput notifresult = sendNotif("Inventory", "Transfer Out Had Succeed", userdb[i].tokenFB, param.transferoutdetail, param.mode);
                    //}
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Transaction Request " + nofaktur + " Has Been Created..";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To Create Transfer Request";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            //SendNotifOutput notifresult = sendNotif("Transfer In", "Stock Baru Telah Diterima", "e1k2jTDgxFE:APA91bHqtRZxkOpHWEW4IqYy93FG5Rf9-v61nh_kBTJX8UdLBuAwfeYRz3-vmjb0yLJfd5l44rD69-Di_-hwdgo27qwhjxxTwsNrNHLmCk8Iuja8M8zV7FgZEkL-8U2koCkqoDxg4Jmf",
            //                cart, 2);
            //var obj = new
            //{
            //    valid = true,
            //    pesansingkat = "ABCD"
            //};
            return Json(obj);
        }

        public ActionResult UpdateTransferRequest(string tanggal, string nofaktur, string keterangan, string tostore, string type, string reqdate, 
            string cart)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            string messages = "";
            string oldnofaktur = nofaktur;
            var dt = DateTime.ParseExact(tanggal, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            var reqdt = DateTime.ParseExact(reqdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            string kodefrom = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            string initialfrom = Session["initial_lokasi"].ToString();
            nofaktur = "TSR/" + initialfrom + "/";
            var checkpenjualan = db.tblthMutasiStokTRs
                .Where(p => p.NoFaktur.Contains(nofaktur)).OrderByDescending(p => p.nourut).ToList();
            if (checkpenjualan.Count() != 0)
            {
                if (checkpenjualan.First().NoFaktur.StartsWith(nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd")))
                {
                    string[] splitfaktur = checkpenjualan.First().NoFaktur.Split('/');
                    nofaktur = nofaktur + (Int32.Parse(splitfaktur[2]) + 1);
                }
                else
                {
                    nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
                }
            }
            else
            {
                nofaktur = nofaktur + dt.ToString("yy") + dt.ToString("MM") + dt.ToString("dd") + "0001";
            }
            var checklokasi = db.tblLokasis.Where(p => p.kodelokasi == kodefrom).FirstOrDefault();
            string transittostore = tostore;
            if (checklokasi != null)
            {
                transittostore = checklokasi.InTransit;
            }
            var txOptions = new System.Transactions.TransactionOptions();
            txOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions()
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(30)
            }))
            {
                try
                {
                    db.sp_ReturnOldPosMutasiStokTR(oldnofaktur, userid, "1");
                    db.sp_SimpanHeaderMutasiStokTR(oldnofaktur, dt, keterangan, Session["user_id"].ToString(), "1",
                        tostore, transittostore, type, reqdt);
                    string[] splitcart = cart.Split(';');
                    for (int i = 0; i < splitcart.Length; i++)
                    {
                        string[] splitrowcart = splitcart[i].Split('$');
                        db.sp_SimpanDetailMutasiStokTR(oldnofaktur, splitrowcart[0], (decimal)Int32.Parse(splitrowcart[1]), "",
                            dt, kodefrom);
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Transaction Request " + oldnofaktur + " Has Been Updated..";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To Update Transfer Request";
                    transaction.Dispose();
                }
               
            }
            return RedirectToAction("TransferRequest", new { message = messages });
        }

        public ActionResult ViewCart(string code)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login");
            }
            string[] peritem = code.Split(';');
            List<ViewCartClass> viewcarts = new List<ViewCartClass>();
            int index = 1;
            for (int i = 0; i < peritem.Length; i++)
            {
                string[] codeitem = peritem[i].Split('$');
                string codebarang = codeitem[0];
                var checkcart = db.tblHBarangs.Where(p => p.kodebarang == codebarang).FirstOrDefault();
                if (checkcart != null)
                {
                    ViewCartClass viewcart = new ViewCartClass();
                    viewcart.itemno = index;
                    viewcart.itemcode = checkcart.kodebarang;
                    viewcart.itemname = checkcart.nama;
                    viewcart.itemqty = Int32.Parse(codeitem[1]);
                    viewcarts.Add(viewcart);
                    index++;
                }

            }
            return View(viewcarts);
        }

        public List<ChooseItemClass> getItemFromStore()
        {
            string kodelok = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen)
                          from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                          (p.stokmasuk - p.stokkeluar > 0))
                          group new { e, u, k } by new
                          {
                              u.stokkeluar,
                              u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.stokkeluar,
                              g.Key.stokmasuk
                          }).ToList().OrderBy(p => p.nama);
            int i = 0;
            int count = itemdb.Count();
            try
            {
                foreach (var itemdbs in itemdb)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        kodeprodusen = itemdbs.kodeprodusen,
                        namaprodusen = itemdbs.namaprodusen,
                        stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar
                    };
                    journallist.Add(soloitem);
                    i++;
                };
            }
            catch (Exception e)
            {
                string message = e.Message;
            }
            
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            //return Json(journallist, JsonRequestBehavior.AllowGet);
            return journallist;
        }

        public JsonResult getItemFromStoreTSO()
        {
            string kodelok = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen && p.aktif == true)
                          from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                          (p.stokmasuk - p.stokkeluar > 0))
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelok && p.kodekategori == k.kodekategori)
                          group new { e, u, k } by new
                          {
                              u.stokkeluar,
                              u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.stokkeluar,
                              g.Key.stokmasuk,
                              g.Key.itemcode
                          }).ToList().OrderBy(p => p.kodebarang);
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    kodeprodusen = itemdbs.kodeprodusen,
                    namaprodusen = itemdbs.namaprodusen,
                    stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromStoreTSOWithStore(string kodelokasi)
        {
            string kodelok = Session["kode_lokasi"].ToString();
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen && p.aktif == true)
                          from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                          (p.stokmasuk - p.stokkeluar > 0))
                          from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelok && p.kodekategori == k.kodekategori)
                          group new { e, u, k } by new
                          {
                              u.stokkeluar,
                              u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.stokkeluar,
                              g.Key.stokmasuk,
                              g.Key.itemcode,
                              checkkategory = db.tblLokasiKategoris.Where(p => p.kodekategori == g.Key.kodekategori && p.kodelokasi == kodelokasi).FirstOrDefault(),
                              checkprodusen = db.tblLokasiProdusens.Where(p => p.kodeprodusen == g.Key.kodeprodusen && p.kodelokasi == kodelokasi).FirstOrDefault()
                          }).ToList().OrderBy(p => p.kodebarang);
            int count = itemdb.Count();
            if (count > 0)
            {
                foreach (var itemdbs in itemdb)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        kodeprodusen = itemdbs.kodeprodusen,
                        namaprodusen = itemdbs.namaprodusen,
                        stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                        itemcode = itemdbs.itemcode
                    };
                    if (itemdbs.checkkategory != null && itemdbs.checkprodusen != null)
                    {
                        journallist.Add(soloitem);
                    }
                };
            }
            else
            {
                var itemdball = (from k in db.tblKategoris
                              from pr in db.tblProdusens
                              from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                              p.kodeprodusen == pr.kodeprodusen && p.aktif == true)
                              from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                              (p.stokmasuk - p.stokkeluar > 0))
                              group new { e, u, k } by new
                              {
                                  u.stokkeluar,
                                  u.stokmasuk,
                                  e.kodebarang,
                                  e.nama,
                                  k.kodekategori,
                                  namakategori = k.nama,
                                  pr.kodeprodusen,
                                  namaprodusen = pr.nama,
                                  e.itemcode
                              }
                      into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  g.Key.kodeprodusen,
                                  g.Key.namaprodusen,
                                  g.Key.stokkeluar,
                                  g.Key.stokmasuk,
                                  g.Key.itemcode
                              }).ToList().OrderBy(p => p.kodebarang);
                foreach (var itemdbs in itemdball)
                {
                    var soloitem = new ChooseItemClass
                    {
                        kodebarang = itemdbs.kodebarang,
                        namabarang = itemdbs.nama,
                        kodekategori = itemdbs.kodekategori,
                        namakategori = itemdbs.namakategori,
                        kodeprodusen = itemdbs.kodeprodusen,
                        namaprodusen = itemdbs.namaprodusen,
                        stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                        itemcode = itemdbs.itemcode
                    };
                    journallist.Add(soloitem);
                };
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public JsonResult getItemFromStoreTR(string kodelokasi)
        {
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            string kodelok = Session["kode_lokasi"].ToString();
            if (Session["tr_cek_stok"].ToString().Equals("True"))
            {
                var itemdb = (from e in db.tblHBarangs.Where(p => p.aktif == true)
                              from k in db.tblKategoris.Where(p => p.kodekategori == e.kodekategori)
                              from pr in db.tblProdusens.Where(p => p.kodeprodusen == e.kodeprodusen)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier).DefaultIfEmpty()
                              from ls in db.tblLokasiKategoris.Where(p => (p.kodelokasi == kodelokasi && p.kodekategori == e.kodekategori))
                              from lp in db.tblLokasiProdusens.Where(p => (p.kodelokasi == kodelokasi && p.kodeprodusen == e.kodeprodusen))
                              group new { e, k } by new
                              {
                                  e.kodebarang,
                                  e.nama,
                                  k.kodekategori,
                                  namakategori = k.nama,
                                  pr.kodeprodusen,
                                  namaprodusen = pr.nama,
                                  e.itemcode
                              }
                       into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  g.Key.kodeprodusen,
                                  g.Key.namaprodusen,
                                  g.Key.itemcode,
                                  checkkategory = db.tblLokasiKategoris.Where(p => p.kodekategori == g.Key.kodekategori && p.kodelokasi == kodelok).FirstOrDefault(),
                                  checkprodusen = db.tblLokasiProdusens.Where(p => p.kodeprodusen == g.Key.kodeprodusen && p.kodelokasi == kodelok).FirstOrDefault(),
                                  stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                              }).ToList().OrderBy(p => p.kodebarang);
                int i = 0;
                int count = itemdb.Count();
                if (count > 0)
                {
                    foreach (var itemdbs in itemdb)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodeprodusen,
                            namaprodusen = itemdbs.namaprodusen,
                            stock = itemdbs.stocks != null ? (int)(itemdbs.stocks.stokmasuk - itemdbs.stocks.stokkeluar) : 0,
                            itemcode = itemdbs.itemcode
                        };
                        if (itemdbs.stocks != null)
                        {
                            if (itemdbs.checkkategory != null && itemdbs.checkprodusen != null)
                            {
                                journallist.Add(soloitem);
                                i++;
                            }
                        }
                    };
                }
                else
                {
                    var itemdball = (from k in db.tblKategoris
                                     from pr in db.tblProdusens
                                     from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                                     p.kodeprodusen == pr.kodeprodusen && p.aktif == true)
                                     from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                                     (p.stokmasuk - p.stokkeluar > 0))
                                     group new { e, u, k } by new
                                     {
                                         u.stokkeluar,
                                         u.stokmasuk,
                                         e.kodebarang,
                                         e.nama,
                                         k.kodekategori,
                                         namakategori = k.nama,
                                         pr.kodeprodusen,
                                         namaprodusen = pr.nama,
                                         e.itemcode
                                     }
                          into g
                                     select new
                                     {
                                         g.Key.kodebarang,
                                         g.Key.nama,
                                         g.Key.kodekategori,
                                         g.Key.namakategori,
                                         g.Key.kodeprodusen,
                                         g.Key.namaprodusen,
                                         g.Key.stokkeluar,
                                         g.Key.stokmasuk,
                                         g.Key.itemcode
                                     }).ToList().OrderBy(p => p.kodebarang);
                    foreach (var itemdbs in itemdball)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodeprodusen,
                            namaprodusen = itemdbs.namaprodusen,
                            stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                            itemcode = itemdbs.itemcode
                        };
                        journallist.Add(soloitem);
                    };
                }
            }
            else
            {
                var itemdb = (from e in db.tblHBarangs.Where(p => p.aktif == true)
                              from k in db.tblKategoris.Where(p => p.kodekategori == e.kodekategori)
                              from pr in db.tblProdusens.Where(p => p.kodeprodusen == e.kodeprodusen)
                              from s in db.tblSuppliers.Where(p => p.kodesupplier == e.kodesupplier).DefaultIfEmpty()
                              from ls in db.tblLokasiKategoris.Where(p => p.kodelokasi == kodelokasi && p.kodekategori == e.kodekategori)
                              from lp in db.tblLokasiProdusens.Where(p => p.kodelokasi == kodelokasi && p.kodeprodusen == e.kodeprodusen)
                              group new { e, k } by new
                              {
                                  e.kodebarang,
                                  e.nama,
                                  k.kodekategori,
                                  namakategori = k.nama,
                                  pr.kodeprodusen,
                                  namaprodusen = pr.nama,
                                  e.itemcode
                              }
                       into g
                              select new
                              {
                                  g.Key.kodebarang,
                                  g.Key.nama,
                                  g.Key.kodekategori,
                                  g.Key.namakategori,
                                  g.Key.kodeprodusen,
                                  g.Key.namaprodusen,
                                  g.Key.itemcode,
                                  checkkategory = db.tblLokasiKategoris.Where(p => p.kodekategori == g.Key.kodekategori && p.kodelokasi == kodelok).FirstOrDefault(),
                                  checkprodusen = db.tblLokasiProdusens.Where(p => p.kodeprodusen == g.Key.kodeprodusen && p.kodelokasi == kodelok).FirstOrDefault(),
                                  stocks = db.tblStoks.Where(p => p.kodebarang == g.Key.kodebarang && p.kodelokasi == kodelokasi).FirstOrDefault()
                              }).ToList().OrderBy(p => p.kodebarang);
                int i = 0;
                int count = itemdb.Count();
                if (count > 0)
                {
                    foreach (var itemdbs in itemdb)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodeprodusen,
                            namaprodusen = itemdbs.namaprodusen,
                            stock = itemdbs.stocks != null ? (int)(itemdbs.stocks.stokmasuk - itemdbs.stocks.stokkeluar) : 0,
                            itemcode = itemdbs.itemcode
                        };
                        if (itemdbs.checkkategory != null && itemdbs.checkprodusen != null)
                        {
                            journallist.Add(soloitem);
                            i++;
                        }
                    };
                }
                else
                {
                    var itemdball = (from k in db.tblKategoris
                                     from pr in db.tblProdusens
                                     from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                                     p.kodeprodusen == pr.kodeprodusen && p.aktif == true)
                                     from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelok &&
                                     (p.stokmasuk - p.stokkeluar > 0))
                                     group new { e, u, k } by new
                                     {
                                         u.stokkeluar,
                                         u.stokmasuk,
                                         e.kodebarang,
                                         e.nama,
                                         k.kodekategori,
                                         namakategori = k.nama,
                                         pr.kodeprodusen,
                                         namaprodusen = pr.nama,
                                         e.itemcode
                                     }
                          into g
                                     select new
                                     {
                                         g.Key.kodebarang,
                                         g.Key.nama,
                                         g.Key.kodekategori,
                                         g.Key.namakategori,
                                         g.Key.kodeprodusen,
                                         g.Key.namaprodusen,
                                         g.Key.stokkeluar,
                                         g.Key.stokmasuk,
                                         g.Key.itemcode
                                     }).ToList().OrderBy(p => p.kodebarang);
                    foreach (var itemdbs in itemdball)
                    {
                        var soloitem = new ChooseItemClass
                        {
                            kodebarang = itemdbs.kodebarang,
                            namabarang = itemdbs.nama,
                            kodekategori = itemdbs.kodekategori,
                            namakategori = itemdbs.namakategori,
                            kodeprodusen = itemdbs.kodeprodusen,
                            namaprodusen = itemdbs.namaprodusen,
                            stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                            itemcode = itemdbs.itemcode
                        };
                        journallist.Add(soloitem);
                    };
                }
            }
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public JsonResult getTRDetailFromTR(string tr)
        {
            var checkdetailtr = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur.Equals(tr)).ToList();
            ChoosePOClass solopo = new ChoosePOClass();
            solopo.nopo = tr;
            solopo.itempo = checkdetailtr.Count();
            solopo.details = new List<GoodReceiptPOItemDetailClass>();
            foreach (var itemdbs in checkdetailtr)
            {
                string kodebarang = itemdbs.kodebarang;
                var checkitem = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).FirstOrDefault();
                if (checkitem != null)
                {
                    var soloitem = new GoodReceiptPOItemDetailClass
                    {
                        kodebarang = checkitem.kodebarang,
                        namabarang = checkitem.nama,
                        order = (int)itemdbs.qty,
                        satuan = "-"
                    };
                    solopo.details.Add(soloitem);
                }
            };
            return Json(solopo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getTRDetailFromTRList(string trlist)
        {
            List<GoodReceiptPOItemDetailClass> podetails = new List<GoodReceiptPOItemDetailClass>();

            string[] solopo = trlist.Split(';');
            for (int i = 0; i < solopo.Length; i++)
            {
                string nofaktur = solopo[i];
                var checkpo = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur.Equals(nofaktur)).ToList();
                GoodReceiptPOItemDetailClass podetail = new GoodReceiptPOItemDetailClass();
                foreach (var itemdbs in checkpo)
                {
                    string kodebarang = itemdbs.kodebarang;
                    var checkitem = db.tblHBarangs.Where(p => p.kodebarang == kodebarang).FirstOrDefault();
                    if (checkitem != null)
                    {
                        var soloitem = new GoodReceiptPOItemDetailClass
                        {
                            kodebarang = checkitem.kodebarang,
                            namabarang = checkitem.nama,
                            order = (int)itemdbs.qty,
                            satuan = "-"
                        };
                        podetails.Add(soloitem);
                    }
                };
            }
            return Json(podetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getItemStoreFromBarcode(string barcode, string kodelokasi)
        {
            List<ChooseItemClass> journallist = new List<ChooseItemClass>();
            var itemdb = (from k in db.tblKategoris
                          from pr in db.tblProdusens
                          from e in db.tblHBarangs.Where(p => p.kodekategori == k.kodekategori &&
                          p.kodeprodusen == pr.kodeprodusen && p.aktif == true && p.itemcode == barcode)
                          from u in db.tblStoks.Where(p => p.kodebarang == e.kodebarang && p.kodelokasi == kodelokasi)
                          group new { e, u, k } by new
                          {
                              u.stokkeluar,
                              u.stokmasuk,
                              e.kodebarang,
                              e.nama,
                              k.kodekategori,
                              namakategori = k.nama,
                              pr.kodeprodusen,
                              namaprodusen = pr.nama,
                              e.hargajual,
                              e.satuan,
                              e.itemcode
                          }
                       into g
                          select new
                          {
                              g.Key.kodebarang,
                              g.Key.nama,
                              g.Key.kodekategori,
                              g.Key.namakategori,
                              g.Key.kodeprodusen,
                              g.Key.namaprodusen,
                              g.Key.stokkeluar,
                              g.Key.stokmasuk,
                              g.Key.hargajual,
                              g.Key.satuan,
                              g.Key.itemcode
                          }).OrderBy(p => p.nama).ToList();
            int i = 0;
            int count = itemdb.Count();
            foreach (var itemdbs in itemdb)
            {
                var soloitem = new ChooseItemClass
                {
                    kodebarang = itemdbs.kodebarang,
                    namabarang = itemdbs.nama,
                    kodekategori = itemdbs.kodekategori,
                    namakategori = itemdbs.namakategori,
                    kodeprodusen = itemdbs.kodeprodusen,
                    namaprodusen = itemdbs.namaprodusen,
                    stock = (int)itemdbs.stokmasuk - (int)itemdbs.stokkeluar,
                    harga = (double)itemdbs.hargajual,
                    strharga = Convert.ToDecimal((double)itemdbs.hargajual).ToString("#,###"),
                    satuan = itemdbs.satuan,
                    itemcode = itemdbs.itemcode
                };
                journallist.Add(soloitem);
                i++;
            };
            //return new JsonResult() { Data = journallist, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            var jsonResult = Json(journallist, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
            //return journallist;
        }

        public ActionResult CloseManualTranferRequest(string nofaktur)
        {
            try
            {
                var checktr = db.tblthMutasiStokTRs.Where(p => p.NoFaktur == nofaktur).FirstOrDefault();
                if (checktr != null)
                {
                    checktr.closed = true;
                    var checktrdetail = db.tbltdMutasiStokTRs.Where(p => p.NoFaktur == nofaktur).ToList();
                    for (int i = 0; i < checktrdetail.Count; i++)
                    {
                        checktrdetail[i].closed = true;
                    }
                    db.SaveChanges();
                    return RedirectToAction("TransferRequest", new { message = "Close TR " + nofaktur + " Success.." });
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return RedirectToAction("TransferRequest", new { message = "Close TR " + nofaktur + " Failed.." });
        }

        public SendNotifOutput sendNotif(string title, string body, string to, string transferoutdetail, string param)
        {
            SendNotifOutput output = new SendNotifOutput();
            string key = "key=AAAAuO0dp7c:APA91bGDHrWdZUzjHuvlNwh_GJDHJdJppO_lQCVPe7mPFjU80mXJfJxVMq8QGZiJseTUL1P7Ri3hJh-KupQ7NwnWMi6CS0MF1K0xqb4o9IKMawauDxAggM683nU8hMirbyk0osoG_Sxj";
            try
            {
                string URL = "https://fcm.googleapis.com/fcm/send";
                StringContent content = new StringContent("");
                var datanotif = new
                {
                    title = title,
                    body = body,
                    detail = transferoutdetail,
                    flag = param,
                    notif = 1
                };
                var paramnotif = new
                {
                    to = to,
                    data = datanotif
                };
                var stringPayload = JsonConvert.SerializeObject(paramnotif);
                content = new StringContent(stringPayload, Encoding.UTF8, "application/json");
                var client = new HttpClient { };
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", key);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsync(URL, content).Result;
                string StatusCode = response.StatusCode.ToString();
                if (response.IsSuccessStatusCode)
                {
                    output.result = "OK";
                    output.transferoutdetail = transferoutdetail;
                    output.message = "Success";
                }
                else
                {
                    output.result = "NG";
                    output.transferoutdetail = "";
                    output.message = "Fail";
                }
            }
            catch (Exception ex)
            {
                output.result = "NG";
                output.message = ex.ToString();
            }
            return output;
        }

        [HttpPost] // can be HttpGet
        public ActionResult Test(string id, string tostore)
        {
            return RedirectToAction("TransferRequest", new { message = "TEST" });
        }
    }
}