﻿using POSVITWeb.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace POSVITWeb.Controllers
{
    public class SettlementController : Controller
    {
        POSVIT_JTEntities db = new POSVIT_JTEntities();

        public ActionResult EndofShift(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            EndofShiftClass eos = new EndofShiftClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            var checklasteos = db.tblSettlementEndofShifts.Where(p => p.userid == userid).OrderByDescending(p => p.tgl).FirstOrDefault();
            if (checklasteos != null)
            {
                eos.fromdate = checklasteos.tgl.ToString("dd-MM-yyyy HH.mm.ss");
            }
            else
            {
                var checkeosloc = db.tblSaldoAwalShifts.Where(p => p.userid == userid).OrderByDescending(p => p.startshift).FirstOrDefault();
                if (checkeosloc != null)
                {
                    eos.fromdate = checkeosloc.startshift.ToString("dd-MM-yyyy HH.mm.ss");
                }
                else
                {
                    eos.fromdate = DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss");
                }
            }
            eos.todate = DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss");
            eos.todatesecond = DateTime.Now.ToString("dd-MM-yyyy HH.mm.ss");
            //eos.fromdate = "24-09-2020 13.35.36";
            //eos.todate = "04-11-2020 16.01.00";
            //eos.todatesecond = "04-11-2020 16.01.00";
            var dtfrom = DateTime.ParseExact(eos.fromdate, "dd-MM-yyyy HH.mm.ss", CultureInfo.InvariantCulture);
            var dtto = DateTime.ParseExact(eos.todate, "dd-MM-yyyy HH.mm.ss", CultureInfo.InvariantCulture);
            var checksaldoawal = db.tblSaldoAwalShifts.Where(p => p.userid == userid && p.startshift <= dtto).OrderByDescending(p => p.startshift).FirstOrDefault();
            if (checksaldoawal != null)
            {
                var checkendofshift = db.tblSettlementEndofShifts.Where(p => p.idshift == checksaldoawal.idshift && p.userid == checksaldoawal.userid).FirstOrDefault();
                if (checkendofshift != null)
                {
                    eos.saldoawal = 0;
                    eos.strsaldoawal = "0";
                    eos.shiftid = 0;
                    eos.isclosed = 0;
                    eos.startshiftdate = "";
                }
                else
                {
                    eos.saldoawal = (double)checksaldoawal.nominal;
                    eos.strsaldoawal = Convert.ToDecimal((double)checksaldoawal.nominal).ToString("#,###");
                    eos.shiftid = checksaldoawal.idshift;
                    eos.isclosed = checksaldoawal.closesession;
                    eos.startshiftdate = checksaldoawal.startshift.ToString("dd-MMM-yyyy HH:mm:ss");
                }
            }
            else
            {
                eos.saldoawal = 0;
                eos.strsaldoawal = "0";
                eos.shiftid = 0;
                eos.isclosed = 0;
                eos.startshiftdate = "";
            }
            eos.payments = new List<PembayaranClass>();
            var tblpembayaran = db.tblPembayarans.Where(p => p.pos == true).OrderBy(p => p.grouppembayaran).ToList();
            string tempHeader = "";
            for (int i = 0; i < tblpembayaran.Count; i++)
            {
                if (tempHeader != tblpembayaran[i].grouppembayaran)
                {
                    PembayaranClass pembayaranHeader = new PembayaranClass();
                    pembayaranHeader.headerName = tblpembayaran[i].grouppembayaran;
                    pembayaranHeader.isHeader = 1;
                    pembayaranHeader.kodepembayaran = "HeaderCode" + i;
                    eos.payments.Add(pembayaranHeader);
                    tempHeader = tblpembayaran[i].grouppembayaran;
                    if (i == 0)
                    {
                        eos.strTamp = pembayaranHeader.kodepembayaran;
                    }
                    else
                    {
                        eos.strTamp = eos.strTamp + ";" + pembayaranHeader.kodepembayaran;
                    }
                }
                PembayaranClass pembayaran = new PembayaranClass();
                if (i == 0)
                {
                    eos.strTamp = tblpembayaran[i].kodepembayaran;
                }
                else
                {
                    eos.strTamp = eos.strTamp + ";" + tblpembayaran[i].kodepembayaran;
                }
                pembayaran.nomor = i;
                pembayaran.kodepembayaran = tblpembayaran[i].kodepembayaran;
                pembayaran.namapembayaran = tblpembayaran[i].nama;
                string kodepembayaran = tblpembayaran[i].kodepembayaran;
                var checktblpenjualan = (from m in db.tblthPenjualans.Where(p => p.userid == userid && p.tgl <= dtto && p.tgl >= dtfrom)
                                         from l1 in db.tblPenjualanBayars.Where(p => p.kodepembayaran == kodepembayaran && p.nofaktur == m.NoFaktur)
                                         group new { m, l1 } by new
                                         {
                                             m.NoFaktur,
                                             m.tgl,
                                             l1.nominal,
                                             l1.charge
                                         }
                           into g
                                         select new
                                         {
                                             g.Key.NoFaktur,
                                             g.Key.tgl,
                                             g.Key.nominal,
                                             g.Key.charge
                                         }).ToList();
                decimal total = 0;
                for (int j = 0; j < checktblpenjualan.Count; j++)
                {
                    total = total + checktblpenjualan[j].nominal + checktblpenjualan[j].charge;
                }
                pembayaran.nominal = (double)total;
                pembayaran.isHeader = 0;
                eos.payments.Add(pembayaran);
            }
            var last = db.tblSettlementEndofShifts.Where(p => p.userid == userid).OrderByDescending(p => p.tgl).FirstOrDefault();
            long check = 0;
            if (last != null)
            {
                if (last.idshift != 0)
                {
                    DateTime tgl = last.tgl;
                    var lasteos = db.tblSettlementEndofShifts.Where(p => p.userid == userid).OrderBy(p => p.tgl).ToList();
                    for (int i = 0; i < lasteos.Count; i++)
                    {
                        if (lasteos[i].beritaacara.Length == 0)
                        {
                            check = lasteos[i].idshift;
                        }
                    }
                }
                else
                {
                    var lasteos = db.tblSettlementEndofShifts.Where(p => p.userid == userid && p.idshift == 0).OrderBy(p => p.tgl).ToList();
                    for (int i = 0; i < lasteos.Count; i++)
                    {
                        if (lasteos[i].beritaacara.Length == 0)
                        {
                            check = -1;
                        }
                    }
                }
            }
            if (eos.shiftid != 0)
            {
                if (check != 0)
                {
                    return RedirectToAction("BeritaAcara", "Settlement", new { lasteos = (check == -1) ? 0 : check });
                }
                else
                {
                    if (eos.isclosed == 0)
                    {
                        return RedirectToAction("Welcome", "Home", new { message = "Please Close Session First" });
                    }
                    else
                    {
                        return View(eos);
                    }
                }
            }
            else
            {
                return RedirectToAction("Welcome", "Home", new { message = "Please Start Shift First" });
            }
        }

        public ActionResult EndofDay(string message)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Message = message;
            EndofDayClass eos = new EndofDayClass();
            string kodelokasi = Session["kode_lokasi"].ToString();
            DateTime dtnow = DateTime.Now;
            var checklasteos = db.tblSettlements.Where(p => p.kodelokasi == kodelokasi).OrderByDescending(p => p.tgl).FirstOrDefault();
            if (checklasteos != null)
            {
                eos.indate = checklasteos.tgl.AddDays(1).ToString("dd-MM-yyyy");
                var tommorowdate = checklasteos.tgl.AddDays(1);
                if (tommorowdate > DateTime.Now)
                {
                    eos.checksettlement = 0;
                }
                else
                {
                    eos.checksettlement = 1;
                }
            }
            else
            {
                eos.indate = DateTime.Now.ToString("dd-MM-yyyy");
                eos.checksettlement = 1;
            }
            var dtindate = DateTime.ParseExact(eos.indate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            eos.payments = new List<PembayaranClass>();
            var tblpembayaran = db.tblPembayarans.Where(p => p.pos == true).OrderBy(p => p.grouppembayaran).ToList();
            string tempHeader = "";
            for (int i = 0; i < tblpembayaran.Count; i++)
            {
                if (tempHeader != tblpembayaran[i].grouppembayaran)
                {
                    PembayaranClass pembayaranHeader = new PembayaranClass();
                    pembayaranHeader.headerName = tblpembayaran[i].grouppembayaran;
                    pembayaranHeader.isHeader = 1;
                    pembayaranHeader.kodepembayaran = "HeaderCode" + i;
                    eos.payments.Add(pembayaranHeader);
                    tempHeader = tblpembayaran[i].grouppembayaran;
                    if (i == 0)
                    {
                        eos.strTamp = pembayaranHeader.kodepembayaran;
                    }
                    else
                    {
                        eos.strTamp = eos.strTamp + ";" + pembayaranHeader.kodepembayaran;
                    }
                }
                PembayaranClass pembayaran = new PembayaranClass();
                if (i == 0)
                {
                    eos.strTamp = tblpembayaran[i].kodepembayaran;
                }
                else
                {
                    eos.strTamp = eos.strTamp + ";" + tblpembayaran[i].kodepembayaran;
                }
                pembayaran.kodepembayaran = tblpembayaran[i].kodepembayaran;
                pembayaran.namapembayaran = tblpembayaran[i].nama;
                string kodepembayaran = tblpembayaran[i].kodepembayaran;
                var checktblpenjualan = (from m in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && p.tgl.Day == dtindate.Day &&
                                         p.tgl.Month == dtindate.Month && p.tgl.Year == dtindate.Year)
                                         from l1 in db.tblPenjualanBayars.Where(p => p.kodepembayaran == kodepembayaran && p.nofaktur == m.NoFaktur)
                                         group new { m, l1 } by new
                                         {
                                             m.NoFaktur,
                                             m.tgl,
                                             l1.nominal,
                                             l1.charge
                                         }
                           into g
                                         select new
                                         {
                                             g.Key.NoFaktur,
                                             g.Key.tgl,
                                             g.Key.nominal,
                                             g.Key.charge
                                         }).ToList();
                decimal total = 0;
                for (int j = 0; j < checktblpenjualan.Count; j++)
                {
                    total = total + checktblpenjualan[j].nominal + checktblpenjualan[j].charge;
                }
                pembayaran.nominal = (double)total;
                pembayaran.isHeader = 0;
                eos.payments.Add(pembayaran);
            }
            var last = db.tblSettlements.Where(p => p.kodelokasi == kodelokasi).OrderByDescending(p => p.tgl).FirstOrDefault();
            int check = 0;
            if (last != null)
            {
                DateTime tgl = last.tgl;
                var lasteos = db.tblSettlements.Where(p => p.tgl == tgl && p.kodelokasi == kodelokasi).ToList();
                for (int i = 0; i < lasteos.Count; i++)
                {
                    //if (lasteos[i].nominal != lasteos[i].fisik && lasteos[i].beritaacara.Length == 0)
                    if (lasteos[i].beritaacara.Length == 0)
                    {
                        check = check + 1;
                    }
                }
            }
            if (check != 0)
            {
                return RedirectToAction("BeritaAcaraEOD", "Settlement", new { lasteos = check });
            }
            else
            {
                var checkallclosesession = (from m in db.musers.Where(p => p.kodelokasi == kodelokasi)
                                         from l1 in db.tblSaldoAwalShifts.Where(p => p.userid == m.muserid &&
                                         p.startshift.Day == dtindate.Day && p.startshift.Month == dtindate.Month && p.startshift.Year == dtindate.Year)
                                         group new { m, l1 } by new
                                         {
                                            m.muserid
                                         }
                                         into g
                                         select new
                                         {
                                             g.Key.muserid
                                         }).ToList();
                var checkclosesession = (from m in db.musers.Where(p => p.kodelokasi == kodelokasi)
                                         from l1 in db.tblSaldoAwalShifts.Where(p => p.userid == m.muserid && p.closesession == 1 &&
                                         p.startshift.Day == dtindate.Day && p.startshift.Month == dtindate.Month && p.startshift.Year == dtindate.Year)
                                         group new { m, l1 } by new
                                         {
                                             m.muserid
                                         }
                                        into g
                                         select new
                                         {
                                             g.Key.muserid
                                         }).ToList();
                if (checkallclosesession.Count == checkclosesession.Count)
                {
                    return View(eos);
                }
                else
                {
                    return RedirectToAction("Welcome", "Home", new { message = "Please Close All Session First" });
                    //return View(eos);
                }
            }
        }

        public ActionResult BeritaAcara(long lasteos)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            EndofShiftClass eos = new EndofShiftClass();
            eos.payments = new List<PembayaranClass>();
            var getlasteos = db.tblSettlementEndofShifts.Where(p => p.userid == userid && p.idshift == lasteos && p.beritaacara.Length == 0).OrderByDescending(p => p.tgl).ToList();
            double totalnominal = 0;
            double totalfisik = 0;
            double totaldiff = 0;
            for (int i = 0; i < getlasteos.Count; i++)
            {
                PembayaranClass pembayaran = new PembayaranClass();
                string kodepembayaran = getlasteos[i].kodepembayaran;
                var checkpembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepembayaran).FirstOrDefault();
                if (checkpembayaran != null)
                {
                    pembayaran.kodepembayaran = checkpembayaran.kodepembayaran;
                    pembayaran.namapembayaran = checkpembayaran.nama;
                }
                double subnominal = (double)getlasteos[i].nominal + (double)getlasteos[i].nominalmdr;
                double subfisik = (double)getlasteos[i].fisik + (double)getlasteos[i].nominalfisikmdr;
                double subdiff = subfisik - subnominal;
                pembayaran.nominal = subnominal;
                pembayaran.fisik = subfisik;
                pembayaran.diff = subdiff;
                eos.payments.Add(pembayaran);
                totalnominal = totalnominal + subnominal;
                totalfisik = totalfisik + subfisik;
                totaldiff = totaldiff + subdiff;
                eos.fromdate = getlasteos[i].startdate?.ToString("dd-MM-yyyy HH.mm.ss");
                eos.todate = getlasteos[i].enddate?.ToString("dd-MM-yyyy HH.mm.ss");
                //eos.fromdate = DateTime.ParseExact(getlasteos[i].startdate?.ToString("dd-MMM-yyyy HH.mm"), "dd-MMM-yyyy HH.mm", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy HH.mm");
                //eos.todate = DateTime.ParseExact(getlasteos[i].enddate?.ToString("dd-MMM-yyyy HH.mm"), "dd-MMM-yyyy HH.mm", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy HH.mm");
            }
            var getstartshift = db.tblSaldoAwalShifts.Where(p => p.idshift == lasteos).FirstOrDefault();
            if (getstartshift != null)
            {
                eos.startshiftdate = getstartshift.startshift.ToString("dd-MM-yyyy HH.mm.ss");
            }
            PembayaranClass pembayarantotal = new PembayaranClass();
            pembayarantotal.kodepembayaran = "";
            pembayarantotal.namapembayaran = "Total";
            pembayarantotal.nominal = totalnominal;
            pembayarantotal.fisik = totalfisik;
            pembayarantotal.diff = totaldiff;
            eos.payments.Add(pembayarantotal);
            eos.headerdiff = totaldiff;
            return View(eos);
        }

        public ActionResult BeritaAcaraEOD(int lasteos)
        {
            if (Session["role_id"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            string kodelokasi = Session["kode_lokasi"].ToString();
            EndofDayClass eos = new EndofDayClass();
            eos.payments = new List<PembayaranClass>();
            var getlasteos = db.tblSettlements.Where(p => p.kodelokasi == kodelokasi).OrderByDescending(p => p.tgl).Take(lasteos).ToList();
            double totalnominal = 0;
            double totalfisik = 0;
            double totaldiff = 0;
            for (int i = 0; i < getlasteos.Count; i++)
            {
                PembayaranClass pembayaran = new PembayaranClass();
                string kodepembayaran = getlasteos[i].kodepembayaran;
                var checkpembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepembayaran).FirstOrDefault();
                if (checkpembayaran != null)
                {
                    pembayaran.kodepembayaran = checkpembayaran.kodepembayaran;
                    pembayaran.namapembayaran = checkpembayaran.nama;
                }
                double subnominal = (double)getlasteos[i].nominal + (double)getlasteos[i].nominalmdr;
                double subfisik = (double)getlasteos[i].fisik + (double)getlasteos[i].nominalfisikmdr;
                double subdiff = subfisik - subnominal;
                pembayaran.nominal = subnominal;
                pembayaran.fisik = subfisik;
                pembayaran.diff = subdiff;
                eos.payments.Add(pembayaran);
                totalnominal = totalnominal + subnominal;
                totalfisik = totalfisik + subfisik;
                totaldiff = totaldiff + subdiff;
                eos.indate = getlasteos[i].tgl.ToString("dd-MM-yyyy");
            }
            PembayaranClass pembayarantotal = new PembayaranClass();
            pembayarantotal.kodepembayaran = "";
            pembayarantotal.namapembayaran = "Total";
            pembayarantotal.nominal = totalnominal;
            pembayarantotal.fisik = totalfisik;
            pembayarantotal.diff = totaldiff;
            eos.payments.Add(pembayarantotal);
            eos.headerdiff = totaldiff;
            return View(eos);
        }

        public ActionResult SubmitEndofShift(string from, string to, string berita, string shiftid, string payments)
        {
            var dtfrom = DateTime.ParseExact(from, "dd-MM-yyyy HH.mm.ss", CultureInfo.InvariantCulture);
            var dtto = DateTime.ParseExact(to, "dd-MM-yyyy HH.mm.ss", CultureInfo.InvariantCulture);
            string strdtfrom = dtfrom.ToString("yyyyMMddHHmmss");
            string strdtto = dtto.ToString("yyyyMMddHHmmss");
            string kodelokasi = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            string[] splitpayment = payments.Split(';');
            long count = 0;
            int checkdiff = 0;
            bool isValid = true;
            for (int i = 0; i < splitpayment.Length; i++)
            {
                string[] splitpaymentperrow = splitpayment[i].Split('$');
                if (splitpaymentperrow[3].Length > 0 || splitpaymentperrow[1].Length > 0)
                {
                    string kodepembayaran = splitpaymentperrow[0];
                    tblSettlementEndofShift endofShift = new tblSettlementEndofShift();
                    endofShift.userid = userid;
                    endofShift.tgl = dtto;
                    endofShift.kodelokasi = kodelokasi;
                    endofShift.kodepembayaran = splitpaymentperrow[0];
                    var checktblpenjualan = (from m in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && p.tgl <= dtto && p.tgl >= dtfrom && p.userid == userid)
                                             from l1 in db.tblPenjualanBayars.Where(p => p.kodepembayaran == kodepembayaran && p.nofaktur == m.NoFaktur)
                                             group new { m, l1 } by new
                                             {
                                                 m.NoFaktur,
                                                 m.tgl,
                                                 l1.nominal,
                                                 l1.charge
                                             }
                                            into g
                                             select new
                                             {
                                                 g.Key.NoFaktur,
                                                 g.Key.tgl,
                                                 g.Key.nominal,
                                                 g.Key.charge
                                             }).ToList();
                    decimal total = 0;
                    for (int j = 0; j < checktblpenjualan.Count; j++)
                    {
                        total = total + checktblpenjualan[j].nominal - checktblpenjualan[j].charge;
                    }
                    endofShift.nominal = total;
                    endofShift.qty = 1;
                    endofShift.fisik = splitpaymentperrow[1].Length > 0 ? (decimal)Int32.Parse(splitpaymentperrow[1]) : 0;
                    endofShift.keterangan = splitpaymentperrow[2];
                    endofShift.beritaacara = berita;
                    endofShift.modidate = dtto;
                    endofShift.startdate = dtfrom;
                    endofShift.enddate = dtto;
                    var checktblpembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepembayaran).FirstOrDefault();
                    if (checktblpembayaran != null)
                    {
                        endofShift.nominalmdr = total * checktblpembayaran.charge / 100;
                        endofShift.nominalfisikmdr = (splitpaymentperrow[1].Length > 0 ? (decimal)Int32.Parse(splitpaymentperrow[1]) : 0) * checktblpembayaran.charge / 100;
                    }
                    endofShift.idshift = long.Parse(shiftid);
                    if (endofShift.fisik != endofShift.nominal)
                    {
                        checkdiff = checkdiff + 1;
                    }
                    db.tblSettlementEndofShifts.Add(endofShift);
                    count = long.Parse(shiftid);
                }
            }
            db.SaveChanges();
            //if (checkdiff != 0)
            //{
            //    return RedirectToAction("BeritaAcara", "Settlement", new { lasteos = count });
            //}
            //else
            //{
            //    return RedirectToAction("Welcome", "Home", new { message = "End of Shift Success without Difference" });
            //}
            var obj = new
            {
                valid = isValid,
                totalcount = count
            };
            return Json(obj);
            //return RedirectToAction("BeritaAcara", "Settlement", new { lasteos = count });
        }

        public ActionResult SubmitEndofDay(string indate, string berita, string payments)
        {
            var dtfrom = DateTime.ParseExact(indate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            string kodelokasi = Session["kode_lokasi"].ToString();
            string userid = Session["user_id"].ToString();
            string[] splitpayment = payments.Split(';');
            int count = 0;
            int checkdiff = 0;
            bool isValid = true;
            for (int i = 0; i < splitpayment.Length; i++)
            {
                string[] splitpaymentperrow = splitpayment[i].Split('$');
                if (splitpaymentperrow[3].Length > 0 || splitpaymentperrow[1].Length > 0)
                {
                    string kodepembayaran = splitpaymentperrow[0];
                    tblSettlement endofShift = new tblSettlement();
                    endofShift.tgl = dtfrom;
                    endofShift.kodelokasi = kodelokasi;
                    endofShift.kodepembayaran = splitpaymentperrow[0];
                    var checktblpenjualan = (from m in db.tblthPenjualans.Where(p => p.kodelokasi == kodelokasi && p.tgl.Day == dtfrom.Day &&
                                         p.tgl.Month == dtfrom.Month && p.tgl.Year == dtfrom.Year)
                                             from l1 in db.tblPenjualanBayars.Where(p => p.kodepembayaran == kodepembayaran && p.nofaktur == m.NoFaktur)
                                             group new { m, l1 } by new
                                             {
                                                 m.NoFaktur,
                                                 m.tgl,
                                                 l1.nominal,
                                                 l1.charge
                                             }
                                             into g
                                             select new
                                             {
                                                 g.Key.NoFaktur,
                                                 g.Key.tgl,
                                                 g.Key.nominal,
                                                 g.Key.charge
                                             }).ToList();
                    decimal total = 0;
                    for (int j = 0; j < checktblpenjualan.Count; j++)
                    {
                        total = total + checktblpenjualan[j].nominal - checktblpenjualan[j].charge;
                    }
                    endofShift.nominal = total;
                    endofShift.qty = 1;
                    endofShift.fisik = splitpaymentperrow[1].Length > 0 ? (decimal)Int32.Parse(splitpaymentperrow[1]) : 0;
                    endofShift.keterangan = splitpaymentperrow[2];
                    endofShift.beritaacara = berita;
                    endofShift.userid = userid;
                    endofShift.modidate = DateTime.Now;
                    var checktblpembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepembayaran).FirstOrDefault();
                    if (checktblpembayaran != null)
                    {
                        endofShift.nominalmdr = total * checktblpembayaran.charge / 100;
                        endofShift.nominalfisikmdr = (splitpaymentperrow[1].Length > 0 ? (decimal)Int32.Parse(splitpaymentperrow[1]) : 0) * checktblpembayaran.charge / 100;
                    }
                    if (endofShift.fisik != endofShift.nominal)
                    {
                        checkdiff = checkdiff + 1;
                    }
                    db.tblSettlements.Add(endofShift);
                    db.SaveChanges();
                    count = count + 1;
                }
            }
            //if (checkdiff != 0)
            //{
            //    return RedirectToAction("BeritaAcaraEOD", "Settlement", new { lasteos = count });
            //}
            //else
            //{
            //    return RedirectToAction("Welcome", "Home", new { message = "End of Day has been Succeed without Difference" });
            //}
            var obj = new
            {
                valid = isValid,
                totalcount = count
            };
            return Json(obj);
            //return RedirectToAction("BeritaAcaraEOD", "Settlement", new { lasteos = count });
        }

        public ActionResult UpdateEndofShift(string from, string to, string berita)
        {
            bool isValid = true;
            string kodelokasi = Session["kode_lokasi"].ToString();
            var dt = DateTime.ParseExact(to, "dd-MM-yyyy HH.mm.ss", CultureInfo.InvariantCulture);
            var checklasteos = db.tblSettlementEndofShifts.Where(p => p.tgl == dt && p.kodelokasi == kodelokasi).ToList();
            string headerberitaacara = "Berikut saya sampaikan Hasil End of Shift " + from + " - " + to + " sebenarnya berdasarkan fisik yang ada:\n\n";
            string footerberitaacara = "Terjadinya selisih pada End of Shift " + from + " - " + to + "\ndikarenakan hal sebagai berikut:\n\n" + berita +
                "\n\nDemikian BA ini dibuat agar dapat dipergunakan sebagaimana mestinya.\nHormat saya,\n\n" + Session["user_name"].ToString();
            string fullberitaacara = headerberitaacara;
            for (int i = 0; i < checklasteos.Count; i++)
            {
                string kodepemb = checklasteos[i].kodepembayaran;
                var checkkodepembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepemb).FirstOrDefault();
                fullberitaacara = fullberitaacara + checkkodepembayaran.nama + "   " + "(S) " +
                    (checklasteos[i].nominal == 0 ? "0" : checklasteos[i].nominal.ToString("#,###")) + "   " + "(F) " +
                    (checklasteos[i].fisik == 0 ? "0" : checklasteos[i].fisik.ToString("#,###")) + "   " + "(+/-) " +
                    (checklasteos[i].fisik - checklasteos[i].nominal).ToString("#,###") + "\n";
                if (i == checklasteos.Count - 1)
                {
                    fullberitaacara = fullberitaacara + "\n";
                }
            }
            fullberitaacara = fullberitaacara + footerberitaacara;
            for (int i = 0; i < checklasteos.Count; i++)
            {
                checklasteos[i].beritaacara = fullberitaacara;
                db.SaveChanges();
            }
            //var client = new SmtpClient("smtp.gmail.com", 587)
            //{
            //    Credentials = new NetworkCredential("tehpucuk1234@gmail.com", "pucuk1234"),
            //    EnableSsl = true
            //};
            //client.Send("tehpucuk1234@gmail.com", "andreferandokwee@gmail.com", "Berita Acara End of Shift", fullberitaacara);
            var obj = new
            {
                valid = isValid,
                messages = "End of Shift Success"
            };
            return Json(obj);
            //return RedirectToAction("Welcome", "Home", new { message = "End of Shift Success" });
        }

        public ActionResult UpdateEndofDay(string indate, string berita)
        {
            bool isValid = true;
            string kodelokasi = Session["kode_lokasi"].ToString();
            var dt = DateTime.ParseExact(indate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            var checklasteos = db.tblSettlements.Where(p => p.tgl == dt && p.kodelokasi == kodelokasi).ToList();
            string headerberitaacara = "Berikut saya sampaikan Hasil End of Day " + indate + " sebenarnya berdasarkan fisik yang ada:\n\n";
            string footerberitaacara = "Terjadinya selisih pada End of Day " + indate + "\ndikarenakan hal sebagai berikut:\n\n" + berita +
                "\n\nDemikian BA ini dibuat agar dapat dipergunakan sebagaimana mestinya.\nHormat saya,\n\n" + Session["user_name"].ToString();
            string fullberitaacara = headerberitaacara;
            for (int i = 0; i < checklasteos.Count; i++)
            {
                string kodepemb = checklasteos[i].kodepembayaran;
                var checkkodepembayaran = db.tblPembayarans.Where(p => p.kodepembayaran == kodepemb).FirstOrDefault();
                fullberitaacara = fullberitaacara + checkkodepembayaran.nama + "   " + "(S) " +
                    (checklasteos[i].nominal == 0 ? "0" : checklasteos[i].nominal.ToString("#,###")) + "   " + "(F) " +
                    (checklasteos[i].fisik == 0 ? "0" : checklasteos[i].fisik.ToString("#,###")) + "   " + "(+/-) " +
                    (checklasteos[i].fisik - checklasteos[i].nominal).ToString("#,###") + "\n";
                if (i == checklasteos.Count - 1)
                {
                    fullberitaacara = fullberitaacara + "\n";
                }
            }
            fullberitaacara = fullberitaacara + footerberitaacara;
            for (int i = 0; i < checklasteos.Count; i++)
            {
                checklasteos[i].beritaacara = fullberitaacara;
                db.SaveChanges();
            }
            var obj = new
            {
                valid = isValid,
                messages = "End of Day Success"
            };
            return Json(obj);
            //return RedirectToAction("Welcome", "Home", new { message = "End of Day Success" });
        }
    }
}